/**
 */
package my.home.dsl.deepClone;

import org.eclipse.emf.ecore.EObject;

/**
 * <!-- begin-user-doc -->
 * A representation of the model object '<em><b>Model</b></em>'.
 * <!-- end-user-doc -->
 *
 *
 * @see my.home.dsl.deepClone.DeepClonePackage#getModel()
 * @model
 * @generated
 */
public interface Model extends EObject
{
} // Model
