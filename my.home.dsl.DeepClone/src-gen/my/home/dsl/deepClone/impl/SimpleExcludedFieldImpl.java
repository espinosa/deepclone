/**
 */
package my.home.dsl.deepClone.impl;

import my.home.dsl.deepClone.DeepClonePackage;
import my.home.dsl.deepClone.SimpleExcludedField;

import org.eclipse.emf.ecore.EClass;

/**
 * <!-- begin-user-doc -->
 * An implementation of the model object '<em><b>Simple Excluded Field</b></em>'.
 * <!-- end-user-doc -->
 * <p>
 * </p>
 *
 * @generated
 */
public class SimpleExcludedFieldImpl extends FieldClonerTypeImpl implements SimpleExcludedField
{
  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  protected SimpleExcludedFieldImpl()
  {
    super();
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  @Override
  protected EClass eStaticClass()
  {
    return DeepClonePackage.Literals.SIMPLE_EXCLUDED_FIELD;
  }

} //SimpleExcludedFieldImpl
