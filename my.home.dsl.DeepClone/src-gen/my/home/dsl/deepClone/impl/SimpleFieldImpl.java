/**
 */
package my.home.dsl.deepClone.impl;

import my.home.dsl.deepClone.DeepClonePackage;
import my.home.dsl.deepClone.SimpleField;

import org.eclipse.emf.ecore.EClass;

/**
 * <!-- begin-user-doc -->
 * An implementation of the model object '<em><b>Simple Field</b></em>'.
 * <!-- end-user-doc -->
 * <p>
 * </p>
 *
 * @generated
 */
public class SimpleFieldImpl extends FieldClonerTypeImpl implements SimpleField
{
  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  protected SimpleFieldImpl()
  {
    super();
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  @Override
  protected EClass eStaticClass()
  {
    return DeepClonePackage.Literals.SIMPLE_FIELD;
  }

} //SimpleFieldImpl
