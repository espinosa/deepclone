/**
 */
package my.home.dsl.deepClone.impl;

import my.home.dsl.deepClone.ClassCloner;
import my.home.dsl.deepClone.DeepClonePackage;
import my.home.dsl.deepClone.ReferenceField;

import org.eclipse.emf.common.notify.Notification;

import org.eclipse.emf.ecore.EClass;
import org.eclipse.emf.ecore.InternalEObject;

import org.eclipse.emf.ecore.impl.ENotificationImpl;

/**
 * <!-- begin-user-doc -->
 * An implementation of the model object '<em><b>Reference Field</b></em>'.
 * <!-- end-user-doc -->
 * <p>
 * The following features are implemented:
 * <ul>
 *   <li>{@link my.home.dsl.deepClone.impl.ReferenceFieldImpl#getClonerReference <em>Cloner Reference</em>}</li>
 * </ul>
 * </p>
 *
 * @generated
 */
public class ReferenceFieldImpl extends FieldClonerTypeImpl implements ReferenceField
{
  /**
   * The cached value of the '{@link #getClonerReference() <em>Cloner Reference</em>}' reference.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @see #getClonerReference()
   * @generated
   * @ordered
   */
  protected ClassCloner clonerReference;

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  protected ReferenceFieldImpl()
  {
    super();
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  @Override
  protected EClass eStaticClass()
  {
    return DeepClonePackage.Literals.REFERENCE_FIELD;
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public ClassCloner getClonerReference()
  {
    if (clonerReference != null && clonerReference.eIsProxy())
    {
      InternalEObject oldClonerReference = (InternalEObject)clonerReference;
      clonerReference = (ClassCloner)eResolveProxy(oldClonerReference);
      if (clonerReference != oldClonerReference)
      {
        if (eNotificationRequired())
          eNotify(new ENotificationImpl(this, Notification.RESOLVE, DeepClonePackage.REFERENCE_FIELD__CLONER_REFERENCE, oldClonerReference, clonerReference));
      }
    }
    return clonerReference;
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public ClassCloner basicGetClonerReference()
  {
    return clonerReference;
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public void setClonerReference(ClassCloner newClonerReference)
  {
    ClassCloner oldClonerReference = clonerReference;
    clonerReference = newClonerReference;
    if (eNotificationRequired())
      eNotify(new ENotificationImpl(this, Notification.SET, DeepClonePackage.REFERENCE_FIELD__CLONER_REFERENCE, oldClonerReference, clonerReference));
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  @Override
  public Object eGet(int featureID, boolean resolve, boolean coreType)
  {
    switch (featureID)
    {
      case DeepClonePackage.REFERENCE_FIELD__CLONER_REFERENCE:
        if (resolve) return getClonerReference();
        return basicGetClonerReference();
    }
    return super.eGet(featureID, resolve, coreType);
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  @Override
  public void eSet(int featureID, Object newValue)
  {
    switch (featureID)
    {
      case DeepClonePackage.REFERENCE_FIELD__CLONER_REFERENCE:
        setClonerReference((ClassCloner)newValue);
        return;
    }
    super.eSet(featureID, newValue);
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  @Override
  public void eUnset(int featureID)
  {
    switch (featureID)
    {
      case DeepClonePackage.REFERENCE_FIELD__CLONER_REFERENCE:
        setClonerReference((ClassCloner)null);
        return;
    }
    super.eUnset(featureID);
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  @Override
  public boolean eIsSet(int featureID)
  {
    switch (featureID)
    {
      case DeepClonePackage.REFERENCE_FIELD__CLONER_REFERENCE:
        return clonerReference != null;
    }
    return super.eIsSet(featureID);
  }

} //ReferenceFieldImpl
