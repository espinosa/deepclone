/**
 */
package my.home.dsl.deepClone.impl;

import my.home.dsl.deepClone.BaseType;
import my.home.dsl.deepClone.DeepClonePackage;

import org.eclipse.emf.common.notify.Notification;
import org.eclipse.emf.common.notify.NotificationChain;

import org.eclipse.emf.ecore.EClass;
import org.eclipse.emf.ecore.InternalEObject;

import org.eclipse.emf.ecore.impl.ENotificationImpl;
import org.eclipse.emf.ecore.impl.MinimalEObjectImpl;

import org.eclipse.xtext.common.types.JvmTypeReference;

/**
 * <!-- begin-user-doc -->
 * An implementation of the model object '<em><b>Base Type</b></em>'.
 * <!-- end-user-doc -->
 * <p>
 * The following features are implemented:
 * <ul>
 *   <li>{@link my.home.dsl.deepClone.impl.BaseTypeImpl#getJavaType <em>Java Type</em>}</li>
 * </ul>
 * </p>
 *
 * @generated
 */
public class BaseTypeImpl extends MinimalEObjectImpl.Container implements BaseType
{
  /**
   * The cached value of the '{@link #getJavaType() <em>Java Type</em>}' containment reference.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @see #getJavaType()
   * @generated
   * @ordered
   */
  protected JvmTypeReference javaType;

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  protected BaseTypeImpl()
  {
    super();
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  @Override
  protected EClass eStaticClass()
  {
    return DeepClonePackage.Literals.BASE_TYPE;
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public JvmTypeReference getJavaType()
  {
    return javaType;
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public NotificationChain basicSetJavaType(JvmTypeReference newJavaType, NotificationChain msgs)
  {
    JvmTypeReference oldJavaType = javaType;
    javaType = newJavaType;
    if (eNotificationRequired())
    {
      ENotificationImpl notification = new ENotificationImpl(this, Notification.SET, DeepClonePackage.BASE_TYPE__JAVA_TYPE, oldJavaType, newJavaType);
      if (msgs == null) msgs = notification; else msgs.add(notification);
    }
    return msgs;
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public void setJavaType(JvmTypeReference newJavaType)
  {
    if (newJavaType != javaType)
    {
      NotificationChain msgs = null;
      if (javaType != null)
        msgs = ((InternalEObject)javaType).eInverseRemove(this, EOPPOSITE_FEATURE_BASE - DeepClonePackage.BASE_TYPE__JAVA_TYPE, null, msgs);
      if (newJavaType != null)
        msgs = ((InternalEObject)newJavaType).eInverseAdd(this, EOPPOSITE_FEATURE_BASE - DeepClonePackage.BASE_TYPE__JAVA_TYPE, null, msgs);
      msgs = basicSetJavaType(newJavaType, msgs);
      if (msgs != null) msgs.dispatch();
    }
    else if (eNotificationRequired())
      eNotify(new ENotificationImpl(this, Notification.SET, DeepClonePackage.BASE_TYPE__JAVA_TYPE, newJavaType, newJavaType));
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  @Override
  public NotificationChain eInverseRemove(InternalEObject otherEnd, int featureID, NotificationChain msgs)
  {
    switch (featureID)
    {
      case DeepClonePackage.BASE_TYPE__JAVA_TYPE:
        return basicSetJavaType(null, msgs);
    }
    return super.eInverseRemove(otherEnd, featureID, msgs);
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  @Override
  public Object eGet(int featureID, boolean resolve, boolean coreType)
  {
    switch (featureID)
    {
      case DeepClonePackage.BASE_TYPE__JAVA_TYPE:
        return getJavaType();
    }
    return super.eGet(featureID, resolve, coreType);
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  @Override
  public void eSet(int featureID, Object newValue)
  {
    switch (featureID)
    {
      case DeepClonePackage.BASE_TYPE__JAVA_TYPE:
        setJavaType((JvmTypeReference)newValue);
        return;
    }
    super.eSet(featureID, newValue);
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  @Override
  public void eUnset(int featureID)
  {
    switch (featureID)
    {
      case DeepClonePackage.BASE_TYPE__JAVA_TYPE:
        setJavaType((JvmTypeReference)null);
        return;
    }
    super.eUnset(featureID);
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  @Override
  public boolean eIsSet(int featureID)
  {
    switch (featureID)
    {
      case DeepClonePackage.BASE_TYPE__JAVA_TYPE:
        return javaType != null;
    }
    return super.eIsSet(featureID);
  }

} //BaseTypeImpl
