/**
 */
package my.home.dsl.deepClone.impl;

import java.util.Collection;

import my.home.dsl.deepClone.Body;
import my.home.dsl.deepClone.ClassCloner;
import my.home.dsl.deepClone.DeepClonePackage;
import my.home.dsl.deepClone.PackageConfig;

import org.eclipse.emf.common.notify.Notification;
import org.eclipse.emf.common.notify.NotificationChain;

import org.eclipse.emf.common.util.EList;

import org.eclipse.emf.ecore.EClass;
import org.eclipse.emf.ecore.InternalEObject;

import org.eclipse.emf.ecore.impl.ENotificationImpl;

import org.eclipse.emf.ecore.util.EObjectContainmentEList;
import org.eclipse.emf.ecore.util.InternalEList;

/**
 * <!-- begin-user-doc -->
 * An implementation of the model object '<em><b>Body</b></em>'.
 * <!-- end-user-doc -->
 * <p>
 * The following features are implemented:
 * <ul>
 *   <li>{@link my.home.dsl.deepClone.impl.BodyImpl#getPackageConfig <em>Package Config</em>}</li>
 *   <li>{@link my.home.dsl.deepClone.impl.BodyImpl#getCloners <em>Cloners</em>}</li>
 * </ul>
 * </p>
 *
 * @generated
 */
public class BodyImpl extends ModelImpl implements Body
{
  /**
   * The cached value of the '{@link #getPackageConfig() <em>Package Config</em>}' containment reference.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @see #getPackageConfig()
   * @generated
   * @ordered
   */
  protected PackageConfig packageConfig;

  /**
   * The cached value of the '{@link #getCloners() <em>Cloners</em>}' containment reference list.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @see #getCloners()
   * @generated
   * @ordered
   */
  protected EList<ClassCloner> cloners;

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  protected BodyImpl()
  {
    super();
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  @Override
  protected EClass eStaticClass()
  {
    return DeepClonePackage.Literals.BODY;
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public PackageConfig getPackageConfig()
  {
    return packageConfig;
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public NotificationChain basicSetPackageConfig(PackageConfig newPackageConfig, NotificationChain msgs)
  {
    PackageConfig oldPackageConfig = packageConfig;
    packageConfig = newPackageConfig;
    if (eNotificationRequired())
    {
      ENotificationImpl notification = new ENotificationImpl(this, Notification.SET, DeepClonePackage.BODY__PACKAGE_CONFIG, oldPackageConfig, newPackageConfig);
      if (msgs == null) msgs = notification; else msgs.add(notification);
    }
    return msgs;
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public void setPackageConfig(PackageConfig newPackageConfig)
  {
    if (newPackageConfig != packageConfig)
    {
      NotificationChain msgs = null;
      if (packageConfig != null)
        msgs = ((InternalEObject)packageConfig).eInverseRemove(this, EOPPOSITE_FEATURE_BASE - DeepClonePackage.BODY__PACKAGE_CONFIG, null, msgs);
      if (newPackageConfig != null)
        msgs = ((InternalEObject)newPackageConfig).eInverseAdd(this, EOPPOSITE_FEATURE_BASE - DeepClonePackage.BODY__PACKAGE_CONFIG, null, msgs);
      msgs = basicSetPackageConfig(newPackageConfig, msgs);
      if (msgs != null) msgs.dispatch();
    }
    else if (eNotificationRequired())
      eNotify(new ENotificationImpl(this, Notification.SET, DeepClonePackage.BODY__PACKAGE_CONFIG, newPackageConfig, newPackageConfig));
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public EList<ClassCloner> getCloners()
  {
    if (cloners == null)
    {
      cloners = new EObjectContainmentEList<ClassCloner>(ClassCloner.class, this, DeepClonePackage.BODY__CLONERS);
    }
    return cloners;
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  @Override
  public NotificationChain eInverseRemove(InternalEObject otherEnd, int featureID, NotificationChain msgs)
  {
    switch (featureID)
    {
      case DeepClonePackage.BODY__PACKAGE_CONFIG:
        return basicSetPackageConfig(null, msgs);
      case DeepClonePackage.BODY__CLONERS:
        return ((InternalEList<?>)getCloners()).basicRemove(otherEnd, msgs);
    }
    return super.eInverseRemove(otherEnd, featureID, msgs);
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  @Override
  public Object eGet(int featureID, boolean resolve, boolean coreType)
  {
    switch (featureID)
    {
      case DeepClonePackage.BODY__PACKAGE_CONFIG:
        return getPackageConfig();
      case DeepClonePackage.BODY__CLONERS:
        return getCloners();
    }
    return super.eGet(featureID, resolve, coreType);
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  @SuppressWarnings("unchecked")
  @Override
  public void eSet(int featureID, Object newValue)
  {
    switch (featureID)
    {
      case DeepClonePackage.BODY__PACKAGE_CONFIG:
        setPackageConfig((PackageConfig)newValue);
        return;
      case DeepClonePackage.BODY__CLONERS:
        getCloners().clear();
        getCloners().addAll((Collection<? extends ClassCloner>)newValue);
        return;
    }
    super.eSet(featureID, newValue);
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  @Override
  public void eUnset(int featureID)
  {
    switch (featureID)
    {
      case DeepClonePackage.BODY__PACKAGE_CONFIG:
        setPackageConfig((PackageConfig)null);
        return;
      case DeepClonePackage.BODY__CLONERS:
        getCloners().clear();
        return;
    }
    super.eUnset(featureID);
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  @Override
  public boolean eIsSet(int featureID)
  {
    switch (featureID)
    {
      case DeepClonePackage.BODY__PACKAGE_CONFIG:
        return packageConfig != null;
      case DeepClonePackage.BODY__CLONERS:
        return cloners != null && !cloners.isEmpty();
    }
    return super.eIsSet(featureID);
  }

} //BodyImpl
