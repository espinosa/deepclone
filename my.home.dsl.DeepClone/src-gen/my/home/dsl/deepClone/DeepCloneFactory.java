/**
 */
package my.home.dsl.deepClone;

import org.eclipse.emf.ecore.EFactory;

/**
 * <!-- begin-user-doc -->
 * The <b>Factory</b> for the model.
 * It provides a create method for each non-abstract class of the model.
 * <!-- end-user-doc -->
 * @see my.home.dsl.deepClone.DeepClonePackage
 * @generated
 */
public interface DeepCloneFactory extends EFactory
{
  /**
   * The singleton instance of the factory.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  DeepCloneFactory eINSTANCE = my.home.dsl.deepClone.impl.DeepCloneFactoryImpl.init();

  /**
   * Returns a new object of class '<em>Model</em>'.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @return a new object of class '<em>Model</em>'.
   * @generated
   */
  Model createModel();

  /**
   * Returns a new object of class '<em>Body</em>'.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @return a new object of class '<em>Body</em>'.
   * @generated
   */
  Body createBody();

  /**
   * Returns a new object of class '<em>Package Config</em>'.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @return a new object of class '<em>Package Config</em>'.
   * @generated
   */
  PackageConfig createPackageConfig();

  /**
   * Returns a new object of class '<em>Class Cloner</em>'.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @return a new object of class '<em>Class Cloner</em>'.
   * @generated
   */
  ClassCloner createClassCloner();

  /**
   * Returns a new object of class '<em>Field Cloner Type</em>'.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @return a new object of class '<em>Field Cloner Type</em>'.
   * @generated
   */
  FieldClonerType createFieldClonerType();

  /**
   * Returns a new object of class '<em>Simple Field</em>'.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @return a new object of class '<em>Simple Field</em>'.
   * @generated
   */
  SimpleField createSimpleField();

  /**
   * Returns a new object of class '<em>Simple Excluded Field</em>'.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @return a new object of class '<em>Simple Excluded Field</em>'.
   * @generated
   */
  SimpleExcludedField createSimpleExcludedField();

  /**
   * Returns a new object of class '<em>Complex Field</em>'.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @return a new object of class '<em>Complex Field</em>'.
   * @generated
   */
  ComplexField createComplexField();

  /**
   * Returns a new object of class '<em>Reference Field</em>'.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @return a new object of class '<em>Reference Field</em>'.
   * @generated
   */
  ReferenceField createReferenceField();

  /**
   * Returns a new object of class '<em>Base Type</em>'.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @return a new object of class '<em>Base Type</em>'.
   * @generated
   */
  BaseType createBaseType();

  /**
   * Returns a new object of class '<em>Container Type</em>'.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @return a new object of class '<em>Container Type</em>'.
   * @generated
   */
  ContainerType createContainerType();

  /**
   * Returns the package supported by this factory.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @return the package supported by this factory.
   * @generated
   */
  DeepClonePackage getDeepClonePackage();

} //DeepCloneFactory
