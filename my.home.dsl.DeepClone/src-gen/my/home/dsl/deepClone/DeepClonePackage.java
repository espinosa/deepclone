/**
 */
package my.home.dsl.deepClone;

import org.eclipse.emf.ecore.EAttribute;
import org.eclipse.emf.ecore.EClass;
import org.eclipse.emf.ecore.EPackage;
import org.eclipse.emf.ecore.EReference;

/**
 * <!-- begin-user-doc -->
 * The <b>Package</b> for the model.
 * It contains accessors for the meta objects to represent
 * <ul>
 *   <li>each class,</li>
 *   <li>each feature of each class,</li>
 *   <li>each enum,</li>
 *   <li>and each data type</li>
 * </ul>
 * <!-- end-user-doc -->
 * @see my.home.dsl.deepClone.DeepCloneFactory
 * @model kind="package"
 * @generated
 */
public interface DeepClonePackage extends EPackage
{
  /**
   * The package name.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  String eNAME = "deepClone";

  /**
   * The package namespace URI.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  String eNS_URI = "http://www.home.my/dsl/DeepClone";

  /**
   * The package namespace name.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  String eNS_PREFIX = "deepClone";

  /**
   * The singleton instance of the package.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  DeepClonePackage eINSTANCE = my.home.dsl.deepClone.impl.DeepClonePackageImpl.init();

  /**
   * The meta object id for the '{@link my.home.dsl.deepClone.impl.ModelImpl <em>Model</em>}' class.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @see my.home.dsl.deepClone.impl.ModelImpl
   * @see my.home.dsl.deepClone.impl.DeepClonePackageImpl#getModel()
   * @generated
   */
  int MODEL = 0;

  /**
   * The number of structural features of the '<em>Model</em>' class.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   * @ordered
   */
  int MODEL_FEATURE_COUNT = 0;

  /**
   * The meta object id for the '{@link my.home.dsl.deepClone.impl.BodyImpl <em>Body</em>}' class.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @see my.home.dsl.deepClone.impl.BodyImpl
   * @see my.home.dsl.deepClone.impl.DeepClonePackageImpl#getBody()
   * @generated
   */
  int BODY = 1;

  /**
   * The feature id for the '<em><b>Package Config</b></em>' containment reference.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   * @ordered
   */
  int BODY__PACKAGE_CONFIG = MODEL_FEATURE_COUNT + 0;

  /**
   * The feature id for the '<em><b>Cloners</b></em>' containment reference list.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   * @ordered
   */
  int BODY__CLONERS = MODEL_FEATURE_COUNT + 1;

  /**
   * The number of structural features of the '<em>Body</em>' class.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   * @ordered
   */
  int BODY_FEATURE_COUNT = MODEL_FEATURE_COUNT + 2;

  /**
   * The meta object id for the '{@link my.home.dsl.deepClone.impl.PackageConfigImpl <em>Package Config</em>}' class.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @see my.home.dsl.deepClone.impl.PackageConfigImpl
   * @see my.home.dsl.deepClone.impl.DeepClonePackageImpl#getPackageConfig()
   * @generated
   */
  int PACKAGE_CONFIG = 2;

  /**
   * The feature id for the '<em><b>Name</b></em>' attribute.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   * @ordered
   */
  int PACKAGE_CONFIG__NAME = 0;

  /**
   * The number of structural features of the '<em>Package Config</em>' class.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   * @ordered
   */
  int PACKAGE_CONFIG_FEATURE_COUNT = 1;

  /**
   * The meta object id for the '{@link my.home.dsl.deepClone.impl.BaseTypeImpl <em>Base Type</em>}' class.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @see my.home.dsl.deepClone.impl.BaseTypeImpl
   * @see my.home.dsl.deepClone.impl.DeepClonePackageImpl#getBaseType()
   * @generated
   */
  int BASE_TYPE = 9;

  /**
   * The feature id for the '<em><b>Java Type</b></em>' containment reference.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   * @ordered
   */
  int BASE_TYPE__JAVA_TYPE = 0;

  /**
   * The number of structural features of the '<em>Base Type</em>' class.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   * @ordered
   */
  int BASE_TYPE_FEATURE_COUNT = 1;

  /**
   * The meta object id for the '{@link my.home.dsl.deepClone.impl.ContainerTypeImpl <em>Container Type</em>}' class.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @see my.home.dsl.deepClone.impl.ContainerTypeImpl
   * @see my.home.dsl.deepClone.impl.DeepClonePackageImpl#getContainerType()
   * @generated
   */
  int CONTAINER_TYPE = 10;

  /**
   * The feature id for the '<em><b>Java Type</b></em>' containment reference.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   * @ordered
   */
  int CONTAINER_TYPE__JAVA_TYPE = BASE_TYPE__JAVA_TYPE;

  /**
   * The feature id for the '<em><b>Fields</b></em>' containment reference list.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   * @ordered
   */
  int CONTAINER_TYPE__FIELDS = BASE_TYPE_FEATURE_COUNT + 0;

  /**
   * The number of structural features of the '<em>Container Type</em>' class.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   * @ordered
   */
  int CONTAINER_TYPE_FEATURE_COUNT = BASE_TYPE_FEATURE_COUNT + 1;

  /**
   * The meta object id for the '{@link my.home.dsl.deepClone.impl.ClassClonerImpl <em>Class Cloner</em>}' class.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @see my.home.dsl.deepClone.impl.ClassClonerImpl
   * @see my.home.dsl.deepClone.impl.DeepClonePackageImpl#getClassCloner()
   * @generated
   */
  int CLASS_CLONER = 3;

  /**
   * The feature id for the '<em><b>Java Type</b></em>' containment reference.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   * @ordered
   */
  int CLASS_CLONER__JAVA_TYPE = CONTAINER_TYPE__JAVA_TYPE;

  /**
   * The feature id for the '<em><b>Fields</b></em>' containment reference list.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   * @ordered
   */
  int CLASS_CLONER__FIELDS = CONTAINER_TYPE__FIELDS;

  /**
   * The feature id for the '<em><b>Class To Clone</b></em>' containment reference.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   * @ordered
   */
  int CLASS_CLONER__CLASS_TO_CLONE = CONTAINER_TYPE_FEATURE_COUNT + 0;

  /**
   * The feature id for the '<em><b>Name</b></em>' attribute.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   * @ordered
   */
  int CLASS_CLONER__NAME = CONTAINER_TYPE_FEATURE_COUNT + 1;

  /**
   * The number of structural features of the '<em>Class Cloner</em>' class.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   * @ordered
   */
  int CLASS_CLONER_FEATURE_COUNT = CONTAINER_TYPE_FEATURE_COUNT + 2;

  /**
   * The meta object id for the '{@link my.home.dsl.deepClone.impl.FieldClonerTypeImpl <em>Field Cloner Type</em>}' class.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @see my.home.dsl.deepClone.impl.FieldClonerTypeImpl
   * @see my.home.dsl.deepClone.impl.DeepClonePackageImpl#getFieldClonerType()
   * @generated
   */
  int FIELD_CLONER_TYPE = 4;

  /**
   * The feature id for the '<em><b>Java Type</b></em>' containment reference.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   * @ordered
   */
  int FIELD_CLONER_TYPE__JAVA_TYPE = BASE_TYPE__JAVA_TYPE;

  /**
   * The feature id for the '<em><b>Field Name</b></em>' attribute.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   * @ordered
   */
  int FIELD_CLONER_TYPE__FIELD_NAME = BASE_TYPE_FEATURE_COUNT + 0;

  /**
   * The number of structural features of the '<em>Field Cloner Type</em>' class.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   * @ordered
   */
  int FIELD_CLONER_TYPE_FEATURE_COUNT = BASE_TYPE_FEATURE_COUNT + 1;

  /**
   * The meta object id for the '{@link my.home.dsl.deepClone.impl.SimpleFieldImpl <em>Simple Field</em>}' class.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @see my.home.dsl.deepClone.impl.SimpleFieldImpl
   * @see my.home.dsl.deepClone.impl.DeepClonePackageImpl#getSimpleField()
   * @generated
   */
  int SIMPLE_FIELD = 5;

  /**
   * The feature id for the '<em><b>Java Type</b></em>' containment reference.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   * @ordered
   */
  int SIMPLE_FIELD__JAVA_TYPE = FIELD_CLONER_TYPE__JAVA_TYPE;

  /**
   * The feature id for the '<em><b>Field Name</b></em>' attribute.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   * @ordered
   */
  int SIMPLE_FIELD__FIELD_NAME = FIELD_CLONER_TYPE__FIELD_NAME;

  /**
   * The number of structural features of the '<em>Simple Field</em>' class.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   * @ordered
   */
  int SIMPLE_FIELD_FEATURE_COUNT = FIELD_CLONER_TYPE_FEATURE_COUNT + 0;

  /**
   * The meta object id for the '{@link my.home.dsl.deepClone.impl.SimpleExcludedFieldImpl <em>Simple Excluded Field</em>}' class.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @see my.home.dsl.deepClone.impl.SimpleExcludedFieldImpl
   * @see my.home.dsl.deepClone.impl.DeepClonePackageImpl#getSimpleExcludedField()
   * @generated
   */
  int SIMPLE_EXCLUDED_FIELD = 6;

  /**
   * The feature id for the '<em><b>Java Type</b></em>' containment reference.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   * @ordered
   */
  int SIMPLE_EXCLUDED_FIELD__JAVA_TYPE = FIELD_CLONER_TYPE__JAVA_TYPE;

  /**
   * The feature id for the '<em><b>Field Name</b></em>' attribute.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   * @ordered
   */
  int SIMPLE_EXCLUDED_FIELD__FIELD_NAME = FIELD_CLONER_TYPE__FIELD_NAME;

  /**
   * The number of structural features of the '<em>Simple Excluded Field</em>' class.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   * @ordered
   */
  int SIMPLE_EXCLUDED_FIELD_FEATURE_COUNT = FIELD_CLONER_TYPE_FEATURE_COUNT + 0;

  /**
   * The meta object id for the '{@link my.home.dsl.deepClone.impl.ComplexFieldImpl <em>Complex Field</em>}' class.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @see my.home.dsl.deepClone.impl.ComplexFieldImpl
   * @see my.home.dsl.deepClone.impl.DeepClonePackageImpl#getComplexField()
   * @generated
   */
  int COMPLEX_FIELD = 7;

  /**
   * The feature id for the '<em><b>Java Type</b></em>' containment reference.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   * @ordered
   */
  int COMPLEX_FIELD__JAVA_TYPE = FIELD_CLONER_TYPE__JAVA_TYPE;

  /**
   * The feature id for the '<em><b>Field Name</b></em>' attribute.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   * @ordered
   */
  int COMPLEX_FIELD__FIELD_NAME = FIELD_CLONER_TYPE__FIELD_NAME;

  /**
   * The feature id for the '<em><b>Fields</b></em>' containment reference list.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   * @ordered
   */
  int COMPLEX_FIELD__FIELDS = FIELD_CLONER_TYPE_FEATURE_COUNT + 0;

  /**
   * The number of structural features of the '<em>Complex Field</em>' class.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   * @ordered
   */
  int COMPLEX_FIELD_FEATURE_COUNT = FIELD_CLONER_TYPE_FEATURE_COUNT + 1;

  /**
   * The meta object id for the '{@link my.home.dsl.deepClone.impl.ReferenceFieldImpl <em>Reference Field</em>}' class.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @see my.home.dsl.deepClone.impl.ReferenceFieldImpl
   * @see my.home.dsl.deepClone.impl.DeepClonePackageImpl#getReferenceField()
   * @generated
   */
  int REFERENCE_FIELD = 8;

  /**
   * The feature id for the '<em><b>Java Type</b></em>' containment reference.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   * @ordered
   */
  int REFERENCE_FIELD__JAVA_TYPE = FIELD_CLONER_TYPE__JAVA_TYPE;

  /**
   * The feature id for the '<em><b>Field Name</b></em>' attribute.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   * @ordered
   */
  int REFERENCE_FIELD__FIELD_NAME = FIELD_CLONER_TYPE__FIELD_NAME;

  /**
   * The feature id for the '<em><b>Cloner Reference</b></em>' reference.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   * @ordered
   */
  int REFERENCE_FIELD__CLONER_REFERENCE = FIELD_CLONER_TYPE_FEATURE_COUNT + 0;

  /**
   * The number of structural features of the '<em>Reference Field</em>' class.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   * @ordered
   */
  int REFERENCE_FIELD_FEATURE_COUNT = FIELD_CLONER_TYPE_FEATURE_COUNT + 1;


  /**
   * Returns the meta object for class '{@link my.home.dsl.deepClone.Model <em>Model</em>}'.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @return the meta object for class '<em>Model</em>'.
   * @see my.home.dsl.deepClone.Model
   * @generated
   */
  EClass getModel();

  /**
   * Returns the meta object for class '{@link my.home.dsl.deepClone.Body <em>Body</em>}'.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @return the meta object for class '<em>Body</em>'.
   * @see my.home.dsl.deepClone.Body
   * @generated
   */
  EClass getBody();

  /**
   * Returns the meta object for the containment reference '{@link my.home.dsl.deepClone.Body#getPackageConfig <em>Package Config</em>}'.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @return the meta object for the containment reference '<em>Package Config</em>'.
   * @see my.home.dsl.deepClone.Body#getPackageConfig()
   * @see #getBody()
   * @generated
   */
  EReference getBody_PackageConfig();

  /**
   * Returns the meta object for the containment reference list '{@link my.home.dsl.deepClone.Body#getCloners <em>Cloners</em>}'.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @return the meta object for the containment reference list '<em>Cloners</em>'.
   * @see my.home.dsl.deepClone.Body#getCloners()
   * @see #getBody()
   * @generated
   */
  EReference getBody_Cloners();

  /**
   * Returns the meta object for class '{@link my.home.dsl.deepClone.PackageConfig <em>Package Config</em>}'.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @return the meta object for class '<em>Package Config</em>'.
   * @see my.home.dsl.deepClone.PackageConfig
   * @generated
   */
  EClass getPackageConfig();

  /**
   * Returns the meta object for the attribute '{@link my.home.dsl.deepClone.PackageConfig#getName <em>Name</em>}'.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @return the meta object for the attribute '<em>Name</em>'.
   * @see my.home.dsl.deepClone.PackageConfig#getName()
   * @see #getPackageConfig()
   * @generated
   */
  EAttribute getPackageConfig_Name();

  /**
   * Returns the meta object for class '{@link my.home.dsl.deepClone.ClassCloner <em>Class Cloner</em>}'.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @return the meta object for class '<em>Class Cloner</em>'.
   * @see my.home.dsl.deepClone.ClassCloner
   * @generated
   */
  EClass getClassCloner();

  /**
   * Returns the meta object for the containment reference '{@link my.home.dsl.deepClone.ClassCloner#getClassToClone <em>Class To Clone</em>}'.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @return the meta object for the containment reference '<em>Class To Clone</em>'.
   * @see my.home.dsl.deepClone.ClassCloner#getClassToClone()
   * @see #getClassCloner()
   * @generated
   */
  EReference getClassCloner_ClassToClone();

  /**
   * Returns the meta object for the attribute '{@link my.home.dsl.deepClone.ClassCloner#getName <em>Name</em>}'.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @return the meta object for the attribute '<em>Name</em>'.
   * @see my.home.dsl.deepClone.ClassCloner#getName()
   * @see #getClassCloner()
   * @generated
   */
  EAttribute getClassCloner_Name();

  /**
   * Returns the meta object for class '{@link my.home.dsl.deepClone.FieldClonerType <em>Field Cloner Type</em>}'.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @return the meta object for class '<em>Field Cloner Type</em>'.
   * @see my.home.dsl.deepClone.FieldClonerType
   * @generated
   */
  EClass getFieldClonerType();

  /**
   * Returns the meta object for the attribute '{@link my.home.dsl.deepClone.FieldClonerType#getFieldName <em>Field Name</em>}'.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @return the meta object for the attribute '<em>Field Name</em>'.
   * @see my.home.dsl.deepClone.FieldClonerType#getFieldName()
   * @see #getFieldClonerType()
   * @generated
   */
  EAttribute getFieldClonerType_FieldName();

  /**
   * Returns the meta object for class '{@link my.home.dsl.deepClone.SimpleField <em>Simple Field</em>}'.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @return the meta object for class '<em>Simple Field</em>'.
   * @see my.home.dsl.deepClone.SimpleField
   * @generated
   */
  EClass getSimpleField();

  /**
   * Returns the meta object for class '{@link my.home.dsl.deepClone.SimpleExcludedField <em>Simple Excluded Field</em>}'.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @return the meta object for class '<em>Simple Excluded Field</em>'.
   * @see my.home.dsl.deepClone.SimpleExcludedField
   * @generated
   */
  EClass getSimpleExcludedField();

  /**
   * Returns the meta object for class '{@link my.home.dsl.deepClone.ComplexField <em>Complex Field</em>}'.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @return the meta object for class '<em>Complex Field</em>'.
   * @see my.home.dsl.deepClone.ComplexField
   * @generated
   */
  EClass getComplexField();

  /**
   * Returns the meta object for class '{@link my.home.dsl.deepClone.ReferenceField <em>Reference Field</em>}'.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @return the meta object for class '<em>Reference Field</em>'.
   * @see my.home.dsl.deepClone.ReferenceField
   * @generated
   */
  EClass getReferenceField();

  /**
   * Returns the meta object for the reference '{@link my.home.dsl.deepClone.ReferenceField#getClonerReference <em>Cloner Reference</em>}'.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @return the meta object for the reference '<em>Cloner Reference</em>'.
   * @see my.home.dsl.deepClone.ReferenceField#getClonerReference()
   * @see #getReferenceField()
   * @generated
   */
  EReference getReferenceField_ClonerReference();

  /**
   * Returns the meta object for class '{@link my.home.dsl.deepClone.BaseType <em>Base Type</em>}'.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @return the meta object for class '<em>Base Type</em>'.
   * @see my.home.dsl.deepClone.BaseType
   * @generated
   */
  EClass getBaseType();

  /**
   * Returns the meta object for the containment reference '{@link my.home.dsl.deepClone.BaseType#getJavaType <em>Java Type</em>}'.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @return the meta object for the containment reference '<em>Java Type</em>'.
   * @see my.home.dsl.deepClone.BaseType#getJavaType()
   * @see #getBaseType()
   * @generated
   */
  EReference getBaseType_JavaType();

  /**
   * Returns the meta object for class '{@link my.home.dsl.deepClone.ContainerType <em>Container Type</em>}'.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @return the meta object for class '<em>Container Type</em>'.
   * @see my.home.dsl.deepClone.ContainerType
   * @generated
   */
  EClass getContainerType();

  /**
   * Returns the meta object for the containment reference list '{@link my.home.dsl.deepClone.ContainerType#getFields <em>Fields</em>}'.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @return the meta object for the containment reference list '<em>Fields</em>'.
   * @see my.home.dsl.deepClone.ContainerType#getFields()
   * @see #getContainerType()
   * @generated
   */
  EReference getContainerType_Fields();

  /**
   * Returns the factory that creates the instances of the model.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @return the factory that creates the instances of the model.
   * @generated
   */
  DeepCloneFactory getDeepCloneFactory();

  /**
   * <!-- begin-user-doc -->
   * Defines literals for the meta objects that represent
   * <ul>
   *   <li>each class,</li>
   *   <li>each feature of each class,</li>
   *   <li>each enum,</li>
   *   <li>and each data type</li>
   * </ul>
   * <!-- end-user-doc -->
   * @generated
   */
  interface Literals
  {
    /**
     * The meta object literal for the '{@link my.home.dsl.deepClone.impl.ModelImpl <em>Model</em>}' class.
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * @see my.home.dsl.deepClone.impl.ModelImpl
     * @see my.home.dsl.deepClone.impl.DeepClonePackageImpl#getModel()
     * @generated
     */
    EClass MODEL = eINSTANCE.getModel();

    /**
     * The meta object literal for the '{@link my.home.dsl.deepClone.impl.BodyImpl <em>Body</em>}' class.
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * @see my.home.dsl.deepClone.impl.BodyImpl
     * @see my.home.dsl.deepClone.impl.DeepClonePackageImpl#getBody()
     * @generated
     */
    EClass BODY = eINSTANCE.getBody();

    /**
     * The meta object literal for the '<em><b>Package Config</b></em>' containment reference feature.
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * @generated
     */
    EReference BODY__PACKAGE_CONFIG = eINSTANCE.getBody_PackageConfig();

    /**
     * The meta object literal for the '<em><b>Cloners</b></em>' containment reference list feature.
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * @generated
     */
    EReference BODY__CLONERS = eINSTANCE.getBody_Cloners();

    /**
     * The meta object literal for the '{@link my.home.dsl.deepClone.impl.PackageConfigImpl <em>Package Config</em>}' class.
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * @see my.home.dsl.deepClone.impl.PackageConfigImpl
     * @see my.home.dsl.deepClone.impl.DeepClonePackageImpl#getPackageConfig()
     * @generated
     */
    EClass PACKAGE_CONFIG = eINSTANCE.getPackageConfig();

    /**
     * The meta object literal for the '<em><b>Name</b></em>' attribute feature.
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * @generated
     */
    EAttribute PACKAGE_CONFIG__NAME = eINSTANCE.getPackageConfig_Name();

    /**
     * The meta object literal for the '{@link my.home.dsl.deepClone.impl.ClassClonerImpl <em>Class Cloner</em>}' class.
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * @see my.home.dsl.deepClone.impl.ClassClonerImpl
     * @see my.home.dsl.deepClone.impl.DeepClonePackageImpl#getClassCloner()
     * @generated
     */
    EClass CLASS_CLONER = eINSTANCE.getClassCloner();

    /**
     * The meta object literal for the '<em><b>Class To Clone</b></em>' containment reference feature.
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * @generated
     */
    EReference CLASS_CLONER__CLASS_TO_CLONE = eINSTANCE.getClassCloner_ClassToClone();

    /**
     * The meta object literal for the '<em><b>Name</b></em>' attribute feature.
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * @generated
     */
    EAttribute CLASS_CLONER__NAME = eINSTANCE.getClassCloner_Name();

    /**
     * The meta object literal for the '{@link my.home.dsl.deepClone.impl.FieldClonerTypeImpl <em>Field Cloner Type</em>}' class.
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * @see my.home.dsl.deepClone.impl.FieldClonerTypeImpl
     * @see my.home.dsl.deepClone.impl.DeepClonePackageImpl#getFieldClonerType()
     * @generated
     */
    EClass FIELD_CLONER_TYPE = eINSTANCE.getFieldClonerType();

    /**
     * The meta object literal for the '<em><b>Field Name</b></em>' attribute feature.
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * @generated
     */
    EAttribute FIELD_CLONER_TYPE__FIELD_NAME = eINSTANCE.getFieldClonerType_FieldName();

    /**
     * The meta object literal for the '{@link my.home.dsl.deepClone.impl.SimpleFieldImpl <em>Simple Field</em>}' class.
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * @see my.home.dsl.deepClone.impl.SimpleFieldImpl
     * @see my.home.dsl.deepClone.impl.DeepClonePackageImpl#getSimpleField()
     * @generated
     */
    EClass SIMPLE_FIELD = eINSTANCE.getSimpleField();

    /**
     * The meta object literal for the '{@link my.home.dsl.deepClone.impl.SimpleExcludedFieldImpl <em>Simple Excluded Field</em>}' class.
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * @see my.home.dsl.deepClone.impl.SimpleExcludedFieldImpl
     * @see my.home.dsl.deepClone.impl.DeepClonePackageImpl#getSimpleExcludedField()
     * @generated
     */
    EClass SIMPLE_EXCLUDED_FIELD = eINSTANCE.getSimpleExcludedField();

    /**
     * The meta object literal for the '{@link my.home.dsl.deepClone.impl.ComplexFieldImpl <em>Complex Field</em>}' class.
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * @see my.home.dsl.deepClone.impl.ComplexFieldImpl
     * @see my.home.dsl.deepClone.impl.DeepClonePackageImpl#getComplexField()
     * @generated
     */
    EClass COMPLEX_FIELD = eINSTANCE.getComplexField();

    /**
     * The meta object literal for the '{@link my.home.dsl.deepClone.impl.ReferenceFieldImpl <em>Reference Field</em>}' class.
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * @see my.home.dsl.deepClone.impl.ReferenceFieldImpl
     * @see my.home.dsl.deepClone.impl.DeepClonePackageImpl#getReferenceField()
     * @generated
     */
    EClass REFERENCE_FIELD = eINSTANCE.getReferenceField();

    /**
     * The meta object literal for the '<em><b>Cloner Reference</b></em>' reference feature.
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * @generated
     */
    EReference REFERENCE_FIELD__CLONER_REFERENCE = eINSTANCE.getReferenceField_ClonerReference();

    /**
     * The meta object literal for the '{@link my.home.dsl.deepClone.impl.BaseTypeImpl <em>Base Type</em>}' class.
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * @see my.home.dsl.deepClone.impl.BaseTypeImpl
     * @see my.home.dsl.deepClone.impl.DeepClonePackageImpl#getBaseType()
     * @generated
     */
    EClass BASE_TYPE = eINSTANCE.getBaseType();

    /**
     * The meta object literal for the '<em><b>Java Type</b></em>' containment reference feature.
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * @generated
     */
    EReference BASE_TYPE__JAVA_TYPE = eINSTANCE.getBaseType_JavaType();

    /**
     * The meta object literal for the '{@link my.home.dsl.deepClone.impl.ContainerTypeImpl <em>Container Type</em>}' class.
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * @see my.home.dsl.deepClone.impl.ContainerTypeImpl
     * @see my.home.dsl.deepClone.impl.DeepClonePackageImpl#getContainerType()
     * @generated
     */
    EClass CONTAINER_TYPE = eINSTANCE.getContainerType();

    /**
     * The meta object literal for the '<em><b>Fields</b></em>' containment reference list feature.
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * @generated
     */
    EReference CONTAINER_TYPE__FIELDS = eINSTANCE.getContainerType_Fields();

  }

} //DeepClonePackage
