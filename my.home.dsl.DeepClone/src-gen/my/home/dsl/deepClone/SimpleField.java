/**
 */
package my.home.dsl.deepClone;


/**
 * <!-- begin-user-doc -->
 * A representation of the model object '<em><b>Simple Field</b></em>'.
 * <!-- end-user-doc -->
 *
 *
 * @see my.home.dsl.deepClone.DeepClonePackage#getSimpleField()
 * @model
 * @generated
 */
public interface SimpleField extends FieldClonerType
{
} // SimpleField
