/**
 */
package my.home.dsl.deepClone;


/**
 * <!-- begin-user-doc -->
 * A representation of the model object '<em><b>Complex Field</b></em>'.
 * <!-- end-user-doc -->
 *
 *
 * @see my.home.dsl.deepClone.DeepClonePackage#getComplexField()
 * @model
 * @generated
 */
public interface ComplexField extends FieldClonerType, ContainerType
{
} // ComplexField
