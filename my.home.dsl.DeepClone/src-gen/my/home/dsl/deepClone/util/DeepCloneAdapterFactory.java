/**
 */
package my.home.dsl.deepClone.util;

import my.home.dsl.deepClone.*;

import org.eclipse.emf.common.notify.Adapter;
import org.eclipse.emf.common.notify.Notifier;

import org.eclipse.emf.common.notify.impl.AdapterFactoryImpl;

import org.eclipse.emf.ecore.EObject;

/**
 * <!-- begin-user-doc -->
 * The <b>Adapter Factory</b> for the model.
 * It provides an adapter <code>createXXX</code> method for each class of the model.
 * <!-- end-user-doc -->
 * @see my.home.dsl.deepClone.DeepClonePackage
 * @generated
 */
public class DeepCloneAdapterFactory extends AdapterFactoryImpl
{
  /**
   * The cached model package.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  protected static DeepClonePackage modelPackage;

  /**
   * Creates an instance of the adapter factory.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public DeepCloneAdapterFactory()
  {
    if (modelPackage == null)
    {
      modelPackage = DeepClonePackage.eINSTANCE;
    }
  }

  /**
   * Returns whether this factory is applicable for the type of the object.
   * <!-- begin-user-doc -->
   * This implementation returns <code>true</code> if the object is either the model's package or is an instance object of the model.
   * <!-- end-user-doc -->
   * @return whether this factory is applicable for the type of the object.
   * @generated
   */
  @Override
  public boolean isFactoryForType(Object object)
  {
    if (object == modelPackage)
    {
      return true;
    }
    if (object instanceof EObject)
    {
      return ((EObject)object).eClass().getEPackage() == modelPackage;
    }
    return false;
  }

  /**
   * The switch that delegates to the <code>createXXX</code> methods.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  protected DeepCloneSwitch<Adapter> modelSwitch =
    new DeepCloneSwitch<Adapter>()
    {
      @Override
      public Adapter caseModel(Model object)
      {
        return createModelAdapter();
      }
      @Override
      public Adapter caseBody(Body object)
      {
        return createBodyAdapter();
      }
      @Override
      public Adapter casePackageConfig(PackageConfig object)
      {
        return createPackageConfigAdapter();
      }
      @Override
      public Adapter caseClassCloner(ClassCloner object)
      {
        return createClassClonerAdapter();
      }
      @Override
      public Adapter caseFieldClonerType(FieldClonerType object)
      {
        return createFieldClonerTypeAdapter();
      }
      @Override
      public Adapter caseSimpleField(SimpleField object)
      {
        return createSimpleFieldAdapter();
      }
      @Override
      public Adapter caseSimpleExcludedField(SimpleExcludedField object)
      {
        return createSimpleExcludedFieldAdapter();
      }
      @Override
      public Adapter caseComplexField(ComplexField object)
      {
        return createComplexFieldAdapter();
      }
      @Override
      public Adapter caseReferenceField(ReferenceField object)
      {
        return createReferenceFieldAdapter();
      }
      @Override
      public Adapter caseBaseType(BaseType object)
      {
        return createBaseTypeAdapter();
      }
      @Override
      public Adapter caseContainerType(ContainerType object)
      {
        return createContainerTypeAdapter();
      }
      @Override
      public Adapter defaultCase(EObject object)
      {
        return createEObjectAdapter();
      }
    };

  /**
   * Creates an adapter for the <code>target</code>.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @param target the object to adapt.
   * @return the adapter for the <code>target</code>.
   * @generated
   */
  @Override
  public Adapter createAdapter(Notifier target)
  {
    return modelSwitch.doSwitch((EObject)target);
  }


  /**
   * Creates a new adapter for an object of class '{@link my.home.dsl.deepClone.Model <em>Model</em>}'.
   * <!-- begin-user-doc -->
   * This default implementation returns null so that we can easily ignore cases;
   * it's useful to ignore a case when inheritance will catch all the cases anyway.
   * <!-- end-user-doc -->
   * @return the new adapter.
   * @see my.home.dsl.deepClone.Model
   * @generated
   */
  public Adapter createModelAdapter()
  {
    return null;
  }

  /**
   * Creates a new adapter for an object of class '{@link my.home.dsl.deepClone.Body <em>Body</em>}'.
   * <!-- begin-user-doc -->
   * This default implementation returns null so that we can easily ignore cases;
   * it's useful to ignore a case when inheritance will catch all the cases anyway.
   * <!-- end-user-doc -->
   * @return the new adapter.
   * @see my.home.dsl.deepClone.Body
   * @generated
   */
  public Adapter createBodyAdapter()
  {
    return null;
  }

  /**
   * Creates a new adapter for an object of class '{@link my.home.dsl.deepClone.PackageConfig <em>Package Config</em>}'.
   * <!-- begin-user-doc -->
   * This default implementation returns null so that we can easily ignore cases;
   * it's useful to ignore a case when inheritance will catch all the cases anyway.
   * <!-- end-user-doc -->
   * @return the new adapter.
   * @see my.home.dsl.deepClone.PackageConfig
   * @generated
   */
  public Adapter createPackageConfigAdapter()
  {
    return null;
  }

  /**
   * Creates a new adapter for an object of class '{@link my.home.dsl.deepClone.ClassCloner <em>Class Cloner</em>}'.
   * <!-- begin-user-doc -->
   * This default implementation returns null so that we can easily ignore cases;
   * it's useful to ignore a case when inheritance will catch all the cases anyway.
   * <!-- end-user-doc -->
   * @return the new adapter.
   * @see my.home.dsl.deepClone.ClassCloner
   * @generated
   */
  public Adapter createClassClonerAdapter()
  {
    return null;
  }

  /**
   * Creates a new adapter for an object of class '{@link my.home.dsl.deepClone.FieldClonerType <em>Field Cloner Type</em>}'.
   * <!-- begin-user-doc -->
   * This default implementation returns null so that we can easily ignore cases;
   * it's useful to ignore a case when inheritance will catch all the cases anyway.
   * <!-- end-user-doc -->
   * @return the new adapter.
   * @see my.home.dsl.deepClone.FieldClonerType
   * @generated
   */
  public Adapter createFieldClonerTypeAdapter()
  {
    return null;
  }

  /**
   * Creates a new adapter for an object of class '{@link my.home.dsl.deepClone.SimpleField <em>Simple Field</em>}'.
   * <!-- begin-user-doc -->
   * This default implementation returns null so that we can easily ignore cases;
   * it's useful to ignore a case when inheritance will catch all the cases anyway.
   * <!-- end-user-doc -->
   * @return the new adapter.
   * @see my.home.dsl.deepClone.SimpleField
   * @generated
   */
  public Adapter createSimpleFieldAdapter()
  {
    return null;
  }

  /**
   * Creates a new adapter for an object of class '{@link my.home.dsl.deepClone.SimpleExcludedField <em>Simple Excluded Field</em>}'.
   * <!-- begin-user-doc -->
   * This default implementation returns null so that we can easily ignore cases;
   * it's useful to ignore a case when inheritance will catch all the cases anyway.
   * <!-- end-user-doc -->
   * @return the new adapter.
   * @see my.home.dsl.deepClone.SimpleExcludedField
   * @generated
   */
  public Adapter createSimpleExcludedFieldAdapter()
  {
    return null;
  }

  /**
   * Creates a new adapter for an object of class '{@link my.home.dsl.deepClone.ComplexField <em>Complex Field</em>}'.
   * <!-- begin-user-doc -->
   * This default implementation returns null so that we can easily ignore cases;
   * it's useful to ignore a case when inheritance will catch all the cases anyway.
   * <!-- end-user-doc -->
   * @return the new adapter.
   * @see my.home.dsl.deepClone.ComplexField
   * @generated
   */
  public Adapter createComplexFieldAdapter()
  {
    return null;
  }

  /**
   * Creates a new adapter for an object of class '{@link my.home.dsl.deepClone.ReferenceField <em>Reference Field</em>}'.
   * <!-- begin-user-doc -->
   * This default implementation returns null so that we can easily ignore cases;
   * it's useful to ignore a case when inheritance will catch all the cases anyway.
   * <!-- end-user-doc -->
   * @return the new adapter.
   * @see my.home.dsl.deepClone.ReferenceField
   * @generated
   */
  public Adapter createReferenceFieldAdapter()
  {
    return null;
  }

  /**
   * Creates a new adapter for an object of class '{@link my.home.dsl.deepClone.BaseType <em>Base Type</em>}'.
   * <!-- begin-user-doc -->
   * This default implementation returns null so that we can easily ignore cases;
   * it's useful to ignore a case when inheritance will catch all the cases anyway.
   * <!-- end-user-doc -->
   * @return the new adapter.
   * @see my.home.dsl.deepClone.BaseType
   * @generated
   */
  public Adapter createBaseTypeAdapter()
  {
    return null;
  }

  /**
   * Creates a new adapter for an object of class '{@link my.home.dsl.deepClone.ContainerType <em>Container Type</em>}'.
   * <!-- begin-user-doc -->
   * This default implementation returns null so that we can easily ignore cases;
   * it's useful to ignore a case when inheritance will catch all the cases anyway.
   * <!-- end-user-doc -->
   * @return the new adapter.
   * @see my.home.dsl.deepClone.ContainerType
   * @generated
   */
  public Adapter createContainerTypeAdapter()
  {
    return null;
  }

  /**
   * Creates a new adapter for the default case.
   * <!-- begin-user-doc -->
   * This default implementation returns null.
   * <!-- end-user-doc -->
   * @return the new adapter.
   * @generated
   */
  public Adapter createEObjectAdapter()
  {
    return null;
  }

} //DeepCloneAdapterFactory
