/**
 */
package my.home.dsl.deepClone;


/**
 * <!-- begin-user-doc -->
 * A representation of the model object '<em><b>Reference Field</b></em>'.
 * <!-- end-user-doc -->
 *
 * <p>
 * The following features are supported:
 * <ul>
 *   <li>{@link my.home.dsl.deepClone.ReferenceField#getClonerReference <em>Cloner Reference</em>}</li>
 * </ul>
 * </p>
 *
 * @see my.home.dsl.deepClone.DeepClonePackage#getReferenceField()
 * @model
 * @generated
 */
public interface ReferenceField extends FieldClonerType
{
  /**
   * Returns the value of the '<em><b>Cloner Reference</b></em>' reference.
   * <!-- begin-user-doc -->
   * <p>
   * If the meaning of the '<em>Cloner Reference</em>' reference isn't clear,
   * there really should be more of a description here...
   * </p>
   * <!-- end-user-doc -->
   * @return the value of the '<em>Cloner Reference</em>' reference.
   * @see #setClonerReference(ClassCloner)
   * @see my.home.dsl.deepClone.DeepClonePackage#getReferenceField_ClonerReference()
   * @model
   * @generated
   */
  ClassCloner getClonerReference();

  /**
   * Sets the value of the '{@link my.home.dsl.deepClone.ReferenceField#getClonerReference <em>Cloner Reference</em>}' reference.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @param value the new value of the '<em>Cloner Reference</em>' reference.
   * @see #getClonerReference()
   * @generated
   */
  void setClonerReference(ClassCloner value);

} // ReferenceField
