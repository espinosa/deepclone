/**
 */
package my.home.dsl.deepClone;

import org.eclipse.emf.common.util.EList;

/**
 * <!-- begin-user-doc -->
 * A representation of the model object '<em><b>Body</b></em>'.
 * <!-- end-user-doc -->
 *
 * <p>
 * The following features are supported:
 * <ul>
 *   <li>{@link my.home.dsl.deepClone.Body#getPackageConfig <em>Package Config</em>}</li>
 *   <li>{@link my.home.dsl.deepClone.Body#getCloners <em>Cloners</em>}</li>
 * </ul>
 * </p>
 *
 * @see my.home.dsl.deepClone.DeepClonePackage#getBody()
 * @model
 * @generated
 */
public interface Body extends Model
{
  /**
   * Returns the value of the '<em><b>Package Config</b></em>' containment reference.
   * <!-- begin-user-doc -->
   * <p>
   * If the meaning of the '<em>Package Config</em>' containment reference isn't clear,
   * there really should be more of a description here...
   * </p>
   * <!-- end-user-doc -->
   * @return the value of the '<em>Package Config</em>' containment reference.
   * @see #setPackageConfig(PackageConfig)
   * @see my.home.dsl.deepClone.DeepClonePackage#getBody_PackageConfig()
   * @model containment="true"
   * @generated
   */
  PackageConfig getPackageConfig();

  /**
   * Sets the value of the '{@link my.home.dsl.deepClone.Body#getPackageConfig <em>Package Config</em>}' containment reference.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @param value the new value of the '<em>Package Config</em>' containment reference.
   * @see #getPackageConfig()
   * @generated
   */
  void setPackageConfig(PackageConfig value);

  /**
   * Returns the value of the '<em><b>Cloners</b></em>' containment reference list.
   * The list contents are of type {@link my.home.dsl.deepClone.ClassCloner}.
   * <!-- begin-user-doc -->
   * <p>
   * If the meaning of the '<em>Cloners</em>' containment reference list isn't clear,
   * there really should be more of a description here...
   * </p>
   * <!-- end-user-doc -->
   * @return the value of the '<em>Cloners</em>' containment reference list.
   * @see my.home.dsl.deepClone.DeepClonePackage#getBody_Cloners()
   * @model containment="true"
   * @generated
   */
  EList<ClassCloner> getCloners();

} // Body
