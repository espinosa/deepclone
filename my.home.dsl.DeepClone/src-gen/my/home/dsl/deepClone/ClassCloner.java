/**
 */
package my.home.dsl.deepClone;

import org.eclipse.xtext.common.types.JvmTypeReference;

/**
 * <!-- begin-user-doc -->
 * A representation of the model object '<em><b>Class Cloner</b></em>'.
 * <!-- end-user-doc -->
 *
 * <p>
 * The following features are supported:
 * <ul>
 *   <li>{@link my.home.dsl.deepClone.ClassCloner#getClassToClone <em>Class To Clone</em>}</li>
 *   <li>{@link my.home.dsl.deepClone.ClassCloner#getName <em>Name</em>}</li>
 * </ul>
 * </p>
 *
 * @see my.home.dsl.deepClone.DeepClonePackage#getClassCloner()
 * @model
 * @generated
 */
public interface ClassCloner extends ContainerType
{
  /**
   * Returns the value of the '<em><b>Class To Clone</b></em>' containment reference.
   * <!-- begin-user-doc -->
   * <p>
   * If the meaning of the '<em>Class To Clone</em>' containment reference isn't clear,
   * there really should be more of a description here...
   * </p>
   * <!-- end-user-doc -->
   * @return the value of the '<em>Class To Clone</em>' containment reference.
   * @see #setClassToClone(JvmTypeReference)
   * @see my.home.dsl.deepClone.DeepClonePackage#getClassCloner_ClassToClone()
   * @model containment="true"
   * @generated
   */
  JvmTypeReference getClassToClone();

  /**
   * Sets the value of the '{@link my.home.dsl.deepClone.ClassCloner#getClassToClone <em>Class To Clone</em>}' containment reference.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @param value the new value of the '<em>Class To Clone</em>' containment reference.
   * @see #getClassToClone()
   * @generated
   */
  void setClassToClone(JvmTypeReference value);

  /**
   * Returns the value of the '<em><b>Name</b></em>' attribute.
   * <!-- begin-user-doc -->
   * <p>
   * If the meaning of the '<em>Name</em>' attribute isn't clear,
   * there really should be more of a description here...
   * </p>
   * <!-- end-user-doc -->
   * @return the value of the '<em>Name</em>' attribute.
   * @see #setName(String)
   * @see my.home.dsl.deepClone.DeepClonePackage#getClassCloner_Name()
   * @model
   * @generated
   */
  String getName();

  /**
   * Sets the value of the '{@link my.home.dsl.deepClone.ClassCloner#getName <em>Name</em>}' attribute.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @param value the new value of the '<em>Name</em>' attribute.
   * @see #getName()
   * @generated
   */
  void setName(String value);

} // ClassCloner
