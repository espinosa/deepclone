/**
 */
package my.home.dsl.deepClone;


/**
 * <!-- begin-user-doc -->
 * A representation of the model object '<em><b>Simple Excluded Field</b></em>'.
 * <!-- end-user-doc -->
 *
 *
 * @see my.home.dsl.deepClone.DeepClonePackage#getSimpleExcludedField()
 * @model
 * @generated
 */
public interface SimpleExcludedField extends FieldClonerType
{
} // SimpleExcludedField
