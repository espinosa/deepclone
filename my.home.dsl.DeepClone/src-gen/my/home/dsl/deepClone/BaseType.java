/**
 */
package my.home.dsl.deepClone;

import org.eclipse.emf.ecore.EObject;

import org.eclipse.xtext.common.types.JvmTypeReference;

/**
 * <!-- begin-user-doc -->
 * A representation of the model object '<em><b>Base Type</b></em>'.
 * <!-- end-user-doc -->
 *
 * <p>
 * The following features are supported:
 * <ul>
 *   <li>{@link my.home.dsl.deepClone.BaseType#getJavaType <em>Java Type</em>}</li>
 * </ul>
 * </p>
 *
 * @see my.home.dsl.deepClone.DeepClonePackage#getBaseType()
 * @model
 * @generated
 */
public interface BaseType extends EObject
{
  /**
   * Returns the value of the '<em><b>Java Type</b></em>' containment reference.
   * <!-- begin-user-doc -->
   * <p>
   * If the meaning of the '<em>Java Type</em>' containment reference isn't clear,
   * there really should be more of a description here...
   * </p>
   * <!-- end-user-doc -->
   * @return the value of the '<em>Java Type</em>' containment reference.
   * @see #setJavaType(JvmTypeReference)
   * @see my.home.dsl.deepClone.DeepClonePackage#getBaseType_JavaType()
   * @model containment="true"
   * @generated
   */
  JvmTypeReference getJavaType();

  /**
   * Sets the value of the '{@link my.home.dsl.deepClone.BaseType#getJavaType <em>Java Type</em>}' containment reference.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @param value the new value of the '<em>Java Type</em>' containment reference.
   * @see #getJavaType()
   * @generated
   */
  void setJavaType(JvmTypeReference value);

} // BaseType
