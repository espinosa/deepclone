/*
* generated by Xtext
*/
package my.home.dsl.parser.antlr;

import com.google.inject.Inject;

import org.eclipse.xtext.parser.antlr.XtextTokenStream;
import my.home.dsl.services.DeepCloneGrammarAccess;

public class DeepCloneParser extends org.eclipse.xtext.parser.antlr.AbstractAntlrParser {
	
	@Inject
	private DeepCloneGrammarAccess grammarAccess;
	
	@Override
	protected void setInitialHiddenTokens(XtextTokenStream tokenStream) {
		tokenStream.setInitialHiddenTokens("RULE_WS", "RULE_ML_COMMENT", "RULE_SL_COMMENT");
	}
	
	@Override
	protected my.home.dsl.parser.antlr.internal.InternalDeepCloneParser createParser(XtextTokenStream stream) {
		return new my.home.dsl.parser.antlr.internal.InternalDeepCloneParser(stream, getGrammarAccess());
	}
	
	@Override 
	protected String getDefaultRuleName() {
		return "Model";
	}
	
	public DeepCloneGrammarAccess getGrammarAccess() {
		return this.grammarAccess;
	}
	
	public void setGrammarAccess(DeepCloneGrammarAccess grammarAccess) {
		this.grammarAccess = grammarAccess;
	}
	
}
