
package my.home.dsl;

/**
 * Initialization support for running Xtext languages 
 * without equinox extension registry
 */
public class DeepCloneStandaloneSetup extends DeepCloneStandaloneSetupGenerated{

	public static void doSetup() {
		new DeepCloneStandaloneSetup().createInjectorAndDoEMFRegistration();
	}
}

