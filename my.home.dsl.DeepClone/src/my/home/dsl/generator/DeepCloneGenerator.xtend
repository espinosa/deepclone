package my.home.dsl.generator

import com.google.inject.Inject
import java.util.Collection
import java.util.List
import java.util.Set
import my.home.dsl.deepClone.Body
import my.home.dsl.deepClone.ClassCloner
import my.home.dsl.deepClone.ComplexField
import my.home.dsl.deepClone.ContainerType
import my.home.dsl.deepClone.ReferenceField
import my.home.dsl.deepClone.SimpleField
import my.home.dsl.utils.DeepCloneUtils
import my.home.dsl.utils.ReflectionUtils
import org.eclipse.emf.ecore.resource.Resource
import org.eclipse.xtext.common.types.JvmTypeReference
import org.eclipse.xtext.common.types.util.TypeReferences
import org.eclipse.xtext.generator.IFileSystemAccess
import org.eclipse.xtext.generator.IGenerator
import org.eclipse.xtext.naming.IQualifiedNameConverter
import org.eclipse.xtext.naming.QualifiedName

/**
 * Generate Java code for DeepClone DSL for cloning Java bean instances. 
 * Input is complete set of all cloning definitions, potentially spread across several physical DSL files. 
 * Output is set of physical Java files arranged to appropriate directory hierarchy.  
 * 
 * Note: on the input there is Resource, usually it is mapped to one file, one particular 
 * DSL file, but it can be created synthetically, merging contents of several DSL files, preferably all of the same type.    
 * 
 * @see http://code.google.com/p/google-guice/wiki/InjectionPoints
 * @author espinosa, 17.10.2012, 18.10.2012, 19.10, 9.11.2012
 */
class DeepCloneGenerator implements IGenerator {
	
	@Inject extension IQualifiedNameConverter
	
	@Inject extension DeepCloneUtils 
	
	@Inject extension ReflectionUtils
	
	var String packageNamespace

	override void doGenerate(Resource resource, IFileSystemAccess fsa) {
		packageNamespace = resource.contents.filter(typeof(Body)).head.packageConfig?.name ?: "default"
		for (cc : resource.allContents.toIterable.filter(typeof(ClassCloner))) {
			val clonedClassSimpleName = cc.classToClone.simpleName
			val classClonerClassName = cc.name ?: clonedClassSimpleName + "Cloner" 
			val clonerClassFQN = packageNamespace.toQualifiedName.append(classClonerClassName)
			fsa.generateFile(
				clonerClassFQN.toString("/") + ".java",
				cc.generateJavaFileContent(clonerClassFQN))
		}
	}
	
	/**
	 * Generate root level cloner
	 */
	def generateJavaFileContent(ClassCloner cc, QualifiedName clonerClassFQN) {
		'''
		package «clonerClassFQN.skipLast(1)»;
		public class «clonerClassFQN.lastSegment» {
			«generateApplyMethodAndSubCloners(cc)»
		}
		'''
	}
	
	/**
	 * Generate cloner for complex field, a sub-cloner. 
	 * 
	 * Current implementation generates static nested class inside main cloner class. 
	 * Sub cloners are nested recursively. This is an ultimate prevention of any name conflicts. 
	 * This method is recursively called for child complex fields.
	 */
	def generateSubClonerClass(ComplexField complexField) {
		val fieldClonerClass = complexField.fieldName?.toFirstUpper + "Cloner";
		val fieldClonerInstance = complexField.fieldName?.toFirstLower + "Cloner";
		'''
		
		private final «fieldClonerClass» «fieldClonerInstance» = new «fieldClonerClass»();
		public static class «fieldClonerClass» {
			«generateApplyMethodAndSubCloners(complexField)»
		}
		'''
	}
	
	/**
	 * Declare instance of cloner for referenced cloner.
	 * Example:
	 * {@code private final SectionDeep sectionDeepCloner = new SectionDeep();}
	 * for field:
	 * {@code &section SectionDeep}
	 */
	def generateReferenceClonerInstance(ReferenceField referenceField) {
		val fieldClonerClass = packageNamespace.toQualifiedName.append(referenceField.clonerReference.name);
		val fieldClonerInstance = referenceField.fieldName?.toFirstLower + "Cloner";
		'''
		
		private final «fieldClonerClass» «fieldClonerInstance» = new «fieldClonerClass»();
		'''
	}
	
	/**
	 * Generate content for root and sub cloners: apply() method and all sub-cloners (if any) 
	 */
	def CharSequence generateApplyMethodAndSubCloners(ContainerType container) {
		val containerType = container.javaType.typeOrCollectionTypeParameter.qualifiedName
		'''
		public «containerType» apply(«containerType» other) {
			if (other == null) return null;
			«containerType» it = new «containerType»();
			«FOR field : container.fields»
				«IF     field instanceof SimpleField && !field.isCollection»
					it.set«field.fieldName.toFirstUpper»(other.get«field.fieldName.toFirstUpper»());
				«ELSEIF field instanceof SimpleField && field.isCollection»
					it.set«field.fieldName.toFirstUpper»(«field.fieldName.toFirstLower»CollectionCloner(it.get«field.fieldName.toFirstUpper»(), other.get«field.fieldName.toFirstUpper»()));
				«ELSEIF field instanceof ComplexField && !field.isCollection»
					it.set«field.fieldName.toFirstUpper»(«field.fieldName.toFirstLower»Cloner.apply(other.get«field.fieldName.toFirstUpper»()));
				«ELSEIF field instanceof ComplexField && field.isCollection»
					it.set«field.fieldName.toFirstUpper»(«field.fieldName.toFirstLower»CollectionCloner(it.get«field.fieldName.toFirstUpper»(), other.get«field.fieldName.toFirstUpper»()));
				«ELSEIF field instanceof ReferenceField»
					it.set«field.fieldName.toFirstUpper»(«field.fieldName.toFirstLower»Cloner.apply(other.get«field.fieldName.toFirstUpper»()));
				«ENDIF»
			«ENDFOR»
			return it;
		}
		«FOR field : container.fields»
			«IF field instanceof SimpleField && field.isCollection»
				«generateCollectionFieldClonnerMethod(field as SimpleField)»
			«ELSEIF field instanceof ComplexField && field.isCollection»
				«generateCollectionComplexFieldClonnerMethod(field as ComplexField)»
			«ENDIF»
			«IF field instanceof ComplexField»
				«generateSubClonerClass(field as ComplexField)»
			«ENDIF»
			«IF field instanceof ReferenceField»
				«generateReferenceClonerInstance(field as ReferenceField)»
			«ENDIF»
		«ENDFOR»
		'''
	}
	
	/**
	 * Generate private method for a field representing 1:N relation.
	 * Every simple filed which is also a collection has a custom method generated.
	 * Items are transfered one by one. 
	 */	
	def generateCollectionFieldClonnerMethod(SimpleField field) {
		val collectionType     = field.javaType.qualifiedName // example: java.util.List<java.lang.String>
		val collectionItemType = field.javaType.getTypeOrCollectionTypeParameter.qualifiedName // example: java.lang.String
		'''
		
		private «collectionType» «field.fieldName.toFirstLower»CollectionCloner(«collectionType» thisCollection, «collectionType» otherCollection) {
			if (otherCollection == null) return null;
			«createCollection(field.javaType)»
			for («collectionItemType» otherCollectionItem : otherCollection) {
				thisCollection.add(otherCollectionItem);
			}
			return thisCollection;
		}
		'''
	}
	
	/**
	 * Generate private method for a field representing 1:N relation. A complex field with its own sub-cloner.
	 * Every simple filed which is also a collection has a custom method generated.
	 * Items are transfered one by one calling sub-cloner apply() method.
	 */	
	def generateCollectionComplexFieldClonnerMethod(ComplexField field) {
		val collectionType     = field.javaType.qualifiedName // example: "java.util.List<a.b.m.Section>"
		val collectionItemType = field.javaType.getTypeOrCollectionTypeParameter.qualifiedName // example: "a.b.m.Section"
		'''
		
		private «collectionType» «field.fieldName.toFirstLower»CollectionCloner(«collectionType» thisCollection, «collectionType» otherCollection) {
			if (otherCollection == null) return null;
			«createCollection(field.javaType)»
			for («collectionItemType» otherCollectionItem : otherCollection) {
				thisCollection.add(«field.fieldName.toFirstLower»Cloner.apply(otherCollectionItem));
			}
			return thisCollection;
		}
		'''
	}
	
	@Inject extension TypeReferences;
	
	// Naive implementation of a collection based 1:N, one-to-many fields
	// TODO: this has to be replaced with a proper 
	def createCollection(JvmTypeReference collectionType) {
		switch (collectionType) {
			case collectionType.isInstanceOf(List):
			'''
			if (thisCollection == null) thisCollection = new java.util.ArrayList<«collectionType.typeParameter»>();
			'''
			case collectionType.isInstanceOf(Set):
			'''
			if (thisCollection == null) thisCollection = new java.util.HashSet<«collectionType.typeParameter»>();
			'''
			case collectionType.isInstanceOf(Collection):
			'''
			if (thisCollection == null) thisCollection = new java.util.ArrayList<«collectionType.typeParameter»>();
			'''
			default: '''// unrecognized collection type'''
		}
	}
}