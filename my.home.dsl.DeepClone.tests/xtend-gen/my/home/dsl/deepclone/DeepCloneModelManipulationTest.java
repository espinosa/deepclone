package my.home.dsl.deepclone;

import com.google.common.base.Objects;
import com.google.inject.Inject;
import java.util.ArrayList;
import my.home.dsl.DeepCloneInjectorProvider;
import my.home.dsl.deepClone.Body;
import my.home.dsl.deepClone.ClassCloner;
import my.home.dsl.deepClone.ContainerType;
import my.home.dsl.deepClone.FieldClonerType;
import my.home.dsl.deepClone.Model;
import my.home.dsl.utils.DeepCloneUtils;
import org.eclipse.emf.common.util.EList;
import org.eclipse.xtend2.lib.StringConcatenation;
import org.eclipse.xtext.junit4.InjectWith;
import org.eclipse.xtext.junit4.XtextRunner;
import org.eclipse.xtext.junit4.util.ParseHelper;
import org.eclipse.xtext.serializer.ISerializer;
import org.eclipse.xtext.xbase.lib.CollectionLiterals;
import org.eclipse.xtext.xbase.lib.Exceptions;
import org.eclipse.xtext.xbase.lib.Extension;
import org.eclipse.xtext.xbase.lib.Functions.Function1;
import org.eclipse.xtext.xbase.lib.IterableExtensions;
import org.junit.Assert;
import org.junit.Ignore;
import org.junit.Test;
import org.junit.runner.RunWith;

@InjectWith(DeepCloneInjectorProvider.class)
@RunWith(XtextRunner.class)
@SuppressWarnings("all")
public class DeepCloneModelManipulationTest {
  @Inject
  private ParseHelper<Model> parser;
  
  @Inject
  @Extension
  private ISerializer _iSerializer;
  
  @Inject
  @Extension
  private DeepCloneUtils _deepCloneUtils;
  
  /**
   * WORKS
   */
  @Test
  public void removeFieldFromModelAndSerializeBack_removeFieldFromRootClonerElement() {
    try {
      StringConcatenation _builder = new StringConcatenation();
      _builder.append("deepClone");
      _builder.newLine();
      _builder.append("package my.home.cloners");
      _builder.newLine();
      _builder.append("a.b.m.Magazine {");
      _builder.newLine();
      _builder.append("\t");
      _builder.append("-isbn");
      _builder.newLine();
      _builder.append("\t");
      _builder.append("name");
      _builder.newLine();
      _builder.append("\t");
      _builder.append("fooFoo");
      _builder.newLine();
      _builder.append("\t");
      _builder.append("-article");
      _builder.newLine();
      _builder.append("}");
      _builder.newLine();
      final String sample = _builder.toString();
      StringConcatenation _builder_1 = new StringConcatenation();
      _builder_1.append("deepClone");
      _builder_1.newLine();
      _builder_1.append("package my.home.cloners");
      _builder_1.newLine();
      _builder_1.append("a.b.m.Magazine {");
      _builder_1.newLine();
      _builder_1.append("\t");
      _builder_1.append("-isbn");
      _builder_1.newLine();
      _builder_1.append("\t");
      _builder_1.append("name");
      _builder_1.newLine();
      _builder_1.append("\t");
      _builder_1.append("-article");
      _builder_1.newLine();
      _builder_1.append("}");
      final String expectedModifiedSample = _builder_1.toString();
      final Model model = this.parser.parse(sample);
      EList<ClassCloner> _cloners = ((Body) model).getCloners();
      ClassCloner _get = _cloners.get(0);
      final ContainerType cloner = this._deepCloneUtils.asContainer(_get);
      ArrayList<String> _newArrayList = CollectionLiterals.<String>newArrayList("fooFoo");
      this._deepCloneUtils.removeFields(cloner, _newArrayList);
      final String result = this._iSerializer.serialize(model);
      Assert.assertEquals(expectedModifiedSample, result);
    } catch (Throwable _e) {
      throw Exceptions.sneakyThrow(_e);
    }
  }
  
  /**
   * WORKS
   */
  @Test
  public void removeFieldFromModelAndSerializeBack_removeFieldDeeperInTheStructure() {
    try {
      StringConcatenation _builder = new StringConcatenation();
      _builder.append("deepClone");
      _builder.newLine();
      _builder.append("package my.home.cloners");
      _builder.newLine();
      _builder.append("a.b.m.Magazine {");
      _builder.newLine();
      _builder.append("\t");
      _builder.append("-isbn");
      _builder.newLine();
      _builder.append("\t");
      _builder.append("name");
      _builder.newLine();
      _builder.append("\t");
      _builder.append("article {");
      _builder.newLine();
      _builder.append("\t\t");
      _builder.append("name");
      _builder.newLine();
      _builder.append("\t\t");
      _builder.append("-author");
      _builder.newLine();
      _builder.append("\t\t");
      _builder.append("-pageNumber");
      _builder.newLine();
      _builder.append("\t\t");
      _builder.append("fooFoo");
      _builder.newLine();
      _builder.append("\t\t");
      _builder.append("section {");
      _builder.newLine();
      _builder.append("\t\t\t");
      _builder.append("name");
      _builder.newLine();
      _builder.append("\t\t\t");
      _builder.append("description");
      _builder.newLine();
      _builder.append("\t\t\t");
      _builder.append("-paragraph");
      _builder.newLine();
      _builder.append("\t\t");
      _builder.append("}");
      _builder.newLine();
      _builder.append("\t");
      _builder.append("}");
      _builder.newLine();
      _builder.append("}");
      _builder.newLine();
      final String sample = _builder.toString();
      StringConcatenation _builder_1 = new StringConcatenation();
      _builder_1.append("deepClone");
      _builder_1.newLine();
      _builder_1.append("package my.home.cloners");
      _builder_1.newLine();
      _builder_1.append("a.b.m.Magazine {");
      _builder_1.newLine();
      _builder_1.append("\t");
      _builder_1.append("-isbn");
      _builder_1.newLine();
      _builder_1.append("\t");
      _builder_1.append("name");
      _builder_1.newLine();
      _builder_1.append("\t");
      _builder_1.append("article {");
      _builder_1.newLine();
      _builder_1.append("\t\t");
      _builder_1.append("name");
      _builder_1.newLine();
      _builder_1.append("\t\t");
      _builder_1.append("-author");
      _builder_1.newLine();
      _builder_1.append("\t\t");
      _builder_1.append("-pageNumber");
      _builder_1.newLine();
      _builder_1.append("\t\t");
      _builder_1.append("section {");
      _builder_1.newLine();
      _builder_1.append("\t\t\t");
      _builder_1.append("name");
      _builder_1.newLine();
      _builder_1.append("\t\t\t");
      _builder_1.append("description");
      _builder_1.newLine();
      _builder_1.append("\t\t\t");
      _builder_1.append("-paragraph");
      _builder_1.newLine();
      _builder_1.append("\t\t");
      _builder_1.append("}");
      _builder_1.newLine();
      _builder_1.append("\t");
      _builder_1.append("}");
      _builder_1.newLine();
      _builder_1.append("}");
      final String expectedModifiedSample = _builder_1.toString();
      final Model model = this.parser.parse(sample);
      EList<ClassCloner> _cloners = ((Body) model).getCloners();
      ClassCloner _get = _cloners.get(0);
      EList<FieldClonerType> _fields = _get.getFields();
      final Function1<FieldClonerType,Boolean> _function = new Function1<FieldClonerType,Boolean>() {
        public Boolean apply(final FieldClonerType it) {
          String _fieldName = it.getFieldName();
          boolean _equals = Objects.equal(_fieldName, "article");
          return Boolean.valueOf(_equals);
        }
      };
      FieldClonerType _findFirst = IterableExtensions.<FieldClonerType>findFirst(_fields, _function);
      final ContainerType articleContainerField = this._deepCloneUtils.asContainer(_findFirst);
      ArrayList<String> _newArrayList = CollectionLiterals.<String>newArrayList("fooFoo");
      this._deepCloneUtils.removeFields(articleContainerField, _newArrayList);
      final String result = this._iSerializer.serialize(model);
      Assert.assertEquals(expectedModifiedSample, result);
    } catch (Throwable _e) {
      throw Exceptions.sneakyThrow(_e);
    }
  }
  
  /**
   * WORKS
   */
  @Test
  public void addFieldToModelAndSerializeBack_addFieldToRootClonerElement() {
    try {
      StringConcatenation _builder = new StringConcatenation();
      _builder.append("deepClone");
      _builder.newLine();
      _builder.append("package my.home.cloners");
      _builder.newLine();
      _builder.append("a.b.m.Magazine {");
      _builder.newLine();
      _builder.append("\t");
      _builder.append("-isbn");
      _builder.newLine();
      _builder.append("\t");
      _builder.append("name");
      _builder.newLine();
      _builder.append("\t");
      _builder.append("-article");
      _builder.newLine();
      _builder.append("}");
      _builder.newLine();
      final String sample = _builder.toString();
      StringConcatenation _builder_1 = new StringConcatenation();
      _builder_1.append("deepClone");
      _builder_1.newLine();
      _builder_1.append("package my.home.cloners");
      _builder_1.newLine();
      _builder_1.append("a.b.m.Magazine {");
      _builder_1.newLine();
      _builder_1.append("\t");
      _builder_1.append("-isbn");
      _builder_1.newLine();
      _builder_1.append("\t");
      _builder_1.append("name");
      _builder_1.newLine();
      _builder_1.append("\t");
      _builder_1.append("-article");
      _builder_1.newLine();
      _builder_1.append("\t");
      _builder_1.append("fooFoo");
      _builder_1.newLine();
      _builder_1.append("}");
      final String expectedModifiedSample = _builder_1.toString();
      final Model model = this.parser.parse(sample);
      EList<ClassCloner> _cloners = ((Body) model).getCloners();
      ClassCloner _get = _cloners.get(0);
      final ContainerType cloner = this._deepCloneUtils.asContainer(_get);
      ArrayList<String> _newArrayList = CollectionLiterals.<String>newArrayList("fooFoo");
      this._deepCloneUtils.addFields(cloner, _newArrayList);
      final String result = this._iSerializer.serialize(model);
      Assert.assertEquals(expectedModifiedSample, result);
    } catch (Throwable _e) {
      throw Exceptions.sneakyThrow(_e);
    }
  }
  
  /**
   * WORKS
   */
  @Test
  public void addFieldToModelAndSerializeBack_addFieldDeeperInTheStructure() {
    try {
      StringConcatenation _builder = new StringConcatenation();
      _builder.append("deepClone");
      _builder.newLine();
      _builder.append("package my.home.cloners");
      _builder.newLine();
      _builder.append("a.b.m.Magazine {");
      _builder.newLine();
      _builder.append("\t");
      _builder.append("-isbn");
      _builder.newLine();
      _builder.append("\t");
      _builder.append("name");
      _builder.newLine();
      _builder.append("\t");
      _builder.append("article {");
      _builder.newLine();
      _builder.append("\t\t");
      _builder.append("name");
      _builder.newLine();
      _builder.append("\t\t");
      _builder.append("-author");
      _builder.newLine();
      _builder.append("\t\t");
      _builder.append("-pageNumber");
      _builder.newLine();
      _builder.append("\t\t");
      _builder.append("section {");
      _builder.newLine();
      _builder.append("\t\t\t");
      _builder.append("name");
      _builder.newLine();
      _builder.append("\t\t\t");
      _builder.append("description");
      _builder.newLine();
      _builder.append("\t\t\t");
      _builder.append("-paragraph");
      _builder.newLine();
      _builder.append("\t\t");
      _builder.append("}");
      _builder.newLine();
      _builder.append("\t");
      _builder.append("}");
      _builder.newLine();
      _builder.append("}");
      _builder.newLine();
      final String sample = _builder.toString();
      StringConcatenation _builder_1 = new StringConcatenation();
      _builder_1.append("deepClone");
      _builder_1.newLine();
      _builder_1.append("package my.home.cloners");
      _builder_1.newLine();
      _builder_1.append("a.b.m.Magazine {");
      _builder_1.newLine();
      _builder_1.append("\t");
      _builder_1.append("-isbn");
      _builder_1.newLine();
      _builder_1.append("\t");
      _builder_1.append("name");
      _builder_1.newLine();
      _builder_1.append("\t");
      _builder_1.append("article {");
      _builder_1.newLine();
      _builder_1.append("\t\t");
      _builder_1.append("name");
      _builder_1.newLine();
      _builder_1.append("\t\t");
      _builder_1.append("-author");
      _builder_1.newLine();
      _builder_1.append("\t\t");
      _builder_1.append("-pageNumber");
      _builder_1.newLine();
      _builder_1.append("\t\t");
      _builder_1.append("section {");
      _builder_1.newLine();
      _builder_1.append("\t\t\t");
      _builder_1.append("name");
      _builder_1.newLine();
      _builder_1.append("\t\t\t");
      _builder_1.append("description");
      _builder_1.newLine();
      _builder_1.append("\t\t\t");
      _builder_1.append("-paragraph");
      _builder_1.newLine();
      _builder_1.append("\t\t");
      _builder_1.append("}");
      _builder_1.newLine();
      _builder_1.append("\t\t");
      _builder_1.append("fooFoo");
      _builder_1.newLine();
      _builder_1.append("\t");
      _builder_1.append("}");
      _builder_1.newLine();
      _builder_1.append("}");
      final String expectedModifiedSample = _builder_1.toString();
      final Model model = this.parser.parse(sample);
      EList<ClassCloner> _cloners = ((Body) model).getCloners();
      ClassCloner _get = _cloners.get(0);
      EList<FieldClonerType> _fields = _get.getFields();
      final Function1<FieldClonerType,Boolean> _function = new Function1<FieldClonerType,Boolean>() {
        public Boolean apply(final FieldClonerType it) {
          String _fieldName = it.getFieldName();
          boolean _equals = Objects.equal(_fieldName, "article");
          return Boolean.valueOf(_equals);
        }
      };
      FieldClonerType _findFirst = IterableExtensions.<FieldClonerType>findFirst(_fields, _function);
      final ContainerType articleContainerField = this._deepCloneUtils.asContainer(_findFirst);
      ArrayList<String> _newArrayList = CollectionLiterals.<String>newArrayList("fooFoo");
      this._deepCloneUtils.addFields(articleContainerField, _newArrayList);
      final String result = this._iSerializer.serialize(model);
      Assert.assertEquals(expectedModifiedSample, result);
    } catch (Throwable _e) {
      throw Exceptions.sneakyThrow(_e);
    }
  }
  
  /**
   * FAILS
   */
  @Test
  @Ignore
  public void addFieldToModelAndSerializeBack_testComments() {
    try {
      StringConcatenation _builder = new StringConcatenation();
      _builder.append("deepClone");
      _builder.newLine();
      _builder.append("package my.home.cloners");
      _builder.newLine();
      _builder.append("a.b.m.Magazine {");
      _builder.newLine();
      _builder.append("\t");
      _builder.append("-isbn");
      _builder.newLine();
      _builder.append("\t");
      _builder.append("name");
      _builder.newLine();
      _builder.append("\t");
      _builder.append("article {");
      _builder.newLine();
      _builder.append("\t\t");
      _builder.append("// comment 1");
      _builder.newLine();
      _builder.append("\t\t");
      _builder.append("name");
      _builder.newLine();
      _builder.append("\t\t");
      _builder.append("// comment 2");
      _builder.newLine();
      _builder.append("\t\t");
      _builder.append("-author");
      _builder.newLine();
      _builder.append("\t\t");
      _builder.append("-pageNumber");
      _builder.newLine();
      _builder.append("\t\t");
      _builder.append("// comment 3");
      _builder.newLine();
      _builder.append("\t\t");
      _builder.append("section {");
      _builder.newLine();
      _builder.append("\t\t\t");
      _builder.append("name");
      _builder.newLine();
      _builder.append("\t\t\t");
      _builder.append("description");
      _builder.newLine();
      _builder.append("\t\t\t");
      _builder.append("-paragraph");
      _builder.newLine();
      _builder.append("\t\t\t");
      _builder.append("// comment 4");
      _builder.newLine();
      _builder.append("\t\t");
      _builder.append("}");
      _builder.newLine();
      _builder.append("\t\t");
      _builder.append("// comment 5");
      _builder.newLine();
      _builder.append("\t");
      _builder.append("}");
      _builder.newLine();
      _builder.append("}");
      _builder.newLine();
      final String sample = _builder.toString();
      StringConcatenation _builder_1 = new StringConcatenation();
      _builder_1.append("deepClone");
      _builder_1.newLine();
      _builder_1.append("package my.home.cloners");
      _builder_1.newLine();
      _builder_1.append("a.b.m.Magazine {");
      _builder_1.newLine();
      _builder_1.append("\t");
      _builder_1.append("-isbn");
      _builder_1.newLine();
      _builder_1.append("\t");
      _builder_1.append("name");
      _builder_1.newLine();
      _builder_1.append("\t");
      _builder_1.append("article {");
      _builder_1.newLine();
      _builder_1.append("\t\t");
      _builder_1.append("// comment 1");
      _builder_1.newLine();
      _builder_1.append("\t\t");
      _builder_1.append("name");
      _builder_1.newLine();
      _builder_1.append("\t\t");
      _builder_1.append("// comment 2");
      _builder_1.newLine();
      _builder_1.append("\t\t");
      _builder_1.append("-author");
      _builder_1.newLine();
      _builder_1.append("\t\t");
      _builder_1.append("-pageNumber");
      _builder_1.newLine();
      _builder_1.append("\t\t");
      _builder_1.append("// comment 3");
      _builder_1.newLine();
      _builder_1.append("\t\t");
      _builder_1.append("section {");
      _builder_1.newLine();
      _builder_1.append("\t\t\t");
      _builder_1.append("name");
      _builder_1.newLine();
      _builder_1.append("\t\t\t");
      _builder_1.append("description");
      _builder_1.newLine();
      _builder_1.append("\t\t\t");
      _builder_1.append("-paragraph");
      _builder_1.newLine();
      _builder_1.append("\t\t\t");
      _builder_1.append("// comment 4");
      _builder_1.newLine();
      _builder_1.append("\t\t");
      _builder_1.append("}");
      _builder_1.newLine();
      _builder_1.append("\t\t");
      _builder_1.append("// comment 5");
      _builder_1.newLine();
      _builder_1.append("\t\t");
      _builder_1.append("fooFoo");
      _builder_1.newLine();
      _builder_1.append("\t");
      _builder_1.append("}");
      _builder_1.newLine();
      _builder_1.append("}");
      final String expectedModifiedSample = _builder_1.toString();
      final Model model = this.parser.parse(sample);
      EList<ClassCloner> _cloners = ((Body) model).getCloners();
      ClassCloner _get = _cloners.get(0);
      EList<FieldClonerType> _fields = _get.getFields();
      final Function1<FieldClonerType,Boolean> _function = new Function1<FieldClonerType,Boolean>() {
        public Boolean apply(final FieldClonerType it) {
          String _fieldName = it.getFieldName();
          boolean _equals = Objects.equal(_fieldName, "article");
          return Boolean.valueOf(_equals);
        }
      };
      FieldClonerType _findFirst = IterableExtensions.<FieldClonerType>findFirst(_fields, _function);
      final ContainerType articleContainerField = this._deepCloneUtils.asContainer(_findFirst);
      ArrayList<String> _newArrayList = CollectionLiterals.<String>newArrayList("fooFoo");
      this._deepCloneUtils.addFields(articleContainerField, _newArrayList);
      final String result = this._iSerializer.serialize(model);
      Assert.assertEquals(expectedModifiedSample, result);
    } catch (Throwable _e) {
      throw Exceptions.sneakyThrow(_e);
    }
  }
  
  /**
   * FAILS
   */
  @Test
  @Ignore
  public void addFieldToModelAndSerializeBack_testComments_simple() {
    try {
      StringConcatenation _builder = new StringConcatenation();
      _builder.append("deepClone");
      _builder.newLine();
      _builder.append("package my.home.cloners");
      _builder.newLine();
      _builder.append("a.b.m.Magazine {");
      _builder.newLine();
      _builder.append("\t");
      _builder.append("isbn");
      _builder.newLine();
      _builder.append("\t");
      _builder.append("name");
      _builder.newLine();
      _builder.append("\t");
      _builder.append("article");
      _builder.newLine();
      _builder.append("\t");
      _builder.append("// comment 1");
      _builder.newLine();
      _builder.append("}");
      _builder.newLine();
      final String sample = _builder.toString();
      StringConcatenation _builder_1 = new StringConcatenation();
      _builder_1.append("deepClone");
      _builder_1.newLine();
      _builder_1.append("package my.home.cloners");
      _builder_1.newLine();
      _builder_1.append("a.b.m.Magazine {");
      _builder_1.newLine();
      _builder_1.append("\t");
      _builder_1.append("isbn");
      _builder_1.newLine();
      _builder_1.append("\t");
      _builder_1.append("name");
      _builder_1.newLine();
      _builder_1.append("\t");
      _builder_1.append("article");
      _builder_1.newLine();
      _builder_1.append("\t");
      _builder_1.append("// comment 1");
      _builder_1.newLine();
      _builder_1.append("\t");
      _builder_1.append("fooFoo");
      _builder_1.newLine();
      _builder_1.append("}");
      final String expectedModifiedSample = _builder_1.toString();
      final Model model = this.parser.parse(sample);
      EList<ClassCloner> _cloners = ((Body) model).getCloners();
      ClassCloner _get = _cloners.get(0);
      final ContainerType articleContainerField = this._deepCloneUtils.asContainer(_get);
      ArrayList<String> _newArrayList = CollectionLiterals.<String>newArrayList("fooFoo");
      this._deepCloneUtils.addFields(articleContainerField, _newArrayList);
      final String result = this._iSerializer.serialize(model);
      Assert.assertEquals(expectedModifiedSample, result);
    } catch (Throwable _e) {
      throw Exceptions.sneakyThrow(_e);
    }
  }
  
  /**
   * FAILS
   */
  @Test
  @Ignore
  public void addFieldToModelAndSerializeBack_testCommentsHandling_emptyContainerWithJustComment() {
    try {
      StringConcatenation _builder = new StringConcatenation();
      _builder.append("deepClone");
      _builder.newLine();
      _builder.append("package my.home.cloners // comment 0");
      _builder.newLine();
      _builder.append("a.b.m.Magazine {");
      _builder.newLine();
      _builder.append("\t");
      _builder.append("// comment 1");
      _builder.newLine();
      _builder.append("} // comment 2");
      _builder.newLine();
      _builder.append("// comment 3");
      _builder.newLine();
      final String sample = _builder.toString();
      StringConcatenation _builder_1 = new StringConcatenation();
      _builder_1.append("deepClone");
      _builder_1.newLine();
      _builder_1.append("package my.home.cloners // comment 0");
      _builder_1.newLine();
      _builder_1.append("a.b.m.Magazine {");
      _builder_1.newLine();
      _builder_1.append("\t");
      _builder_1.append("// comment 1");
      _builder_1.newLine();
      _builder_1.append("\t");
      _builder_1.append("fooFoo");
      _builder_1.newLine();
      _builder_1.append("} // comment 2");
      _builder_1.newLine();
      _builder_1.append("// comment 3");
      _builder_1.newLine();
      final String expectedModifiedSample = _builder_1.toString();
      final Model model = this.parser.parse(sample);
      EList<ClassCloner> _cloners = ((Body) model).getCloners();
      ClassCloner _get = _cloners.get(0);
      final ContainerType articleContainerField = this._deepCloneUtils.asContainer(_get);
      ArrayList<String> _newArrayList = CollectionLiterals.<String>newArrayList("fooFoo");
      this._deepCloneUtils.addFields(articleContainerField, _newArrayList);
      final String result = this._iSerializer.serialize(model);
      Assert.assertEquals(expectedModifiedSample, result);
    } catch (Throwable _e) {
      throw Exceptions.sneakyThrow(_e);
    }
  }
  
  /**
   * FAILS
   */
  @Test
  @Ignore
  public void addFieldToModelAndSerializeBack_testSlashStarCommentsHandling() {
    try {
      StringConcatenation _builder = new StringConcatenation();
      _builder.append("deepClone");
      _builder.newLine();
      _builder.append("package my.home.cloners /* comment 0 */");
      _builder.newLine();
      _builder.append("a.b.m.Magazine /* comment 000 */ {");
      _builder.newLine();
      _builder.append("\t");
      _builder.append("/* comment 1 */");
      _builder.newLine();
      _builder.append("} /* comment 2 */");
      _builder.newLine();
      _builder.append("/* comment 3 */");
      _builder.newLine();
      final String sample = _builder.toString();
      StringConcatenation _builder_1 = new StringConcatenation();
      _builder_1.append("deepClone");
      _builder_1.newLine();
      _builder_1.append("package my.home.cloners /* comment 0 */");
      _builder_1.newLine();
      _builder_1.append("a.b.m.Magazine /* comment 000 */ {");
      _builder_1.newLine();
      _builder_1.append("\t");
      _builder_1.append("/* comment 1 */");
      _builder_1.newLine();
      _builder_1.append("\t");
      _builder_1.append("fooFoo");
      _builder_1.newLine();
      _builder_1.append("} /* comment 2 */");
      _builder_1.newLine();
      _builder_1.append("/* comment */");
      _builder_1.newLine();
      final String expectedModifiedSample = _builder_1.toString();
      final Model model = this.parser.parse(sample);
      EList<ClassCloner> _cloners = ((Body) model).getCloners();
      ClassCloner _get = _cloners.get(0);
      final ContainerType articleContainerField = this._deepCloneUtils.asContainer(_get);
      ArrayList<String> _newArrayList = CollectionLiterals.<String>newArrayList("fooFoo");
      this._deepCloneUtils.addFields(articleContainerField, _newArrayList);
      final String result = this._iSerializer.serialize(model);
      Assert.assertEquals(expectedModifiedSample, result);
    } catch (Throwable _e) {
      throw Exceptions.sneakyThrow(_e);
    }
  }
  
  /**
   * FAILS
   */
  @Test
  @Ignore
  public void addFieldToModelAndSerializeBack_testCommentsHandling_simplestCase() {
    try {
      StringConcatenation _builder = new StringConcatenation();
      _builder.append("deepClone");
      _builder.newLine();
      _builder.append("package my.home.cloners");
      _builder.newLine();
      _builder.append("a.b.m.Magazine {");
      _builder.newLine();
      _builder.append("\t");
      _builder.append("// comment 1");
      _builder.newLine();
      _builder.append("}");
      _builder.newLine();
      final String sample = _builder.toString();
      StringConcatenation _builder_1 = new StringConcatenation();
      _builder_1.append("deepClone");
      _builder_1.newLine();
      _builder_1.append("package my.home.cloners");
      _builder_1.newLine();
      _builder_1.append("a.b.m.Magazine {");
      _builder_1.newLine();
      _builder_1.append("\t");
      _builder_1.append("// comment 1");
      _builder_1.newLine();
      _builder_1.append("\t");
      _builder_1.append("fooFoo");
      _builder_1.newLine();
      _builder_1.append("}");
      _builder_1.newLine();
      final String expectedModifiedSample = _builder_1.toString();
      final Model model = this.parser.parse(sample);
      EList<ClassCloner> _cloners = ((Body) model).getCloners();
      ClassCloner _get = _cloners.get(0);
      final ContainerType articleContainerField = this._deepCloneUtils.asContainer(_get);
      ArrayList<String> _newArrayList = CollectionLiterals.<String>newArrayList("fooFoo");
      this._deepCloneUtils.addFields(articleContainerField, _newArrayList);
      final String result = this._iSerializer.serialize(model);
      Assert.assertEquals(expectedModifiedSample, result);
    } catch (Throwable _e) {
      throw Exceptions.sneakyThrow(_e);
    }
  }
  
  /**
   * WORKS
   */
  @Test
  public void doNotChangeAnythingAndSerializeBack_testCommentsHandling_simplestCase() {
    try {
      StringConcatenation _builder = new StringConcatenation();
      _builder.append("deepClone");
      _builder.newLine();
      _builder.append("package my.home.cloners");
      _builder.newLine();
      _builder.append("a.b.m.Magazine {");
      _builder.newLine();
      _builder.append("\t");
      _builder.append("// comment 1");
      _builder.newLine();
      _builder.append("}");
      _builder.newLine();
      final String sample = _builder.toString();
      StringConcatenation _builder_1 = new StringConcatenation();
      _builder_1.append("deepClone");
      _builder_1.newLine();
      _builder_1.append("package my.home.cloners");
      _builder_1.newLine();
      _builder_1.append("a.b.m.Magazine {");
      _builder_1.newLine();
      _builder_1.append("\t");
      _builder_1.append("// comment 1");
      _builder_1.newLine();
      _builder_1.append("}");
      final String expectedModifiedSample = _builder_1.toString();
      final Model model = this.parser.parse(sample);
      final String result = this._iSerializer.serialize(model);
      Assert.assertEquals(expectedModifiedSample, result);
    } catch (Throwable _e) {
      throw Exceptions.sneakyThrow(_e);
    }
  }
  
  /**
   * FAILS
   */
  @Test
  @Ignore
  public void removeFieldFromModelAndSerializeBack_testCommentsHandling_simplestCase() {
    try {
      StringConcatenation _builder = new StringConcatenation();
      _builder.append("deepClone");
      _builder.newLine();
      _builder.append("package my.home.cloners");
      _builder.newLine();
      _builder.append("a.b.m.Magazine {");
      _builder.newLine();
      _builder.append("\t");
      _builder.append("// comment 1");
      _builder.newLine();
      _builder.append("\t");
      _builder.append("name");
      _builder.newLine();
      _builder.append("\t");
      _builder.append("// comment 2");
      _builder.newLine();
      _builder.append("\t");
      _builder.append("fooFoo");
      _builder.newLine();
      _builder.append("\t");
      _builder.append("// comment 3");
      _builder.newLine();
      _builder.append("\t");
      _builder.append("article");
      _builder.newLine();
      _builder.append("\t");
      _builder.append("// comment 4");
      _builder.newLine();
      _builder.append("}");
      _builder.newLine();
      final String sample = _builder.toString();
      StringConcatenation _builder_1 = new StringConcatenation();
      _builder_1.append("deepClone");
      _builder_1.newLine();
      _builder_1.append("package my.home.cloners");
      _builder_1.newLine();
      _builder_1.append("a.b.m.Magazine {");
      _builder_1.newLine();
      _builder_1.append("\t");
      _builder_1.append("// comment 1");
      _builder_1.newLine();
      _builder_1.append("\t");
      _builder_1.append("name");
      _builder_1.newLine();
      _builder_1.append("\t");
      _builder_1.append("// comment 2");
      _builder_1.newLine();
      _builder_1.append("\t");
      _builder_1.append("// comment 3");
      _builder_1.newLine();
      _builder_1.append("\t");
      _builder_1.append("article");
      _builder_1.newLine();
      _builder_1.append("\t");
      _builder_1.append("// comment 4");
      _builder_1.newLine();
      _builder_1.append("}");
      final String expectedModifiedSample = _builder_1.toString();
      final Model model = this.parser.parse(sample);
      EList<ClassCloner> _cloners = ((Body) model).getCloners();
      ClassCloner _get = _cloners.get(0);
      final ContainerType cloner = this._deepCloneUtils.asContainer(_get);
      ArrayList<String> _newArrayList = CollectionLiterals.<String>newArrayList("fooFoo");
      this._deepCloneUtils.removeFields(cloner, _newArrayList);
      final String result = this._iSerializer.serialize(model);
      Assert.assertEquals(expectedModifiedSample, result);
    } catch (Throwable _e) {
      throw Exceptions.sneakyThrow(_e);
    }
  }
}
