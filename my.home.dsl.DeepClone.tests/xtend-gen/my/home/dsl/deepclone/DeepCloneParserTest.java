package my.home.dsl.deepclone;

import com.google.inject.Inject;
import my.home.dsl.DeepCloneInjectorProvider;
import my.home.dsl.deepClone.Body;
import my.home.dsl.deepClone.ClassCloner;
import my.home.dsl.deepClone.ComplexField;
import my.home.dsl.deepClone.FieldClonerType;
import my.home.dsl.deepClone.Model;
import my.home.dsl.deepClone.ReferenceField;
import my.home.dsl.deepClone.SimpleExcludedField;
import my.home.dsl.deepClone.SimpleField;
import org.eclipse.emf.common.util.EList;
import org.eclipse.xtend2.lib.StringConcatenation;
import org.eclipse.xtext.common.types.JvmTypeReference;
import org.eclipse.xtext.junit4.InjectWith;
import org.eclipse.xtext.junit4.XtextRunner;
import org.eclipse.xtext.junit4.util.ParseHelper;
import org.eclipse.xtext.junit4.validation.ValidationTestHelper;
import org.eclipse.xtext.xbase.lib.Exceptions;
import org.eclipse.xtext.xbase.lib.IterableExtensions;
import org.junit.Assert;
import org.junit.Test;
import org.junit.runner.RunWith;

@InjectWith(DeepCloneInjectorProvider.class)
@RunWith(XtextRunner.class)
@SuppressWarnings("all")
public class DeepCloneParserTest {
  @Inject
  private ParseHelper<Model> parser;
  
  @Inject
  private ValidationTestHelper validationHelper;
  
  /**
   * parse DSL referring existing class OpeningHours2
   */
  @Test
  public void parseOpeningHours1() {
    try {
      StringConcatenation _builder = new StringConcatenation();
      _builder.append("deepClone");
      _builder.newLine();
      _builder.append("package a.b.c");
      _builder.newLine();
      _builder.append("a.b.c.WeekdayOpeningHours OHCloner {");
      _builder.newLine();
      _builder.append("\t");
      _builder.append("weekDay");
      _builder.newLine();
      _builder.append("\t");
      _builder.append("hours");
      _builder.newLine();
      _builder.append("}");
      _builder.newLine();
      final String code = _builder.toString();
      final Model model = this.parser.parse(code);
      this.validationHelper.assertNoErrors(model);
      final Body body = ((Body) model);
      EList<ClassCloner> _cloners = body.getCloners();
      int _size = _cloners.size();
      Assert.assertEquals(1, _size);
      EList<ClassCloner> _cloners_1 = body.getCloners();
      ClassCloner _head = IterableExtensions.<ClassCloner>head(_cloners_1);
      final JvmTypeReference classToClone = _head.getClassToClone();
      String _simpleName = classToClone.getSimpleName();
      Assert.assertEquals("WeekdayOpeningHours", _simpleName);
      String _qualifiedName = classToClone.getQualifiedName();
      Assert.assertEquals("a.b.c.WeekdayOpeningHours", _qualifiedName);
      String _identifier = classToClone.getIdentifier();
      Assert.assertEquals("a.b.c.WeekdayOpeningHours", _identifier);
    } catch (Throwable _e) {
      throw Exceptions.sneakyThrow(_e);
    }
  }
  
  /**
   * parse DSL referring nonexisting class OpeningHours2
   */
  @Test(expected = AssertionError.class)
  public void parseOpeningHours2() {
    try {
      StringConcatenation _builder = new StringConcatenation();
      _builder.append("deepClone");
      _builder.newLine();
      _builder.append("package a.b.c");
      _builder.newLine();
      _builder.append("my.home.dsl.deepclone.FooOpeningHours {");
      _builder.newLine();
      _builder.append("\t");
      _builder.append("weekDay ");
      _builder.newLine();
      _builder.append("}");
      final String code = _builder.toString();
      final Model model = this.parser.parse(code);
      this.validationHelper.assertNoErrors(model);
    } catch (Throwable _e) {
      throw Exceptions.sneakyThrow(_e);
    }
  }
  
  @Test
  public void testNestedElement() {
    try {
      StringConcatenation _builder = new StringConcatenation();
      _builder.append("deepClone");
      _builder.newLine();
      _builder.append("package a.b.c");
      _builder.newLine();
      _builder.append("a.b.c.WeekdayOpeningHours {");
      _builder.newLine();
      _builder.append("\t");
      _builder.append("weekDay");
      _builder.newLine();
      _builder.append("\t");
      _builder.append("hours {");
      _builder.newLine();
      _builder.append("\t\t");
      _builder.append("from");
      _builder.newLine();
      _builder.append("\t\t");
      _builder.append("to");
      _builder.newLine();
      _builder.append("\t");
      _builder.append("}");
      _builder.newLine();
      _builder.append("}");
      final String code = _builder.toString();
      final Model model = this.parser.parse(code);
      this.validationHelper.assertNoErrors(model);
      final Body body = ((Body) model);
      EList<ClassCloner> _cloners = body.getCloners();
      int _size = _cloners.size();
      Assert.assertEquals(1, _size);
      EList<ClassCloner> _cloners_1 = body.getCloners();
      ClassCloner _head = IterableExtensions.<ClassCloner>head(_cloners_1);
      JvmTypeReference _classToClone = _head.getClassToClone();
      String _qualifiedName = _classToClone.getQualifiedName();
      Assert.assertEquals("a.b.c.WeekdayOpeningHours", _qualifiedName);
      EList<ClassCloner> _cloners_2 = body.getCloners();
      ClassCloner _head_1 = IterableExtensions.<ClassCloner>head(_cloners_2);
      EList<FieldClonerType> _fields = _head_1.getFields();
      int _size_1 = _fields.size();
      Assert.assertEquals(2, _size_1);
      EList<ClassCloner> _cloners_3 = body.getCloners();
      ClassCloner _head_2 = IterableExtensions.<ClassCloner>head(_cloners_3);
      EList<FieldClonerType> _fields_1 = _head_2.getFields();
      FieldClonerType _get = _fields_1.get(0);
      String _fieldName = ((SimpleField) _get).getFieldName();
      Assert.assertEquals("weekDay", _fieldName);
      EList<ClassCloner> _cloners_4 = body.getCloners();
      ClassCloner _head_3 = IterableExtensions.<ClassCloner>head(_cloners_4);
      EList<FieldClonerType> _fields_2 = _head_3.getFields();
      FieldClonerType _get_1 = _fields_2.get(1);
      String _fieldName_1 = ((ComplexField) _get_1).getFieldName();
      Assert.assertEquals("hours", _fieldName_1);
      EList<ClassCloner> _cloners_5 = body.getCloners();
      ClassCloner _head_4 = IterableExtensions.<ClassCloner>head(_cloners_5);
      EList<FieldClonerType> _fields_3 = _head_4.getFields();
      FieldClonerType _get_2 = _fields_3.get(1);
      Assert.assertTrue((_get_2 instanceof ComplexField));
      EList<ClassCloner> _cloners_6 = body.getCloners();
      ClassCloner _head_5 = IterableExtensions.<ClassCloner>head(_cloners_6);
      EList<FieldClonerType> _fields_4 = _head_5.getFields();
      FieldClonerType _get_3 = _fields_4.get(1);
      EList<FieldClonerType> _fields_5 = ((ComplexField) _get_3).getFields();
      FieldClonerType _get_4 = _fields_5.get(0);
      String _fieldName_2 = ((SimpleField) _get_4).getFieldName();
      Assert.assertEquals("from", _fieldName_2);
      EList<ClassCloner> _cloners_7 = body.getCloners();
      ClassCloner _head_6 = IterableExtensions.<ClassCloner>head(_cloners_7);
      EList<FieldClonerType> _fields_6 = _head_6.getFields();
      FieldClonerType _get_5 = _fields_6.get(1);
      EList<FieldClonerType> _fields_7 = ((ComplexField) _get_5).getFields();
      FieldClonerType _get_6 = _fields_7.get(1);
      String _fieldName_3 = ((SimpleField) _get_6).getFieldName();
      Assert.assertEquals("to", _fieldName_3);
    } catch (Throwable _e) {
      throw Exceptions.sneakyThrow(_e);
    }
  }
  
  @Test
  public void testNestedNestedElement() {
    try {
      StringConcatenation _builder = new StringConcatenation();
      _builder.append("deepClone");
      _builder.newLine();
      _builder.newLine();
      _builder.append("package my.home.cloners.reduced");
      _builder.newLine();
      _builder.newLine();
      _builder.append("a.b.m.Magazine MagazineCloner123 {");
      _builder.newLine();
      _builder.append("\t");
      _builder.append("name");
      _builder.newLine();
      _builder.append("\t");
      _builder.append("-isbn");
      _builder.newLine();
      _builder.append("\t");
      _builder.append("article {");
      _builder.newLine();
      _builder.append("\t\t");
      _builder.append("name");
      _builder.newLine();
      _builder.append("\t\t");
      _builder.append("-author");
      _builder.newLine();
      _builder.append("\t\t");
      _builder.append("-pageNumber");
      _builder.newLine();
      _builder.append("\t\t");
      _builder.append("section {");
      _builder.newLine();
      _builder.append("\t\t\t");
      _builder.append("-name");
      _builder.newLine();
      _builder.append("\t\t\t");
      _builder.append("description");
      _builder.newLine();
      _builder.append("\t\t\t");
      _builder.append("-paragraph");
      _builder.newLine();
      _builder.append("\t\t");
      _builder.append("}");
      _builder.newLine();
      _builder.append("\t");
      _builder.append("}");
      _builder.newLine();
      _builder.append("}");
      _builder.newLine();
      final String code = _builder.toString();
      final Model model = this.parser.parse(code);
      this.validationHelper.assertNoErrors(model);
      final Body body = ((Body) model);
      EList<ClassCloner> _cloners = body.getCloners();
      int _size = _cloners.size();
      Assert.assertEquals(1, _size);
      EList<ClassCloner> _cloners_1 = body.getCloners();
      ClassCloner _head = IterableExtensions.<ClassCloner>head(_cloners_1);
      JvmTypeReference _classToClone = _head.getClassToClone();
      String _qualifiedName = _classToClone.getQualifiedName();
      Assert.assertEquals("a.b.m.Magazine", _qualifiedName);
      EList<ClassCloner> _cloners_2 = body.getCloners();
      ClassCloner _head_1 = IterableExtensions.<ClassCloner>head(_cloners_2);
      EList<FieldClonerType> _fields = _head_1.getFields();
      int _size_1 = _fields.size();
      Assert.assertEquals(3, _size_1);
      EList<ClassCloner> _cloners_3 = body.getCloners();
      ClassCloner _head_2 = IterableExtensions.<ClassCloner>head(_cloners_3);
      EList<FieldClonerType> _fields_1 = _head_2.getFields();
      FieldClonerType _get = _fields_1.get(0);
      String _fieldName = ((SimpleField) _get).getFieldName();
      Assert.assertEquals("name", _fieldName);
      EList<ClassCloner> _cloners_4 = body.getCloners();
      ClassCloner _head_3 = IterableExtensions.<ClassCloner>head(_cloners_4);
      EList<FieldClonerType> _fields_2 = _head_3.getFields();
      FieldClonerType _get_1 = _fields_2.get(1);
      String _fieldName_1 = ((SimpleExcludedField) _get_1).getFieldName();
      Assert.assertEquals("isbn", _fieldName_1);
      EList<ClassCloner> _cloners_5 = body.getCloners();
      ClassCloner _head_4 = IterableExtensions.<ClassCloner>head(_cloners_5);
      EList<FieldClonerType> _fields_3 = _head_4.getFields();
      FieldClonerType _get_2 = _fields_3.get(2);
      String _fieldName_2 = ((ComplexField) _get_2).getFieldName();
      Assert.assertEquals("article", _fieldName_2);
      EList<ClassCloner> _cloners_6 = body.getCloners();
      ClassCloner _head_5 = IterableExtensions.<ClassCloner>head(_cloners_6);
      EList<FieldClonerType> _fields_4 = _head_5.getFields();
      FieldClonerType _get_3 = _fields_4.get(2);
      final ComplexField article = ((ComplexField) _get_3);
      EList<FieldClonerType> _fields_5 = article.getFields();
      FieldClonerType _get_4 = _fields_5.get(0);
      String _fieldName_3 = ((SimpleField) _get_4).getFieldName();
      Assert.assertEquals("name", _fieldName_3);
      EList<FieldClonerType> _fields_6 = article.getFields();
      FieldClonerType _get_5 = _fields_6.get(1);
      String _fieldName_4 = ((SimpleExcludedField) _get_5).getFieldName();
      Assert.assertEquals("author", _fieldName_4);
      EList<FieldClonerType> _fields_7 = article.getFields();
      FieldClonerType _get_6 = _fields_7.get(2);
      String _fieldName_5 = ((SimpleExcludedField) _get_6).getFieldName();
      Assert.assertEquals("pageNumber", _fieldName_5);
      EList<FieldClonerType> _fields_8 = article.getFields();
      FieldClonerType _get_7 = _fields_8.get(3);
      String _fieldName_6 = ((ComplexField) _get_7).getFieldName();
      Assert.assertEquals("section", _fieldName_6);
      EList<FieldClonerType> _fields_9 = article.getFields();
      FieldClonerType _get_8 = _fields_9.get(3);
      final ComplexField section = ((ComplexField) _get_8);
      EList<FieldClonerType> _fields_10 = section.getFields();
      FieldClonerType _get_9 = _fields_10.get(0);
      String _fieldName_7 = ((SimpleExcludedField) _get_9).getFieldName();
      Assert.assertEquals("name", _fieldName_7);
      EList<FieldClonerType> _fields_11 = section.getFields();
      FieldClonerType _get_10 = _fields_11.get(1);
      String _fieldName_8 = ((SimpleField) _get_10).getFieldName();
      Assert.assertEquals("description", _fieldName_8);
      EList<FieldClonerType> _fields_12 = section.getFields();
      FieldClonerType _get_11 = _fields_12.get(2);
      String _fieldName_9 = ((SimpleExcludedField) _get_11).getFieldName();
      Assert.assertEquals("paragraph", _fieldName_9);
    } catch (Throwable _e) {
      throw Exceptions.sneakyThrow(_e);
    }
  }
  
  @Test
  public void testSimpleExclusions() {
    try {
      StringConcatenation _builder = new StringConcatenation();
      _builder.append("deepClone");
      _builder.newLine();
      _builder.append("package a.b.c");
      _builder.newLine();
      _builder.append("a.b.c.WeekdayOpeningHours {");
      _builder.newLine();
      _builder.append("\t");
      _builder.append("-weekDay");
      _builder.newLine();
      _builder.append("\t");
      _builder.append("hours");
      _builder.newLine();
      _builder.append("}");
      final String code = _builder.toString();
      final Model model = this.parser.parse(code);
      this.validationHelper.assertNoErrors(model);
      final Body body = ((Body) model);
      EList<ClassCloner> _cloners = body.getCloners();
      int _size = _cloners.size();
      Assert.assertEquals(1, _size);
      EList<ClassCloner> _cloners_1 = body.getCloners();
      ClassCloner _head = IterableExtensions.<ClassCloner>head(_cloners_1);
      JvmTypeReference _classToClone = _head.getClassToClone();
      String _qualifiedName = _classToClone.getQualifiedName();
      Assert.assertEquals("a.b.c.WeekdayOpeningHours", _qualifiedName);
      EList<ClassCloner> _cloners_2 = body.getCloners();
      ClassCloner _head_1 = IterableExtensions.<ClassCloner>head(_cloners_2);
      EList<FieldClonerType> _fields = _head_1.getFields();
      int _size_1 = _fields.size();
      Assert.assertEquals(2, _size_1);
      EList<ClassCloner> _cloners_3 = body.getCloners();
      ClassCloner _head_2 = IterableExtensions.<ClassCloner>head(_cloners_3);
      EList<FieldClonerType> _fields_1 = _head_2.getFields();
      FieldClonerType _get = _fields_1.get(0);
      String _fieldName = ((SimpleExcludedField) _get).getFieldName();
      Assert.assertEquals("weekDay", _fieldName);
      EList<ClassCloner> _cloners_4 = body.getCloners();
      ClassCloner _head_3 = IterableExtensions.<ClassCloner>head(_cloners_4);
      EList<FieldClonerType> _fields_2 = _head_3.getFields();
      FieldClonerType _get_1 = _fields_2.get(1);
      String _fieldName_1 = ((SimpleField) _get_1).getFieldName();
      Assert.assertEquals("hours", _fieldName_1);
    } catch (Throwable _e) {
      throw Exceptions.sneakyThrow(_e);
    }
  }
  
  @Test
  public void commensShouldBeSupportedButTransparent() {
    try {
      StringConcatenation _builder = new StringConcatenation();
      _builder.append("deepClone");
      _builder.newLine();
      _builder.append("package a.b.c");
      _builder.newLine();
      _builder.append("// comment 1");
      _builder.newLine();
      _builder.append("/* comment 2 */");
      _builder.newLine();
      _builder.append("a.b.c.WeekdayOpeningHours {");
      _builder.newLine();
      _builder.append("\t");
      _builder.append("//comment 3");
      _builder.newLine();
      _builder.append("\t");
      _builder.append("weekDay  // comment 4");
      _builder.newLine();
      _builder.append("\t");
      _builder.append("/* ");
      _builder.newLine();
      _builder.append("\t");
      _builder.append("long");
      _builder.newLine();
      _builder.append("\t");
      _builder.append("multiline ");
      _builder.newLine();
      _builder.append("\t");
      _builder.append("comment ");
      _builder.newLine();
      _builder.append("\t");
      _builder.append("*/");
      _builder.newLine();
      _builder.append("\t");
      _builder.append("hours // comment 5");
      _builder.newLine();
      _builder.append("}");
      _builder.newLine();
      _builder.append("// comment 6");
      _builder.newLine();
      final String code = _builder.toString();
      final Model model = this.parser.parse(code);
      this.validationHelper.assertNoErrors(model);
      final Body body = ((Body) model);
      EList<ClassCloner> _cloners = body.getCloners();
      int _size = _cloners.size();
      Assert.assertEquals(1, _size);
      EList<ClassCloner> _cloners_1 = body.getCloners();
      ClassCloner _head = IterableExtensions.<ClassCloner>head(_cloners_1);
      JvmTypeReference _classToClone = _head.getClassToClone();
      String _qualifiedName = _classToClone.getQualifiedName();
      Assert.assertEquals("a.b.c.WeekdayOpeningHours", _qualifiedName);
      EList<ClassCloner> _cloners_2 = body.getCloners();
      ClassCloner _head_1 = IterableExtensions.<ClassCloner>head(_cloners_2);
      EList<FieldClonerType> _fields = _head_1.getFields();
      int _size_1 = _fields.size();
      Assert.assertEquals(2, _size_1);
      EList<ClassCloner> _cloners_3 = body.getCloners();
      ClassCloner _head_2 = IterableExtensions.<ClassCloner>head(_cloners_3);
      EList<FieldClonerType> _fields_1 = _head_2.getFields();
      FieldClonerType _get = _fields_1.get(0);
      String _fieldName = ((SimpleField) _get).getFieldName();
      Assert.assertEquals("weekDay", _fieldName);
      EList<ClassCloner> _cloners_4 = body.getCloners();
      ClassCloner _head_3 = IterableExtensions.<ClassCloner>head(_cloners_4);
      EList<FieldClonerType> _fields_2 = _head_3.getFields();
      FieldClonerType _get_1 = _fields_2.get(1);
      String _fieldName_1 = ((SimpleField) _get_1).getFieldName();
      Assert.assertEquals("hours", _fieldName_1);
    } catch (Throwable _e) {
      throw Exceptions.sneakyThrow(_e);
    }
  }
  
  @Test
  public void complexExampleWithClonerReferences() {
    try {
      StringConcatenation _builder = new StringConcatenation();
      _builder.append("deepClone");
      _builder.newLine();
      _builder.newLine();
      _builder.append("package a.b.m");
      _builder.newLine();
      _builder.newLine();
      _builder.append("a.b.m.Book {");
      _builder.newLine();
      _builder.append("\t");
      _builder.append("name");
      _builder.newLine();
      _builder.append("\t");
      _builder.append("-author");
      _builder.newLine();
      _builder.append("\t");
      _builder.append("&section SectionDeep");
      _builder.newLine();
      _builder.append("}");
      _builder.newLine();
      _builder.newLine();
      _builder.append("a.b.m.Section SectionDeep {");
      _builder.newLine();
      _builder.append("\t");
      _builder.append("name");
      _builder.newLine();
      _builder.append("\t");
      _builder.append("description");
      _builder.newLine();
      _builder.append("\t");
      _builder.append("paragraph {");
      _builder.newLine();
      _builder.append("\t\t");
      _builder.append("number");
      _builder.newLine();
      _builder.append("\t\t");
      _builder.append("text");
      _builder.newLine();
      _builder.append("\t");
      _builder.append("}");
      _builder.newLine();
      _builder.append("}");
      _builder.newLine();
      _builder.newLine();
      _builder.append("a.b.m.Section SectionReduced {");
      _builder.newLine();
      _builder.append("\t");
      _builder.append("name");
      _builder.newLine();
      _builder.append("\t");
      _builder.append("description");
      _builder.newLine();
      _builder.append("\t");
      _builder.append("-paragraph");
      _builder.newLine();
      _builder.append("}");
      _builder.newLine();
      final String code = _builder.toString();
      final Model model = this.parser.parse(code);
      this.validationHelper.assertNoErrors(model);
      final Body body = ((Body) model);
      EList<ClassCloner> _cloners = body.getCloners();
      int _size = _cloners.size();
      Assert.assertEquals(3, _size);
      EList<ClassCloner> _cloners_1 = body.getCloners();
      ClassCloner _get = _cloners_1.get(0);
      JvmTypeReference _classToClone = _get.getClassToClone();
      String _qualifiedName = _classToClone.getQualifiedName();
      Assert.assertEquals("a.b.m.Book", _qualifiedName);
      EList<ClassCloner> _cloners_2 = body.getCloners();
      ClassCloner _get_1 = _cloners_2.get(0);
      String _name = _get_1.getName();
      Assert.assertEquals(null, _name);
      EList<ClassCloner> _cloners_3 = body.getCloners();
      ClassCloner _get_2 = _cloners_3.get(1);
      JvmTypeReference _classToClone_1 = _get_2.getClassToClone();
      String _qualifiedName_1 = _classToClone_1.getQualifiedName();
      Assert.assertEquals("a.b.m.Section", _qualifiedName_1);
      EList<ClassCloner> _cloners_4 = body.getCloners();
      ClassCloner _get_3 = _cloners_4.get(1);
      String _name_1 = _get_3.getName();
      Assert.assertEquals("SectionDeep", _name_1);
      EList<ClassCloner> _cloners_5 = body.getCloners();
      ClassCloner _get_4 = _cloners_5.get(2);
      JvmTypeReference _classToClone_2 = _get_4.getClassToClone();
      String _qualifiedName_2 = _classToClone_2.getQualifiedName();
      Assert.assertEquals("a.b.m.Section", _qualifiedName_2);
      EList<ClassCloner> _cloners_6 = body.getCloners();
      ClassCloner _get_5 = _cloners_6.get(2);
      String _name_2 = _get_5.getName();
      Assert.assertEquals("SectionReduced", _name_2);
      EList<ClassCloner> _cloners_7 = body.getCloners();
      final ClassCloner bookCloner = _cloners_7.get(0);
      EList<FieldClonerType> _fields = bookCloner.getFields();
      int _size_1 = _fields.size();
      Assert.assertEquals(3, _size_1);
      EList<FieldClonerType> _fields_1 = bookCloner.getFields();
      FieldClonerType _get_6 = _fields_1.get(0);
      String _fieldName = ((SimpleField) _get_6).getFieldName();
      Assert.assertEquals("name", _fieldName);
      EList<FieldClonerType> _fields_2 = bookCloner.getFields();
      FieldClonerType _get_7 = _fields_2.get(1);
      String _fieldName_1 = ((SimpleExcludedField) _get_7).getFieldName();
      Assert.assertEquals("author", _fieldName_1);
      EList<FieldClonerType> _fields_3 = bookCloner.getFields();
      FieldClonerType _get_8 = _fields_3.get(2);
      String _fieldName_2 = ((ReferenceField) _get_8).getFieldName();
      Assert.assertEquals("section", _fieldName_2);
      EList<FieldClonerType> _fields_4 = bookCloner.getFields();
      FieldClonerType _get_9 = _fields_4.get(2);
      final ReferenceField clonerReferenceField = ((ReferenceField) _get_9);
      ClassCloner _clonerReference = clonerReferenceField.getClonerReference();
      String _name_3 = _clonerReference.getName();
      Assert.assertEquals("SectionDeep", _name_3);
      ClassCloner _clonerReference_1 = clonerReferenceField.getClonerReference();
      JvmTypeReference _classToClone_3 = _clonerReference_1.getClassToClone();
      String _qualifiedName_3 = _classToClone_3.getQualifiedName();
      Assert.assertEquals("a.b.m.Section", _qualifiedName_3);
      EList<FieldClonerType> _fields_5 = bookCloner.getFields();
      FieldClonerType _get_10 = _fields_5.get(2);
      ClassCloner _clonerReference_2 = ((ReferenceField) _get_10).getClonerReference();
      EList<FieldClonerType> _fields_6 = _clonerReference_2.getFields();
      FieldClonerType _get_11 = _fields_6.get(1);
      String _fieldName_3 = ((SimpleField) _get_11).getFieldName();
      Assert.assertEquals("description", _fieldName_3);
    } catch (Throwable _e) {
      throw Exceptions.sneakyThrow(_e);
    }
  }
}
