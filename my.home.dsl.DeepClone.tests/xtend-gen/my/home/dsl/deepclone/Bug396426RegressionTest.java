package my.home.dsl.deepclone;

import com.google.inject.Inject;
import java.util.ArrayList;
import my.home.dsl.DeepCloneInjectorProvider;
import my.home.dsl.deepClone.Body;
import my.home.dsl.deepClone.ClassCloner;
import my.home.dsl.deepClone.ContainerType;
import my.home.dsl.deepClone.Model;
import my.home.dsl.utils.DeepCloneUtils;
import org.eclipse.emf.common.util.EList;
import org.eclipse.xtend2.lib.StringConcatenation;
import org.eclipse.xtext.junit4.InjectWith;
import org.eclipse.xtext.junit4.XtextRunner;
import org.eclipse.xtext.junit4.util.ParseHelper;
import org.eclipse.xtext.resource.SaveOptions;
import org.eclipse.xtext.serializer.ISerializer;
import org.eclipse.xtext.xbase.lib.CollectionLiterals;
import org.eclipse.xtext.xbase.lib.Exceptions;
import org.eclipse.xtext.xbase.lib.Extension;
import org.junit.Assert;
import org.junit.Test;
import org.junit.runner.RunWith;

/**
 * https://bugs.eclipse.org/bugs/show_bug.cgi?id=396426
 * For more similar tests look at Test6DeepCloneModelManipulation
 */
@InjectWith(DeepCloneInjectorProvider.class)
@RunWith(XtextRunner.class)
@SuppressWarnings("all")
public class Bug396426RegressionTest {
  @Inject
  private ParseHelper<Model> parser;
  
  @Inject
  @Extension
  private ISerializer _iSerializer;
  
  @Inject
  @Extension
  private DeepCloneUtils _deepCloneUtils;
  
  @Test
  public void whenAddMissingFieldToBooClonerFormattingOfSectionElementShouldBePreserved_withFormattingOff() {
    try {
      StringConcatenation _builder = new StringConcatenation();
      _builder.append("deepClone");
      _builder.newLine();
      _builder.append("package my.home.cloners");
      _builder.newLine();
      _builder.append("a.b.m.Book BookCloner {");
      _builder.newLine();
      _builder.append("\t");
      _builder.append("name");
      _builder.newLine();
      _builder.append("\t");
      _builder.append("-author");
      _builder.newLine();
      _builder.append("\t");
      _builder.append("section {");
      _builder.newLine();
      _builder.append("\t\t");
      _builder.append("name");
      _builder.newLine();
      _builder.append("\t\t");
      _builder.append("description");
      _builder.newLine();
      _builder.append("\t");
      _builder.append("}");
      _builder.newLine();
      _builder.append("}");
      _builder.newLine();
      final String sample = _builder.toString();
      final Model model = this.parser.parse(sample);
      EList<ClassCloner> _cloners = ((Body) model).getCloners();
      ClassCloner _get = _cloners.get(0);
      final ContainerType cloner = this._deepCloneUtils.asContainer(_get);
      ArrayList<String> _newArrayList = CollectionLiterals.<String>newArrayList("paragraph");
      this._deepCloneUtils.addFields(cloner, _newArrayList);
      final String result = this._iSerializer.serialize(model);
      StringConcatenation _builder_1 = new StringConcatenation();
      _builder_1.append("deepClone");
      _builder_1.newLine();
      _builder_1.append("package my.home.cloners");
      _builder_1.newLine();
      _builder_1.append("a.b.m.Book BookCloner {");
      _builder_1.newLine();
      _builder_1.append("\t");
      _builder_1.append("name");
      _builder_1.newLine();
      _builder_1.append("\t");
      _builder_1.append("-author");
      _builder_1.newLine();
      _builder_1.append("\t");
      _builder_1.append("section {");
      _builder_1.newLine();
      _builder_1.append("\t\t");
      _builder_1.append("name");
      _builder_1.newLine();
      _builder_1.append("\t\t");
      _builder_1.append("description");
      _builder_1.newLine();
      _builder_1.append("\t");
      _builder_1.append("}");
      _builder_1.newLine();
      _builder_1.append("\t");
      _builder_1.append("paragraph");
      _builder_1.newLine();
      _builder_1.append("}");
      final String expectedModifiedSample = _builder_1.toString();
      Assert.assertEquals(expectedModifiedSample, result);
    } catch (Throwable _e) {
      throw Exceptions.sneakyThrow(_e);
    }
  }
  
  @Test
  public void whenAddMissingFieldToBooClonerFormattingOfSectionElementShouldBePreserved_withFormattingOn() {
    try {
      StringConcatenation _builder = new StringConcatenation();
      _builder.append("deepClone");
      _builder.newLine();
      _builder.append("package my.home.cloners");
      _builder.newLine();
      _builder.append("a.b.m.Book BookCloner {");
      _builder.newLine();
      _builder.append("\t");
      _builder.append("name");
      _builder.newLine();
      _builder.append("\t");
      _builder.append("-author");
      _builder.newLine();
      _builder.append("\t");
      _builder.append("section {");
      _builder.newLine();
      _builder.append("\t\t");
      _builder.append("name");
      _builder.newLine();
      _builder.append("\t\t");
      _builder.append("description");
      _builder.newLine();
      _builder.append("\t");
      _builder.append("}");
      _builder.newLine();
      _builder.append("}");
      _builder.newLine();
      final String sample = _builder.toString();
      final Model model = this.parser.parse(sample);
      EList<ClassCloner> _cloners = ((Body) model).getCloners();
      ClassCloner _get = _cloners.get(0);
      final ContainerType cloner = this._deepCloneUtils.asContainer(_get);
      ArrayList<String> _newArrayList = CollectionLiterals.<String>newArrayList("paragraph");
      this._deepCloneUtils.addFields(cloner, _newArrayList);
      SaveOptions.Builder _newBuilder = SaveOptions.newBuilder();
      SaveOptions.Builder _format = _newBuilder.format();
      SaveOptions _options = _format.getOptions();
      final String result = this._iSerializer.serialize(model, _options);
      StringConcatenation _builder_1 = new StringConcatenation();
      _builder_1.append("deepClone");
      _builder_1.newLine();
      _builder_1.append("package my.home.cloners");
      _builder_1.newLine();
      _builder_1.append("a.b.m.Book BookCloner {");
      _builder_1.newLine();
      _builder_1.append("\t");
      _builder_1.append("name");
      _builder_1.newLine();
      _builder_1.append("\t");
      _builder_1.append("-author");
      _builder_1.newLine();
      _builder_1.append("\t");
      _builder_1.append("section {");
      _builder_1.newLine();
      _builder_1.append("\t\t");
      _builder_1.append("name");
      _builder_1.newLine();
      _builder_1.append("\t\t");
      _builder_1.append("description");
      _builder_1.newLine();
      _builder_1.append("\t");
      _builder_1.append("}");
      _builder_1.newLine();
      _builder_1.append("\t");
      _builder_1.append("paragraph");
      _builder_1.newLine();
      _builder_1.append("}");
      final String expectedModifiedSample = _builder_1.toString();
      Assert.assertEquals(expectedModifiedSample, result);
    } catch (Throwable _e) {
      throw Exceptions.sneakyThrow(_e);
    }
  }
}
