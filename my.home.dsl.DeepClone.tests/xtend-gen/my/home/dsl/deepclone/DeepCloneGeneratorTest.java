package my.home.dsl.deepclone;

import com.google.inject.Inject;
import java.util.Map;
import my.home.dsl.DeepCloneInjectorProvider;
import my.home.dsl.deepClone.Model;
import org.eclipse.emf.ecore.resource.Resource;
import org.eclipse.xtend2.lib.StringConcatenation;
import org.eclipse.xtext.generator.IFileSystemAccess;
import org.eclipse.xtext.generator.IGenerator;
import org.eclipse.xtext.generator.InMemoryFileSystemAccess;
import org.eclipse.xtext.junit4.InjectWith;
import org.eclipse.xtext.junit4.XtextRunner;
import org.eclipse.xtext.junit4.util.ParseHelper;
import org.eclipse.xtext.junit4.validation.ValidationTestHelper;
import org.eclipse.xtext.xbase.lib.Exceptions;
import org.junit.Assert;
import org.junit.Test;
import org.junit.runner.RunWith;

/**
 * Parse sample DeepClone DSL file and test DeepClone to Java generator.
 * Files are generated to memory, see {@link InMemoryFileSystemAccess}, perfect for testing.
 * @see http://christiandietrich.wordpress.com/2012/05/08/unittesting-xtend-generators/
 * 
 * @author espinosa
 */
@RunWith(XtextRunner.class)
@InjectWith(DeepCloneInjectorProvider.class)
@SuppressWarnings("all")
public class DeepCloneGeneratorTest {
  @Inject
  private IGenerator generatorDslToJava;
  
  @Inject
  private ParseHelper<Model> parseHelper;
  
  @Inject
  private ValidationTestHelper validationHelper;
  
  @Test
  public void testDeepStructure() {
    try {
      StringConcatenation _builder = new StringConcatenation();
      _builder.append("deepClone");
      _builder.newLine();
      _builder.newLine();
      _builder.append("package my.home.cloners");
      _builder.newLine();
      _builder.newLine();
      _builder.append("a.b.m.Magazine {");
      _builder.newLine();
      _builder.append("\t");
      _builder.append("name");
      _builder.newLine();
      _builder.append("\t");
      _builder.append("isbn");
      _builder.newLine();
      _builder.append("\t");
      _builder.append("article {");
      _builder.newLine();
      _builder.append("\t\t");
      _builder.append("name");
      _builder.newLine();
      _builder.append("\t\t");
      _builder.append("author");
      _builder.newLine();
      _builder.append("\t\t");
      _builder.append("pageNumber");
      _builder.newLine();
      _builder.append("\t\t");
      _builder.append("section {");
      _builder.newLine();
      _builder.append("\t\t\t");
      _builder.append("name");
      _builder.newLine();
      _builder.append("\t\t\t");
      _builder.append("description");
      _builder.newLine();
      _builder.append("\t\t\t");
      _builder.append("paragraph {");
      _builder.newLine();
      _builder.append("\t\t\t\t");
      _builder.append("number");
      _builder.newLine();
      _builder.append("\t\t\t\t");
      _builder.append("text");
      _builder.newLine();
      _builder.append("\t\t\t");
      _builder.append("}");
      _builder.newLine();
      _builder.append("\t\t");
      _builder.append("}");
      _builder.newLine();
      _builder.append("\t");
      _builder.append("}");
      _builder.newLine();
      _builder.append("}");
      _builder.newLine();
      final String code = _builder.toString();
      final Model model = this.parseHelper.parse(code);
      this.validationHelper.assertNoIssues(model);
      InMemoryFileSystemAccess _inMemoryFileSystemAccess = new InMemoryFileSystemAccess();
      final InMemoryFileSystemAccess fsa = _inMemoryFileSystemAccess;
      Resource _eResource = model.eResource();
      this.generatorDslToJava.doGenerate(_eResource, fsa);
      Map<String,CharSequence> _textFiles = fsa.getTextFiles();
      int _size = _textFiles.size();
      Assert.assertEquals(1, _size);
      final String topClonerJavaFileName = (IFileSystemAccess.DEFAULT_OUTPUT + "my/home/cloners/MagazineCloner.java");
      Map<String,CharSequence> _textFiles_1 = fsa.getTextFiles();
      boolean _containsKey = _textFiles_1.containsKey(topClonerJavaFileName);
      Assert.assertTrue(_containsKey);
      StringConcatenation _builder_1 = new StringConcatenation();
      _builder_1.append("package my.home.cloners;");
      _builder_1.newLine();
      _builder_1.append("public class MagazineCloner {");
      _builder_1.newLine();
      _builder_1.append("\t");
      _builder_1.append("public a.b.m.Magazine apply(a.b.m.Magazine other) {");
      _builder_1.newLine();
      _builder_1.append("\t\t");
      _builder_1.append("if (other == null) return null;");
      _builder_1.newLine();
      _builder_1.append("\t\t");
      _builder_1.append("a.b.m.Magazine it = new a.b.m.Magazine();");
      _builder_1.newLine();
      _builder_1.append("\t\t");
      _builder_1.append("it.setName(other.getName());");
      _builder_1.newLine();
      _builder_1.append("\t\t");
      _builder_1.append("it.setIsbn(other.getIsbn());");
      _builder_1.newLine();
      _builder_1.append("\t\t");
      _builder_1.append("it.setArticle(articleCloner.apply(other.getArticle()));");
      _builder_1.newLine();
      _builder_1.append("\t\t");
      _builder_1.append("return it;");
      _builder_1.newLine();
      _builder_1.append("\t");
      _builder_1.append("}");
      _builder_1.newLine();
      _builder_1.append("\t");
      _builder_1.newLine();
      _builder_1.append("\t");
      _builder_1.append("private final ArticleCloner articleCloner = new ArticleCloner();");
      _builder_1.newLine();
      _builder_1.append("\t");
      _builder_1.append("public static class ArticleCloner {");
      _builder_1.newLine();
      _builder_1.append("\t\t");
      _builder_1.append("public a.b.m.Article apply(a.b.m.Article other) {");
      _builder_1.newLine();
      _builder_1.append("\t\t\t");
      _builder_1.append("if (other == null) return null;");
      _builder_1.newLine();
      _builder_1.append("\t\t\t");
      _builder_1.append("a.b.m.Article it = new a.b.m.Article();");
      _builder_1.newLine();
      _builder_1.append("\t\t\t");
      _builder_1.append("it.setName(other.getName());");
      _builder_1.newLine();
      _builder_1.append("\t\t\t");
      _builder_1.append("it.setAuthor(other.getAuthor());");
      _builder_1.newLine();
      _builder_1.append("\t\t\t");
      _builder_1.append("it.setPageNumber(other.getPageNumber());");
      _builder_1.newLine();
      _builder_1.append("\t\t\t");
      _builder_1.append("it.setSection(sectionCloner.apply(other.getSection()));");
      _builder_1.newLine();
      _builder_1.append("\t\t\t");
      _builder_1.append("return it;");
      _builder_1.newLine();
      _builder_1.append("\t\t");
      _builder_1.append("}");
      _builder_1.newLine();
      _builder_1.append("\t\t");
      _builder_1.newLine();
      _builder_1.append("\t\t");
      _builder_1.append("private final SectionCloner sectionCloner = new SectionCloner();");
      _builder_1.newLine();
      _builder_1.append("\t\t");
      _builder_1.append("public static class SectionCloner {");
      _builder_1.newLine();
      _builder_1.append("\t\t\t");
      _builder_1.append("public a.b.m.Section apply(a.b.m.Section other) {");
      _builder_1.newLine();
      _builder_1.append("\t\t\t\t");
      _builder_1.append("if (other == null) return null;");
      _builder_1.newLine();
      _builder_1.append("\t\t\t\t");
      _builder_1.append("a.b.m.Section it = new a.b.m.Section();");
      _builder_1.newLine();
      _builder_1.append("\t\t\t\t");
      _builder_1.append("it.setName(other.getName());");
      _builder_1.newLine();
      _builder_1.append("\t\t\t\t");
      _builder_1.append("it.setDescription(other.getDescription());");
      _builder_1.newLine();
      _builder_1.append("\t\t\t\t");
      _builder_1.append("it.setParagraph(paragraphCloner.apply(other.getParagraph()));");
      _builder_1.newLine();
      _builder_1.append("\t\t\t\t");
      _builder_1.append("return it;");
      _builder_1.newLine();
      _builder_1.append("\t\t\t");
      _builder_1.append("}");
      _builder_1.newLine();
      _builder_1.append("\t\t\t");
      _builder_1.newLine();
      _builder_1.append("\t\t\t");
      _builder_1.append("private final ParagraphCloner paragraphCloner = new ParagraphCloner();");
      _builder_1.newLine();
      _builder_1.append("\t\t\t");
      _builder_1.append("public static class ParagraphCloner {");
      _builder_1.newLine();
      _builder_1.append("\t\t\t\t");
      _builder_1.append("public a.b.m.Paragraph apply(a.b.m.Paragraph other) {");
      _builder_1.newLine();
      _builder_1.append("\t\t\t\t\t");
      _builder_1.append("if (other == null) return null;");
      _builder_1.newLine();
      _builder_1.append("\t\t\t\t\t");
      _builder_1.append("a.b.m.Paragraph it = new a.b.m.Paragraph();");
      _builder_1.newLine();
      _builder_1.append("\t\t\t\t\t");
      _builder_1.append("it.setNumber(other.getNumber());");
      _builder_1.newLine();
      _builder_1.append("\t\t\t\t\t");
      _builder_1.append("it.setText(other.getText());");
      _builder_1.newLine();
      _builder_1.append("\t\t\t\t\t");
      _builder_1.append("return it;");
      _builder_1.newLine();
      _builder_1.append("\t\t\t\t");
      _builder_1.append("}");
      _builder_1.newLine();
      _builder_1.append("\t\t\t");
      _builder_1.append("}");
      _builder_1.newLine();
      _builder_1.append("\t\t");
      _builder_1.append("}");
      _builder_1.newLine();
      _builder_1.append("\t");
      _builder_1.append("}");
      _builder_1.newLine();
      _builder_1.append("}");
      _builder_1.newLine();
      String _string = _builder_1.toString();
      Map<String,CharSequence> _textFiles_2 = fsa.getTextFiles();
      CharSequence _get = _textFiles_2.get(topClonerJavaFileName);
      String _string_1 = _get.toString();
      Assert.assertEquals(_string, _string_1);
    } catch (Throwable _e) {
      throw Exceptions.sneakyThrow(_e);
    }
  }
  
  @Test
  public void testDeepStructureWithExclusions() {
    try {
      StringConcatenation _builder = new StringConcatenation();
      _builder.append("deepClone");
      _builder.newLine();
      _builder.newLine();
      _builder.append("package my.home.cloners.reduced");
      _builder.newLine();
      _builder.newLine();
      _builder.append("a.b.m.Magazine {");
      _builder.newLine();
      _builder.append("\t");
      _builder.append("name");
      _builder.newLine();
      _builder.append("\t");
      _builder.append("-isbn");
      _builder.newLine();
      _builder.append("\t");
      _builder.append("article {");
      _builder.newLine();
      _builder.append("\t\t");
      _builder.append("name");
      _builder.newLine();
      _builder.append("\t\t");
      _builder.append("-author");
      _builder.newLine();
      _builder.append("\t\t");
      _builder.append("-pageNumber");
      _builder.newLine();
      _builder.append("\t\t");
      _builder.append("section {");
      _builder.newLine();
      _builder.append("\t\t\t");
      _builder.append("-name");
      _builder.newLine();
      _builder.append("\t\t\t");
      _builder.append("description");
      _builder.newLine();
      _builder.append("\t\t\t");
      _builder.append("-paragraph");
      _builder.newLine();
      _builder.append("\t\t");
      _builder.append("}");
      _builder.newLine();
      _builder.append("\t");
      _builder.append("}");
      _builder.newLine();
      _builder.append("}");
      _builder.newLine();
      final String code = _builder.toString();
      final Model model = this.parseHelper.parse(code);
      this.validationHelper.assertNoIssues(model);
      InMemoryFileSystemAccess _inMemoryFileSystemAccess = new InMemoryFileSystemAccess();
      final InMemoryFileSystemAccess fsa = _inMemoryFileSystemAccess;
      Resource _eResource = model.eResource();
      this.generatorDslToJava.doGenerate(_eResource, fsa);
      Map<String,CharSequence> _textFiles = fsa.getTextFiles();
      int _size = _textFiles.size();
      Assert.assertEquals(1, _size);
      final String topClonerJavaFileName = (IFileSystemAccess.DEFAULT_OUTPUT + "my/home/cloners/reduced/MagazineCloner.java");
      Map<String,CharSequence> _textFiles_1 = fsa.getTextFiles();
      boolean _containsKey = _textFiles_1.containsKey(topClonerJavaFileName);
      Assert.assertTrue(_containsKey);
      StringConcatenation _builder_1 = new StringConcatenation();
      _builder_1.append("package my.home.cloners.reduced;");
      _builder_1.newLine();
      _builder_1.append("public class MagazineCloner {");
      _builder_1.newLine();
      _builder_1.append("\t");
      _builder_1.append("public a.b.m.Magazine apply(a.b.m.Magazine other) {");
      _builder_1.newLine();
      _builder_1.append("\t\t");
      _builder_1.append("if (other == null) return null;");
      _builder_1.newLine();
      _builder_1.append("\t\t");
      _builder_1.append("a.b.m.Magazine it = new a.b.m.Magazine();");
      _builder_1.newLine();
      _builder_1.append("\t\t");
      _builder_1.append("it.setName(other.getName());");
      _builder_1.newLine();
      _builder_1.append("\t\t");
      _builder_1.append("it.setArticle(articleCloner.apply(other.getArticle()));");
      _builder_1.newLine();
      _builder_1.append("\t\t");
      _builder_1.append("return it;");
      _builder_1.newLine();
      _builder_1.append("\t");
      _builder_1.append("}");
      _builder_1.newLine();
      _builder_1.append("\t");
      _builder_1.newLine();
      _builder_1.append("\t");
      _builder_1.append("private final ArticleCloner articleCloner = new ArticleCloner();");
      _builder_1.newLine();
      _builder_1.append("\t");
      _builder_1.append("public static class ArticleCloner {");
      _builder_1.newLine();
      _builder_1.append("\t\t");
      _builder_1.append("public a.b.m.Article apply(a.b.m.Article other) {");
      _builder_1.newLine();
      _builder_1.append("\t\t\t");
      _builder_1.append("if (other == null) return null;");
      _builder_1.newLine();
      _builder_1.append("\t\t\t");
      _builder_1.append("a.b.m.Article it = new a.b.m.Article();");
      _builder_1.newLine();
      _builder_1.append("\t\t\t");
      _builder_1.append("it.setName(other.getName());");
      _builder_1.newLine();
      _builder_1.append("\t\t\t");
      _builder_1.append("it.setSection(sectionCloner.apply(other.getSection()));");
      _builder_1.newLine();
      _builder_1.append("\t\t\t");
      _builder_1.append("return it;");
      _builder_1.newLine();
      _builder_1.append("\t\t");
      _builder_1.append("}");
      _builder_1.newLine();
      _builder_1.append("\t\t");
      _builder_1.newLine();
      _builder_1.append("\t\t");
      _builder_1.append("private final SectionCloner sectionCloner = new SectionCloner();");
      _builder_1.newLine();
      _builder_1.append("\t\t");
      _builder_1.append("public static class SectionCloner {");
      _builder_1.newLine();
      _builder_1.append("\t\t\t");
      _builder_1.append("public a.b.m.Section apply(a.b.m.Section other) {");
      _builder_1.newLine();
      _builder_1.append("\t\t\t\t");
      _builder_1.append("if (other == null) return null;");
      _builder_1.newLine();
      _builder_1.append("\t\t\t\t");
      _builder_1.append("a.b.m.Section it = new a.b.m.Section();");
      _builder_1.newLine();
      _builder_1.append("\t\t\t\t");
      _builder_1.append("it.setDescription(other.getDescription());");
      _builder_1.newLine();
      _builder_1.append("\t\t\t\t");
      _builder_1.append("return it;");
      _builder_1.newLine();
      _builder_1.append("\t\t\t");
      _builder_1.append("}");
      _builder_1.newLine();
      _builder_1.append("\t\t");
      _builder_1.append("}");
      _builder_1.newLine();
      _builder_1.append("\t");
      _builder_1.append("}");
      _builder_1.newLine();
      _builder_1.append("}");
      _builder_1.newLine();
      String _string = _builder_1.toString();
      Map<String,CharSequence> _textFiles_2 = fsa.getTextFiles();
      CharSequence _get = _textFiles_2.get(topClonerJavaFileName);
      String _string_1 = _get.toString();
      Assert.assertEquals(_string, _string_1);
    } catch (Throwable _e) {
      throw Exceptions.sneakyThrow(_e);
    }
  }
  
  @Test
  public void testWithPackageNameAndEnumsAndFieldWithDifferentFieldAndTypeName() {
    try {
      StringConcatenation _builder = new StringConcatenation();
      _builder.append("deepClone ");
      _builder.newLine();
      _builder.newLine();
      _builder.append("package my.home.cloners");
      _builder.newLine();
      _builder.newLine();
      _builder.append("a.b.c.WeekdayOpeningHours {");
      _builder.newLine();
      _builder.append("\t");
      _builder.append("weekDay");
      _builder.newLine();
      _builder.append("\t");
      _builder.append("hours {");
      _builder.newLine();
      _builder.append("\t\t");
      _builder.append("from");
      _builder.newLine();
      _builder.append("\t\t");
      _builder.append("to");
      _builder.newLine();
      _builder.append("\t");
      _builder.append("}");
      _builder.newLine();
      _builder.append("}");
      final String code = _builder.toString();
      final Model model = this.parseHelper.parse(code);
      this.validationHelper.assertNoErrors(model);
      InMemoryFileSystemAccess _inMemoryFileSystemAccess = new InMemoryFileSystemAccess();
      final InMemoryFileSystemAccess fsa = _inMemoryFileSystemAccess;
      Resource _eResource = model.eResource();
      this.generatorDslToJava.doGenerate(_eResource, fsa);
      Map<String,CharSequence> _textFiles = fsa.getTextFiles();
      int _size = _textFiles.size();
      Assert.assertEquals(1, _size);
      final String topClonerJavaFileName = (IFileSystemAccess.DEFAULT_OUTPUT + "my/home/cloners/WeekdayOpeningHoursCloner.java");
      Map<String,CharSequence> _textFiles_1 = fsa.getTextFiles();
      boolean _containsKey = _textFiles_1.containsKey(topClonerJavaFileName);
      Assert.assertTrue(_containsKey);
      StringConcatenation _builder_1 = new StringConcatenation();
      _builder_1.append("package my.home.cloners;");
      _builder_1.newLine();
      _builder_1.append("public class WeekdayOpeningHoursCloner {");
      _builder_1.newLine();
      _builder_1.append("\t");
      _builder_1.append("public a.b.c.WeekdayOpeningHours apply(a.b.c.WeekdayOpeningHours other) {");
      _builder_1.newLine();
      _builder_1.append("\t\t");
      _builder_1.append("if (other == null) return null;");
      _builder_1.newLine();
      _builder_1.append("\t\t");
      _builder_1.append("a.b.c.WeekdayOpeningHours it = new a.b.c.WeekdayOpeningHours();");
      _builder_1.newLine();
      _builder_1.append("\t\t");
      _builder_1.append("it.setWeekDay(other.getWeekDay());");
      _builder_1.newLine();
      _builder_1.append("\t\t");
      _builder_1.append("it.setHours(hoursCloner.apply(other.getHours()));");
      _builder_1.newLine();
      _builder_1.append("\t\t");
      _builder_1.append("return it;");
      _builder_1.newLine();
      _builder_1.append("\t");
      _builder_1.append("}");
      _builder_1.newLine();
      _builder_1.append("\t");
      _builder_1.newLine();
      _builder_1.append("\t");
      _builder_1.append("private final HoursCloner hoursCloner = new HoursCloner();");
      _builder_1.newLine();
      _builder_1.append("\t");
      _builder_1.append("public static class HoursCloner {");
      _builder_1.newLine();
      _builder_1.append("\t\t");
      _builder_1.append("public a.b.c.FromToItem apply(a.b.c.FromToItem other) {");
      _builder_1.newLine();
      _builder_1.append("\t\t\t");
      _builder_1.append("if (other == null) return null;");
      _builder_1.newLine();
      _builder_1.append("\t\t\t");
      _builder_1.append("a.b.c.FromToItem it = new a.b.c.FromToItem();");
      _builder_1.newLine();
      _builder_1.append("\t\t\t");
      _builder_1.append("it.setFrom(other.getFrom());");
      _builder_1.newLine();
      _builder_1.append("\t\t\t");
      _builder_1.append("it.setTo(other.getTo());");
      _builder_1.newLine();
      _builder_1.append("\t\t\t");
      _builder_1.append("return it;");
      _builder_1.newLine();
      _builder_1.append("\t\t");
      _builder_1.append("}");
      _builder_1.newLine();
      _builder_1.append("\t");
      _builder_1.append("}");
      _builder_1.newLine();
      _builder_1.append("}");
      _builder_1.newLine();
      String _string = _builder_1.toString();
      Map<String,CharSequence> _textFiles_2 = fsa.getTextFiles();
      CharSequence _get = _textFiles_2.get(topClonerJavaFileName);
      String _string_1 = _get.toString();
      Assert.assertEquals(_string, _string_1);
    } catch (Throwable _e) {
      throw Exceptions.sneakyThrow(_e);
    }
  }
  
  @Test
  public void testWithoutPackageName() {
    try {
      StringConcatenation _builder = new StringConcatenation();
      _builder.append("deepClone\t");
      _builder.newLine();
      _builder.append("a.b.c.WeekdayOpeningHours {");
      _builder.newLine();
      _builder.append("\t");
      _builder.append("weekDay ");
      _builder.newLine();
      _builder.append("\t");
      _builder.append("hours { ");
      _builder.newLine();
      _builder.append("\t\t");
      _builder.append("from ");
      _builder.newLine();
      _builder.append("\t\t");
      _builder.append("to ");
      _builder.newLine();
      _builder.append("\t");
      _builder.append("}");
      _builder.newLine();
      _builder.append("}");
      final String code = _builder.toString();
      final Model model = this.parseHelper.parse(code);
      this.validationHelper.assertNoIssues(model);
      InMemoryFileSystemAccess _inMemoryFileSystemAccess = new InMemoryFileSystemAccess();
      final InMemoryFileSystemAccess fsa = _inMemoryFileSystemAccess;
      Resource _eResource = model.eResource();
      this.generatorDslToJava.doGenerate(_eResource, fsa);
      Map<String,CharSequence> _textFiles = fsa.getTextFiles();
      int _size = _textFiles.size();
      Assert.assertEquals(1, _size);
      final String topClonerJavaFileName = (IFileSystemAccess.DEFAULT_OUTPUT + "default/WeekdayOpeningHoursCloner.java");
      Map<String,CharSequence> _textFiles_1 = fsa.getTextFiles();
      boolean _containsKey = _textFiles_1.containsKey(topClonerJavaFileName);
      Assert.assertTrue(_containsKey);
      StringConcatenation _builder_1 = new StringConcatenation();
      _builder_1.append("package default;");
      _builder_1.newLine();
      _builder_1.append("public class WeekdayOpeningHoursCloner {");
      _builder_1.newLine();
      _builder_1.append("\t");
      _builder_1.append("public a.b.c.WeekdayOpeningHours apply(a.b.c.WeekdayOpeningHours other) {");
      _builder_1.newLine();
      _builder_1.append("\t\t");
      _builder_1.append("if (other == null) return null;");
      _builder_1.newLine();
      _builder_1.append("\t\t");
      _builder_1.append("a.b.c.WeekdayOpeningHours it = new a.b.c.WeekdayOpeningHours();");
      _builder_1.newLine();
      _builder_1.append("\t\t");
      _builder_1.append("it.setWeekDay(other.getWeekDay());");
      _builder_1.newLine();
      _builder_1.append("\t\t");
      _builder_1.append("it.setHours(hoursCloner.apply(other.getHours()));");
      _builder_1.newLine();
      _builder_1.append("\t\t");
      _builder_1.append("return it;");
      _builder_1.newLine();
      _builder_1.append("\t");
      _builder_1.append("}");
      _builder_1.newLine();
      _builder_1.append("\t");
      _builder_1.newLine();
      _builder_1.append("\t");
      _builder_1.append("private final HoursCloner hoursCloner = new HoursCloner();");
      _builder_1.newLine();
      _builder_1.append("\t");
      _builder_1.append("public static class HoursCloner {");
      _builder_1.newLine();
      _builder_1.append("\t\t");
      _builder_1.append("public a.b.c.FromToItem apply(a.b.c.FromToItem other) {");
      _builder_1.newLine();
      _builder_1.append("\t\t\t");
      _builder_1.append("if (other == null) return null;");
      _builder_1.newLine();
      _builder_1.append("\t\t\t");
      _builder_1.append("a.b.c.FromToItem it = new a.b.c.FromToItem();");
      _builder_1.newLine();
      _builder_1.append("\t\t\t");
      _builder_1.append("it.setFrom(other.getFrom());");
      _builder_1.newLine();
      _builder_1.append("\t\t\t");
      _builder_1.append("it.setTo(other.getTo());");
      _builder_1.newLine();
      _builder_1.append("\t\t\t");
      _builder_1.append("return it;");
      _builder_1.newLine();
      _builder_1.append("\t\t");
      _builder_1.append("}");
      _builder_1.newLine();
      _builder_1.append("\t");
      _builder_1.append("}");
      _builder_1.newLine();
      _builder_1.append("}");
      _builder_1.newLine();
      String _string = _builder_1.toString();
      Map<String,CharSequence> _textFiles_2 = fsa.getTextFiles();
      CharSequence _get = _textFiles_2.get(topClonerJavaFileName);
      String _string_1 = _get.toString();
      Assert.assertEquals(_string, _string_1);
    } catch (Throwable _e) {
      throw Exceptions.sneakyThrow(_e);
    }
  }
}
