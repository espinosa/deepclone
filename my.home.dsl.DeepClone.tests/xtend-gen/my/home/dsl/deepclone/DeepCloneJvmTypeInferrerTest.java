package my.home.dsl.deepclone;

import com.google.inject.Inject;
import my.home.dsl.DeepCloneInjectorProvider;
import my.home.dsl.deepClone.Body;
import my.home.dsl.deepClone.Model;
import my.home.dsl.deepclone.test.utils.DeepCloneInspector;
import my.home.dsl.utils.DeepCloneUtils;
import org.eclipse.xtend2.lib.StringConcatenation;
import org.eclipse.xtext.junit4.InjectWith;
import org.eclipse.xtext.junit4.XtextRunner;
import org.eclipse.xtext.junit4.util.ParseHelper;
import org.eclipse.xtext.xbase.lib.Exceptions;
import org.eclipse.xtext.xbase.lib.Extension;
import org.junit.Assert;
import org.junit.Test;
import org.junit.runner.RunWith;

@InjectWith(DeepCloneInjectorProvider.class)
@RunWith(XtextRunner.class)
@SuppressWarnings("all")
public class DeepCloneJvmTypeInferrerTest {
  @Inject
  private ParseHelper<Model> parser;
  
  @Inject
  @Extension
  private DeepCloneUtils _deepCloneUtils;
  
  @Inject
  @Extension
  private DeepCloneInspector _deepCloneInspector;
  
  @Test
  public void clonerReferringOnlyExistingTypes() {
    try {
      StringConcatenation _builder = new StringConcatenation();
      _builder.append("deepClone");
      _builder.newLine();
      _builder.append("package my.home.cloners");
      _builder.newLine();
      _builder.append("a.b.m.Book BookCloner {");
      _builder.newLine();
      _builder.append("\t");
      _builder.append("name");
      _builder.newLine();
      _builder.append("\t");
      _builder.append("author");
      _builder.newLine();
      _builder.append("\t");
      _builder.append("section {");
      _builder.newLine();
      _builder.append("\t\t");
      _builder.append("name");
      _builder.newLine();
      _builder.append("\t\t");
      _builder.append("description");
      _builder.newLine();
      _builder.append("\t\t");
      _builder.append("paragraph");
      _builder.newLine();
      _builder.append("\t");
      _builder.append("}");
      _builder.newLine();
      _builder.append("}");
      _builder.newLine();
      final String sample = _builder.toString();
      StringConcatenation _builder_1 = new StringConcatenation();
      _builder_1.append("BookCloner : a.b.m.Book {");
      _builder_1.newLine();
      _builder_1.append("\t");
      _builder_1.append("name : java.lang.String");
      _builder_1.newLine();
      _builder_1.append("\t");
      _builder_1.append("author : java.lang.String");
      _builder_1.newLine();
      _builder_1.append("\t");
      _builder_1.append("section : a.b.m.Section {");
      _builder_1.newLine();
      _builder_1.append("\t\t");
      _builder_1.append("name : java.lang.String");
      _builder_1.newLine();
      _builder_1.append("\t\t");
      _builder_1.append("description : java.lang.String");
      _builder_1.newLine();
      _builder_1.append("\t\t");
      _builder_1.append("paragraph : a.b.m.Paragraph");
      _builder_1.newLine();
      _builder_1.append("\t");
      _builder_1.append("}");
      _builder_1.newLine();
      _builder_1.append("}");
      _builder_1.newLine();
      final String expectedSampleWithInferredTypes = _builder_1.toString();
      final Model model = this.parser.parse(sample);
      this._deepCloneUtils.inferJavaTypes(((Body) model));
      final CharSequence result = this._deepCloneInspector.serialize(((Body) model));
      String _string = expectedSampleWithInferredTypes.toString();
      String _string_1 = result.toString();
      Assert.assertEquals(_string, _string_1);
    } catch (Throwable _e) {
      throw Exceptions.sneakyThrow(_e);
    }
  }
  
  @Test
  public void clonerReferringOnlyExistingTypes_collectionsShouldIndicateParameterType() {
    try {
      StringConcatenation _builder = new StringConcatenation();
      _builder.append("deepClone");
      _builder.newLine();
      _builder.append("package my.home.cloners");
      _builder.newLine();
      _builder.append("a.b.m2.Book BookCloner {");
      _builder.newLine();
      _builder.append("\t");
      _builder.append("name");
      _builder.newLine();
      _builder.append("\t");
      _builder.append("author");
      _builder.newLine();
      _builder.append("\t");
      _builder.append("sections {");
      _builder.newLine();
      _builder.append("\t\t");
      _builder.append("name");
      _builder.newLine();
      _builder.append("\t\t");
      _builder.append("description");
      _builder.newLine();
      _builder.append("\t\t");
      _builder.append("paragraphs");
      _builder.newLine();
      _builder.append("\t");
      _builder.append("}");
      _builder.newLine();
      _builder.append("}");
      _builder.newLine();
      final String sample = _builder.toString();
      StringConcatenation _builder_1 = new StringConcatenation();
      _builder_1.append("BookCloner : a.b.m2.Book {");
      _builder_1.newLine();
      _builder_1.append("\t");
      _builder_1.append("name : java.lang.String");
      _builder_1.newLine();
      _builder_1.append("\t");
      _builder_1.append("author : java.lang.String");
      _builder_1.newLine();
      _builder_1.append("\t");
      _builder_1.append("sections : a.b.m2.Section {");
      _builder_1.newLine();
      _builder_1.append("\t\t");
      _builder_1.append("name : java.lang.String");
      _builder_1.newLine();
      _builder_1.append("\t\t");
      _builder_1.append("description : java.lang.String");
      _builder_1.newLine();
      _builder_1.append("\t\t");
      _builder_1.append("paragraphs : a.b.m2.Paragraph");
      _builder_1.newLine();
      _builder_1.append("\t");
      _builder_1.append("}");
      _builder_1.newLine();
      _builder_1.append("}");
      _builder_1.newLine();
      final String expectedSampleWithInferredTypes = _builder_1.toString();
      final Model model = this.parser.parse(sample);
      this._deepCloneUtils.inferJavaTypes(((Body) model));
      final CharSequence result = this._deepCloneInspector.serialize(((Body) model));
      String _string = expectedSampleWithInferredTypes.toString();
      String _string_1 = result.toString();
      Assert.assertEquals(_string, _string_1);
    } catch (Throwable _e) {
      throw Exceptions.sneakyThrow(_e);
    }
  }
  
  @Test
  public void clonerReferringOneNonexistentField_suchMustBeInferredAsUnknownTypeAndNotNull() {
    try {
      StringConcatenation _builder = new StringConcatenation();
      _builder.append("deepClone");
      _builder.newLine();
      _builder.append("package my.home.cloners");
      _builder.newLine();
      _builder.append("a.b.m.Book BookCloner {");
      _builder.newLine();
      _builder.append("\t");
      _builder.append("name");
      _builder.newLine();
      _builder.append("\t");
      _builder.append("author");
      _builder.newLine();
      _builder.append("\t");
      _builder.append("fooFoo  // a.b.m.Book does not have any fooFoo field");
      _builder.newLine();
      _builder.append("}");
      _builder.newLine();
      final String sample = _builder.toString();
      StringConcatenation _builder_1 = new StringConcatenation();
      _builder_1.append("BookCloner : a.b.m.Book {");
      _builder_1.newLine();
      _builder_1.append("\t");
      _builder_1.append("name : java.lang.String");
      _builder_1.newLine();
      _builder_1.append("\t");
      _builder_1.append("author : java.lang.String");
      _builder_1.newLine();
      _builder_1.append("\t");
      _builder_1.append("fooFoo : [unknown]");
      _builder_1.newLine();
      _builder_1.append("}");
      _builder_1.newLine();
      final String expectedSampleWithInferredTypes = _builder_1.toString();
      final Model model = this.parser.parse(sample);
      this._deepCloneUtils.inferJavaTypes(((Body) model));
      final CharSequence result = this._deepCloneInspector.serialize(((Body) model));
      String _string = expectedSampleWithInferredTypes.toString();
      String _string_1 = result.toString();
      Assert.assertEquals(_string, _string_1);
    } catch (Throwable _e) {
      throw Exceptions.sneakyThrow(_e);
    }
  }
  
  @Test
  public void unknownTypeShouldBeReferredAsUnknowTypeInludingNestedStructures() {
    try {
      StringConcatenation _builder = new StringConcatenation();
      _builder.append("deepClone");
      _builder.newLine();
      _builder.append("package my.home.cloners");
      _builder.newLine();
      _builder.append("a.b.m.Book BookCloner {");
      _builder.newLine();
      _builder.append("\t");
      _builder.append("fooFooX1");
      _builder.newLine();
      _builder.append("\t");
      _builder.append("name");
      _builder.newLine();
      _builder.append("\t");
      _builder.append("author");
      _builder.newLine();
      _builder.append("\t");
      _builder.append("fooFooY1 {  // a.b.m.Book does not have any fooFoo field");
      _builder.newLine();
      _builder.append("\t   ");
      _builder.append("fooFooY11");
      _builder.newLine();
      _builder.append("\t   ");
      _builder.append("fooFooY12 {");
      _builder.newLine();
      _builder.append("\t   \t  ");
      _builder.append("fooFooY121");
      _builder.newLine();
      _builder.append("\t   ");
      _builder.append("}");
      _builder.newLine();
      _builder.append("\t");
      _builder.append("}");
      _builder.newLine();
      _builder.append("\t");
      _builder.append("fooFooZ1");
      _builder.newLine();
      _builder.append("}");
      _builder.newLine();
      final String sample = _builder.toString();
      StringConcatenation _builder_1 = new StringConcatenation();
      _builder_1.append("BookCloner : a.b.m.Book {");
      _builder_1.newLine();
      _builder_1.append("\t");
      _builder_1.append("fooFooX1 : [unknown]");
      _builder_1.newLine();
      _builder_1.append("\t");
      _builder_1.append("name : java.lang.String");
      _builder_1.newLine();
      _builder_1.append("\t");
      _builder_1.append("author : java.lang.String");
      _builder_1.newLine();
      _builder_1.append("\t");
      _builder_1.append("fooFooY1 : [unknown] {");
      _builder_1.newLine();
      _builder_1.append("\t\t");
      _builder_1.append("fooFooY11 : [unknown]");
      _builder_1.newLine();
      _builder_1.append("\t\t");
      _builder_1.append("fooFooY12 : [unknown] {");
      _builder_1.newLine();
      _builder_1.append("\t\t\t");
      _builder_1.append("fooFooY121 : [unknown]");
      _builder_1.newLine();
      _builder_1.append("\t\t");
      _builder_1.append("}");
      _builder_1.newLine();
      _builder_1.append("\t");
      _builder_1.append("}");
      _builder_1.newLine();
      _builder_1.append("\t");
      _builder_1.append("fooFooZ1 : [unknown]");
      _builder_1.newLine();
      _builder_1.append("}");
      _builder_1.newLine();
      final String expectedSampleWithInferredTypes = _builder_1.toString();
      final Model model = this.parser.parse(sample);
      this._deepCloneUtils.inferJavaTypes(((Body) model));
      final CharSequence result = this._deepCloneInspector.serialize(((Body) model));
      String _string = expectedSampleWithInferredTypes.toString();
      String _string_1 = result.toString();
      Assert.assertEquals(_string, _string_1);
    } catch (Throwable _e) {
      throw Exceptions.sneakyThrow(_e);
    }
  }
  
  @Test
  public void rootClonerReferringNoneexistingClassShouldBeOfUnknownTypeIncludingAllItsNestedTypes() {
    try {
      StringConcatenation _builder = new StringConcatenation();
      _builder.append("deepClone");
      _builder.newLine();
      _builder.append("package my.home.cloners");
      _builder.newLine();
      _builder.append("a.b.m.Bookkk BookCloner {  // deliberate mispelling of a.b.m.Book");
      _builder.newLine();
      _builder.append("\t");
      _builder.append("name");
      _builder.newLine();
      _builder.append("\t");
      _builder.append("author");
      _builder.newLine();
      _builder.append("\t");
      _builder.append("section {");
      _builder.newLine();
      _builder.append("\t   \t");
      _builder.append("name");
      _builder.newLine();
      _builder.append("\t");
      _builder.append("description");
      _builder.newLine();
      _builder.append("\t");
      _builder.append("paragraph");
      _builder.newLine();
      _builder.append("\t");
      _builder.append("}");
      _builder.newLine();
      _builder.append("}");
      _builder.newLine();
      final String sample = _builder.toString();
      StringConcatenation _builder_1 = new StringConcatenation();
      _builder_1.append("BookCloner : [unknown] {");
      _builder_1.newLine();
      _builder_1.append("\t");
      _builder_1.append("name : [unknown]");
      _builder_1.newLine();
      _builder_1.append("\t");
      _builder_1.append("author : [unknown]");
      _builder_1.newLine();
      _builder_1.append("\t");
      _builder_1.append("section : [unknown] {");
      _builder_1.newLine();
      _builder_1.append("\t\t");
      _builder_1.append("name : [unknown]");
      _builder_1.newLine();
      _builder_1.append("\t\t");
      _builder_1.append("description : [unknown]");
      _builder_1.newLine();
      _builder_1.append("\t\t");
      _builder_1.append("paragraph : [unknown]");
      _builder_1.newLine();
      _builder_1.append("\t");
      _builder_1.append("}");
      _builder_1.newLine();
      _builder_1.append("}");
      _builder_1.newLine();
      final String expectedSampleWithInferredTypes = _builder_1.toString();
      final Model model = this.parser.parse(sample);
      this._deepCloneUtils.inferJavaTypes(((Body) model));
      final CharSequence result = this._deepCloneInspector.serialize(((Body) model));
      String _string = expectedSampleWithInferredTypes.toString();
      String _string_1 = result.toString();
      Assert.assertEquals(_string, _string_1);
    } catch (Throwable _e) {
      throw Exceptions.sneakyThrow(_e);
    }
  }
}
