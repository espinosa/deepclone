package my.home.dsl.deepclone.test.utils;

import com.google.inject.Inject;
import my.home.dsl.deepClone.Body;
import my.home.dsl.deepClone.ClassCloner;
import my.home.dsl.deepClone.ComplexField;
import my.home.dsl.deepClone.FieldClonerType;
import my.home.dsl.deepClone.SimpleField;
import my.home.dsl.utils.ReflectionUtils;
import org.eclipse.emf.common.util.EList;
import org.eclipse.xtend2.lib.StringConcatenation;
import org.eclipse.xtext.common.types.JvmTypeReference;
import org.eclipse.xtext.xbase.lib.Extension;

/**
 * Renders whole DeepClone semantic model to a string representation revealing inferred Java
 * types of individual fields.<br>
 * Example:
 * <pre>
 * 	BookCloner : a.b.m.Book {
 * 		name : java.lang.String
 * 		author : java.lang.String
 * 		section : a.b.m.Section {
 * 			name : java.lang.String
 * 			description : java.lang.String
 * 			paragraph : a.b.m.Paragraph
 * 			fooFoo: [unknown]
 * 		}
 * 	}
 * </pre>
 */
@SuppressWarnings("all")
public class DeepCloneInspector {
  @Inject
  @Extension
  private ReflectionUtils _reflectionUtils;
  
  public CharSequence serialize(final Body body) {
    StringConcatenation _builder = new StringConcatenation();
    {
      EList<ClassCloner> _cloners = body.getCloners();
      for(final ClassCloner cloner : _cloners) {
        CharSequence _serializeCC = this.serializeCC(cloner);
        _builder.append(_serializeCC, "");
        _builder.newLineIfNotEmpty();
      }
    }
    return _builder;
  }
  
  public CharSequence serializeCC(final ClassCloner cloner) {
    StringConcatenation _builder = new StringConcatenation();
    String _name = cloner.getName();
    _builder.append(_name, "");
    _builder.append(" : ");
    JvmTypeReference _javaType = cloner.getJavaType();
    JvmTypeReference _typeOrCollectionTypeParameter = this._reflectionUtils.getTypeOrCollectionTypeParameter(_javaType);
    String _qualifiedName = _typeOrCollectionTypeParameter.getQualifiedName();
    _builder.append(_qualifiedName, "");
    _builder.append(" {");
    _builder.newLineIfNotEmpty();
    {
      EList<FieldClonerType> _fields = cloner.getFields();
      for(final FieldClonerType field : _fields) {
        {
          if ((field instanceof SimpleField)) {
            _builder.append("\t");
            String _fieldName = ((SimpleField)field).getFieldName();
            _builder.append(_fieldName, "\t");
            _builder.append(" : ");
            JvmTypeReference _javaType_1 = ((SimpleField)field).getJavaType();
            JvmTypeReference _typeOrCollectionTypeParameter_1 = this._reflectionUtils.getTypeOrCollectionTypeParameter(_javaType_1);
            String _qualifiedName_1 = _typeOrCollectionTypeParameter_1.getQualifiedName();
            _builder.append(_qualifiedName_1, "\t");
            _builder.newLineIfNotEmpty();
          } else {
            if ((field instanceof ComplexField)) {
              _builder.append("\t");
              String _fieldName_1 = field.getFieldName();
              _builder.append(_fieldName_1, "\t");
              _builder.append(" : ");
              JvmTypeReference _javaType_2 = field.getJavaType();
              JvmTypeReference _typeOrCollectionTypeParameter_2 = this._reflectionUtils.getTypeOrCollectionTypeParameter(_javaType_2);
              String _qualifiedName_2 = _typeOrCollectionTypeParameter_2.getQualifiedName();
              _builder.append(_qualifiedName_2, "\t");
              _builder.append(" {");
              _builder.newLineIfNotEmpty();
              _builder.append("\t");
              _builder.append("\t");
              ComplexField _asComplex = this.asComplex(field);
              CharSequence _serializeCF = this.serializeCF(_asComplex);
              _builder.append(_serializeCF, "\t\t");
              _builder.newLineIfNotEmpty();
              _builder.append("\t");
              _builder.append("}");
              _builder.newLine();
            }
          }
        }
      }
    }
    _builder.append("}");
    _builder.newLine();
    return _builder;
  }
  
  protected CharSequence serializeCF(final ComplexField parentField) {
    StringConcatenation _builder = new StringConcatenation();
    {
      EList<FieldClonerType> _fields = parentField.getFields();
      for(final FieldClonerType field : _fields) {
        {
          if ((field instanceof SimpleField)) {
            String _fieldName = ((SimpleField)field).getFieldName();
            _builder.append(_fieldName, "");
            _builder.append(" : ");
            JvmTypeReference _javaType = ((SimpleField)field).getJavaType();
            JvmTypeReference _typeOrCollectionTypeParameter = this._reflectionUtils.getTypeOrCollectionTypeParameter(_javaType);
            String _qualifiedName = _typeOrCollectionTypeParameter.getQualifiedName();
            _builder.append(_qualifiedName, "");
            _builder.newLineIfNotEmpty();
          } else {
            if ((field instanceof ComplexField)) {
              String _fieldName_1 = field.getFieldName();
              _builder.append(_fieldName_1, "");
              _builder.append(" : ");
              JvmTypeReference _javaType_1 = field.getJavaType();
              JvmTypeReference _typeOrCollectionTypeParameter_1 = this._reflectionUtils.getTypeOrCollectionTypeParameter(_javaType_1);
              String _qualifiedName_1 = _typeOrCollectionTypeParameter_1.getQualifiedName();
              _builder.append(_qualifiedName_1, "");
              _builder.append(" {");
              _builder.newLineIfNotEmpty();
              _builder.append("\t");
              ComplexField _asComplex = this.asComplex(field);
              CharSequence _serializeCF = this.serializeCF(_asComplex);
              _builder.append(_serializeCF, "\t");
              _builder.newLineIfNotEmpty();
              _builder.append("}");
              _builder.newLine();
            }
          }
        }
      }
    }
    return _builder;
  }
  
  protected ComplexField asComplex(final FieldClonerType field) {
    return ((ComplexField) field);
  }
}
