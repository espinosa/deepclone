package my.home.dsl.deepclone;

import com.google.inject.Inject;
import my.home.dsl.DeepCloneInjectorProvider;
import my.home.dsl.deepClone.Model;
import my.home.dsl.deepclone.test.utils.MyValidationHelper;
import org.eclipse.xtend2.lib.StringConcatenation;
import org.eclipse.xtext.junit4.InjectWith;
import org.eclipse.xtext.junit4.XtextRunner;
import org.eclipse.xtext.junit4.util.ParseHelper;
import org.eclipse.xtext.junit4.validation.ValidationTestHelper;
import org.eclipse.xtext.xbase.lib.Exceptions;
import org.junit.Assert;
import org.junit.Test;
import org.junit.runner.RunWith;

@InjectWith(DeepCloneInjectorProvider.class)
@RunWith(XtextRunner.class)
@SuppressWarnings("all")
public class DeepCloneValidationTest {
  @Inject
  private ParseHelper<Model> parser;
  
  @Inject
  private ValidationTestHelper validationHelper;
  
  @Inject
  private MyValidationHelper myValidationHelper;
  
  @Test
  public void validateCorrectModelWithInclusions() {
    try {
      StringConcatenation _builder = new StringConcatenation();
      _builder.append("deepClone");
      _builder.newLine();
      _builder.append("package a.b.m");
      _builder.newLine();
      _builder.append("a.b.m.Paragraph {");
      _builder.newLine();
      _builder.append("\t");
      _builder.append("number");
      _builder.newLine();
      _builder.append("\t");
      _builder.append("text");
      _builder.newLine();
      _builder.append("}");
      _builder.newLine();
      final String code = _builder.toString();
      final Model model = this.parser.parse(code);
      this.validationHelper.assertNoErrors(model);
    } catch (Throwable _e) {
      throw Exceptions.sneakyThrow(_e);
    }
  }
  
  @Test
  public void validateCorrectModelWithExclusions() {
    try {
      StringConcatenation _builder = new StringConcatenation();
      _builder.append("deepClone");
      _builder.newLine();
      _builder.append("package a.b.m");
      _builder.newLine();
      _builder.append("a.b.m.Paragraph {");
      _builder.newLine();
      _builder.append("\t");
      _builder.append("-number");
      _builder.newLine();
      _builder.append("\t");
      _builder.append("-text");
      _builder.newLine();
      _builder.append("}");
      _builder.newLine();
      final String code = _builder.toString();
      final Model model = this.parser.parse(code);
      this.validationHelper.assertNoErrors(model);
    } catch (Throwable _e) {
      throw Exceptions.sneakyThrow(_e);
    }
  }
  
  @Test
  public void validateCorrectModelWithInclusionsAndExclusions() {
    try {
      StringConcatenation _builder = new StringConcatenation();
      _builder.append("deepClone");
      _builder.newLine();
      _builder.append("package a.b.m");
      _builder.newLine();
      _builder.append("a.b.m.Paragraph {");
      _builder.newLine();
      _builder.append("\t");
      _builder.append("-number");
      _builder.newLine();
      _builder.append("\t");
      _builder.append("text");
      _builder.newLine();
      _builder.append("}");
      _builder.newLine();
      final String code = _builder.toString();
      final Model model = this.parser.parse(code);
      this.validationHelper.assertNoErrors(model);
    } catch (Throwable _e) {
      throw Exceptions.sneakyThrow(_e);
    }
  }
  
  @Test
  public void validateIncorrectModel_MissingAndSurplus() {
    try {
      StringConcatenation _builder = new StringConcatenation();
      _builder.append("deepClone");
      _builder.newLine();
      _builder.append("package a.b.m");
      _builder.newLine();
      _builder.append("a.b.m.Paragraph {");
      _builder.newLine();
      _builder.append("\t");
      _builder.append("text");
      _builder.newLine();
      _builder.append("\t");
      _builder.append("fooField");
      _builder.newLine();
      _builder.append("}");
      _builder.newLine();
      final String code = _builder.toString();
      final Model model = this.parser.parse(code);
      StringConcatenation _builder_1 = new StringConcatenation();
      _builder_1.append("The cloner for type a.b.m.Paragraph must exclude or include field number, line: 3, object: ClassCloner");
      _builder_1.newLine();
      _builder_1.append("The cloner for type a.b.m.Paragraph references unknown field fooField, line: 5, object: SimpleField");
      _builder_1.newLine();
      final String expectedValidationErrors = _builder_1.toString();
      MyValidationHelper.ValidationResult _validate = this.myValidationHelper.validate(model);
      final String actualValidationErrors = _validate.issuesToString();
      Assert.assertEquals(expectedValidationErrors, actualValidationErrors);
    } catch (Throwable _e) {
      throw Exceptions.sneakyThrow(_e);
    }
  }
  
  @Test
  public void validateIncorrectModel_MissingAndSurplus_namedCloner() {
    try {
      StringConcatenation _builder = new StringConcatenation();
      _builder.append("deepClone");
      _builder.newLine();
      _builder.append("package a.b.m");
      _builder.newLine();
      _builder.append("a.b.m.Paragraph ShallowCloner {");
      _builder.newLine();
      _builder.append("\t");
      _builder.append("text");
      _builder.newLine();
      _builder.append("\t");
      _builder.append("fooField");
      _builder.newLine();
      _builder.append("}");
      _builder.newLine();
      final String code = _builder.toString();
      final Model model = this.parser.parse(code);
      StringConcatenation _builder_1 = new StringConcatenation();
      _builder_1.append("The cloner for type a.b.m.Paragraph must exclude or include field number, line: 3, object: ClassCloner");
      _builder_1.newLine();
      _builder_1.append("The cloner for type a.b.m.Paragraph references unknown field fooField, line: 5, object: SimpleField");
      _builder_1.newLine();
      final String expectedValidationErrors = _builder_1.toString();
      MyValidationHelper.ValidationResult _validate = this.myValidationHelper.validate(model);
      final String actualValidationErrors = _validate.issuesToString();
      Assert.assertEquals(expectedValidationErrors, actualValidationErrors);
    } catch (Throwable _e) {
      throw Exceptions.sneakyThrow(_e);
    }
  }
  
  @Test
  public void validateIncorrectDeepModel_MissingAndSurplus() {
    try {
      StringConcatenation _builder = new StringConcatenation();
      _builder.append("deepClone");
      _builder.newLine();
      _builder.newLine();
      _builder.append("package my.home.cloners");
      _builder.newLine();
      _builder.newLine();
      _builder.append("a.b.m.Magazine {");
      _builder.newLine();
      _builder.append("\t");
      _builder.append("-isbn");
      _builder.newLine();
      _builder.append("\t");
      _builder.append("article {");
      _builder.newLine();
      _builder.append("\t\t");
      _builder.append("name");
      _builder.newLine();
      _builder.append("\t\t");
      _builder.append("-author");
      _builder.newLine();
      _builder.append("\t\t");
      _builder.append("-pageNumber");
      _builder.newLine();
      _builder.append("\t\t");
      _builder.append("section {");
      _builder.newLine();
      _builder.append("\t\t\t");
      _builder.append("-name");
      _builder.newLine();
      _builder.append("\t\t\t");
      _builder.append("-paragraph");
      _builder.newLine();
      _builder.append("\t\t");
      _builder.append("}");
      _builder.newLine();
      _builder.append("\t");
      _builder.append("}");
      _builder.newLine();
      _builder.append("}");
      _builder.newLine();
      final String code = _builder.toString();
      final Model model = this.parser.parse(code);
      StringConcatenation _builder_1 = new StringConcatenation();
      _builder_1.append("The cloner for type a.b.m.Magazine must exclude or include field name, line: 5, object: ClassCloner");
      _builder_1.newLine();
      _builder_1.append("The cloner for type a.b.m.Section must exclude or include field description, line: 11, object: ComplexField");
      _builder_1.newLine();
      final String expectedValidationErrors = _builder_1.toString();
      MyValidationHelper.ValidationResult _validate = this.myValidationHelper.validate(model);
      final String actualValidationErrors = _validate.issuesToString();
      Assert.assertEquals(expectedValidationErrors, actualValidationErrors);
    } catch (Throwable _e) {
      throw Exceptions.sneakyThrow(_e);
    }
  }
  
  @Test
  public void validateIncorrectDeepModelWithComments() {
    try {
      StringConcatenation _builder = new StringConcatenation();
      _builder.append("deepClone");
      _builder.newLine();
      _builder.newLine();
      _builder.append("package my.home.cloners");
      _builder.newLine();
      _builder.newLine();
      _builder.append("/** javadoc style comment */");
      _builder.newLine();
      _builder.append("//some cooment");
      _builder.newLine();
      _builder.append("a.b.m.Magazine {");
      _builder.newLine();
      _builder.append("\t");
      _builder.append("-isbn");
      _builder.newLine();
      _builder.append("\t");
      _builder.append("// declaration");
      _builder.newLine();
      _builder.append("\t");
      _builder.append("article {");
      _builder.newLine();
      _builder.append("\t\t");
      _builder.append("name");
      _builder.newLine();
      _builder.append("\t\t");
      _builder.append("-author");
      _builder.newLine();
      _builder.append("\t\t");
      _builder.append("-pageNumber");
      _builder.newLine();
      _builder.append("\t\t");
      _builder.append("section {");
      _builder.newLine();
      _builder.append("\t\t\t");
      _builder.append("-name");
      _builder.newLine();
      _builder.append("\t\t\t");
      _builder.append("/*");
      _builder.newLine();
      _builder.append("\t\t\t  ");
      _builder.append("description");
      _builder.newLine();
      _builder.append("\t\t\t ");
      _builder.append("*/");
      _builder.newLine();
      _builder.append("\t\t\t");
      _builder.append("-paragraph");
      _builder.newLine();
      _builder.append("\t\t");
      _builder.append("}");
      _builder.newLine();
      _builder.append("\t");
      _builder.append("}");
      _builder.newLine();
      _builder.append("}");
      _builder.newLine();
      final String code = _builder.toString();
      final Model model = this.parser.parse(code);
      StringConcatenation _builder_1 = new StringConcatenation();
      _builder_1.append("The cloner for type a.b.m.Magazine must exclude or include field name, line: 7, object: ClassCloner");
      _builder_1.newLine();
      _builder_1.append("The cloner for type a.b.m.Section must exclude or include field description, line: 14, object: ComplexField");
      _builder_1.newLine();
      final String expectedValidationErrors = _builder_1.toString();
      MyValidationHelper.ValidationResult _validate = this.myValidationHelper.validate(model);
      final String actualValidationErrors = _validate.issuesToString();
      Assert.assertEquals(expectedValidationErrors, actualValidationErrors);
    } catch (Throwable _e) {
      throw Exceptions.sneakyThrow(_e);
    }
  }
  
  @Test
  public void validateCorrectDeepModelWithComments() {
    try {
      StringConcatenation _builder = new StringConcatenation();
      _builder.append("deepClone");
      _builder.newLine();
      _builder.newLine();
      _builder.append("package my.home.cloners");
      _builder.newLine();
      _builder.newLine();
      _builder.append("/** javadoc style comment */");
      _builder.newLine();
      _builder.append("//some cooment");
      _builder.newLine();
      _builder.append("a.b.m.Magazine {");
      _builder.newLine();
      _builder.append("\t");
      _builder.append("-isbn");
      _builder.newLine();
      _builder.append("\t");
      _builder.append("name  // end of line comment");
      _builder.newLine();
      _builder.append("\t");
      _builder.append("article {");
      _builder.newLine();
      _builder.append("\t\t");
      _builder.append("name");
      _builder.newLine();
      _builder.append("\t\t");
      _builder.append("-author");
      _builder.newLine();
      _builder.append("\t\t");
      _builder.append("-pageNumber");
      _builder.newLine();
      _builder.append("\t\t");
      _builder.append("section {");
      _builder.newLine();
      _builder.append("\t\t\t");
      _builder.append("name ");
      _builder.newLine();
      _builder.append("\t\t\t");
      _builder.append("description ");
      _builder.newLine();
      _builder.append("\t\t\t");
      _builder.append("-paragraph");
      _builder.newLine();
      _builder.append("\t\t");
      _builder.append("}");
      _builder.newLine();
      _builder.append("\t");
      _builder.append("}");
      _builder.newLine();
      _builder.append("}");
      _builder.newLine();
      final String code = _builder.toString();
      final Model model = this.parser.parse(code);
      final String expectedValidationErrors = "";
      MyValidationHelper.ValidationResult _validate = this.myValidationHelper.validate(model);
      final String actualValidationErrors = _validate.issuesToString();
      Assert.assertEquals(expectedValidationErrors, actualValidationErrors);
    } catch (Throwable _e) {
      throw Exceptions.sneakyThrow(_e);
    }
  }
}
