package my.home.dsl.deepclone;

import com.google.common.base.Objects;
import com.google.inject.Inject;
import java.util.List;
import java.util.Set;
import my.home.dsl.DeepCloneInjectorProvider;
import my.home.dsl.deepClone.Body;
import my.home.dsl.deepClone.ClassCloner;
import my.home.dsl.deepClone.ContainerType;
import my.home.dsl.deepClone.FieldClonerType;
import my.home.dsl.deepClone.Model;
import my.home.dsl.utils.DeepCloneUtils;
import org.eclipse.emf.common.util.EList;
import org.eclipse.xtend2.lib.StringConcatenation;
import org.eclipse.xtext.junit4.InjectWith;
import org.eclipse.xtext.junit4.XtextRunner;
import org.eclipse.xtext.junit4.util.ParseHelper;
import org.eclipse.xtext.xbase.lib.Conversions;
import org.eclipse.xtext.xbase.lib.Exceptions;
import org.eclipse.xtext.xbase.lib.Extension;
import org.eclipse.xtext.xbase.lib.Functions.Function1;
import org.eclipse.xtext.xbase.lib.IterableExtensions;
import org.junit.Assert;
import org.junit.Test;
import org.junit.runner.RunWith;

@InjectWith(DeepCloneInjectorProvider.class)
@RunWith(XtextRunner.class)
@SuppressWarnings("all")
public class DeepCloneUtilFindMissingFieldsTest {
  @Inject
  private ParseHelper<Model> parser;
  
  @Inject
  @Extension
  private DeepCloneUtils _deepCloneUtils;
  
  @Test
  public void detectMissingTwoSimpleFieldsFromRootCloner() {
    try {
      StringConcatenation _builder = new StringConcatenation();
      _builder.append("deepClone");
      _builder.newLine();
      _builder.append("package my.home.cloners");
      _builder.newLine();
      _builder.append("a.b.m.Book BookCloner {");
      _builder.newLine();
      _builder.append("\t");
      _builder.append("//name");
      _builder.newLine();
      _builder.append("\t");
      _builder.append("//author");
      _builder.newLine();
      _builder.append("\t");
      _builder.append("section {");
      _builder.newLine();
      _builder.append("\t\t");
      _builder.append("name");
      _builder.newLine();
      _builder.append("\t\t");
      _builder.append("description");
      _builder.newLine();
      _builder.append("\t");
      _builder.append("}");
      _builder.newLine();
      _builder.append("}");
      _builder.newLine();
      final String sample = _builder.toString();
      StringConcatenation _builder_1 = new StringConcatenation();
      _builder_1.append("[author, name]");
      final String expectedMissingFieldNamesForBookCloner = _builder_1.toString();
      final Model model = this.parser.parse(sample);
      EList<ClassCloner> _cloners = ((Body) model).getCloners();
      ClassCloner _get = _cloners.get(0);
      final ContainerType cloner = this._deepCloneUtils.asContainer(_get);
      final Set<String> missingFieldNamesSet = this._deepCloneUtils.findMissingFieldsInContainerElement(cloner);
      String _string = expectedMissingFieldNamesForBookCloner.toString();
      List<String> _sort = IterableExtensions.<String>sort(missingFieldNamesSet);
      Object[] _array = _sort.toArray();
      String _string_1 = ((List<Object>)Conversions.doWrapArray(_array)).toString();
      Assert.assertEquals(_string, _string_1);
    } catch (Throwable _e) {
      throw Exceptions.sneakyThrow(_e);
    }
  }
  
  @Test
  public void detectMissingComplexFieldSectionFromRootCloner() {
    try {
      StringConcatenation _builder = new StringConcatenation();
      _builder.append("deepClone");
      _builder.newLine();
      _builder.append("package my.home.cloners");
      _builder.newLine();
      _builder.append("a.b.m.Book BookCloner {");
      _builder.newLine();
      _builder.append("\t");
      _builder.append("name");
      _builder.newLine();
      _builder.append("\t");
      _builder.append("author");
      _builder.newLine();
      _builder.append("\t");
      _builder.append("//section");
      _builder.newLine();
      _builder.append("}");
      _builder.newLine();
      final String sample = _builder.toString();
      StringConcatenation _builder_1 = new StringConcatenation();
      _builder_1.append("[section]");
      final String expectedMissingFieldNamesForBookCloner = _builder_1.toString();
      final Model model = this.parser.parse(sample);
      EList<ClassCloner> _cloners = ((Body) model).getCloners();
      ClassCloner _get = _cloners.get(0);
      final ContainerType cloner = this._deepCloneUtils.asContainer(_get);
      final Set<String> missingFieldNamesSet = this._deepCloneUtils.findMissingFieldsInContainerElement(cloner);
      String _string = expectedMissingFieldNamesForBookCloner.toString();
      List<String> _sort = IterableExtensions.<String>sort(missingFieldNamesSet);
      Object[] _array = _sort.toArray();
      String _string_1 = ((List<Object>)Conversions.doWrapArray(_array)).toString();
      Assert.assertEquals(_string, _string_1);
    } catch (Throwable _e) {
      throw Exceptions.sneakyThrow(_e);
    }
  }
  
  @Test
  public void detectMissingTwoSimpleFieldsFromnestedElementSection() {
    try {
      StringConcatenation _builder = new StringConcatenation();
      _builder.append("deepClone");
      _builder.newLine();
      _builder.append("package my.home.cloners");
      _builder.newLine();
      _builder.append("a.b.m.Book BookCloner {");
      _builder.newLine();
      _builder.append("\t");
      _builder.append("name");
      _builder.newLine();
      _builder.append("\t");
      _builder.append("author");
      _builder.newLine();
      _builder.append("\t");
      _builder.append("section {");
      _builder.newLine();
      _builder.append("\t\t");
      _builder.append("//name");
      _builder.newLine();
      _builder.append("\t\t");
      _builder.append("//description");
      _builder.newLine();
      _builder.append("\t\t");
      _builder.append("//paragraph");
      _builder.newLine();
      _builder.append("\t");
      _builder.append("}");
      _builder.newLine();
      _builder.append("}");
      _builder.newLine();
      final String sample = _builder.toString();
      StringConcatenation _builder_1 = new StringConcatenation();
      _builder_1.append("[description, name, paragraph]");
      final String expectedMissingFieldNamesForSectionElement = _builder_1.toString();
      final Model model = this.parser.parse(sample);
      EList<ClassCloner> _cloners = ((Body) model).getCloners();
      ClassCloner _get = _cloners.get(0);
      final ContainerType cloner = this._deepCloneUtils.asContainer(_get);
      EList<FieldClonerType> _fields = cloner.getFields();
      final Function1<FieldClonerType,Boolean> _function = new Function1<FieldClonerType,Boolean>() {
        public Boolean apply(final FieldClonerType it) {
          String _fieldName = it.getFieldName();
          boolean _equals = Objects.equal(_fieldName, "section");
          return Boolean.valueOf(_equals);
        }
      };
      FieldClonerType _findFirst = IterableExtensions.<FieldClonerType>findFirst(_fields, _function);
      final ContainerType section = this._deepCloneUtils.asContainer(_findFirst);
      final Set<String> missingFieldNamesInSection = this._deepCloneUtils.findMissingFieldsInContainerElement(section);
      String _string = expectedMissingFieldNamesForSectionElement.toString();
      List<String> _sort = IterableExtensions.<String>sort(missingFieldNamesInSection);
      Object[] _array = _sort.toArray();
      String _string_1 = ((List<Object>)Conversions.doWrapArray(_array)).toString();
      Assert.assertEquals(_string, _string_1);
    } catch (Throwable _e) {
      throw Exceptions.sneakyThrow(_e);
    }
  }
  
  @Test
  public void attemptToGetMissingFieldsForNonExistingClassFieldMustBeHandledGratiously() {
    try {
      StringConcatenation _builder = new StringConcatenation();
      _builder.append("deepClone");
      _builder.newLine();
      _builder.append("package my.home.cloners");
      _builder.newLine();
      _builder.append("a.b.m.Book BookCloner {");
      _builder.newLine();
      _builder.append("\t");
      _builder.append("name");
      _builder.newLine();
      _builder.append("\t");
      _builder.append("author");
      _builder.newLine();
      _builder.append("\t");
      _builder.append("fooFoo {  // a.b.m.Book does not have any fooFoo field");
      _builder.newLine();
      _builder.append("\t");
      _builder.append("}");
      _builder.newLine();
      _builder.append("}");
      _builder.newLine();
      final String sample = _builder.toString();
      StringConcatenation _builder_1 = new StringConcatenation();
      _builder_1.append("[]");
      final String expectedMissingFieldNamesForFooFooElement = _builder_1.toString();
      final Model model = this.parser.parse(sample);
      EList<ClassCloner> _cloners = ((Body) model).getCloners();
      ClassCloner _get = _cloners.get(0);
      final ContainerType cloner = this._deepCloneUtils.asContainer(_get);
      EList<FieldClonerType> _fields = cloner.getFields();
      final Function1<FieldClonerType,Boolean> _function = new Function1<FieldClonerType,Boolean>() {
        public Boolean apply(final FieldClonerType it) {
          String _fieldName = it.getFieldName();
          boolean _equals = Objects.equal(_fieldName, "fooFoo");
          return Boolean.valueOf(_equals);
        }
      };
      FieldClonerType _findFirst = IterableExtensions.<FieldClonerType>findFirst(_fields, _function);
      final ContainerType fooFoo = this._deepCloneUtils.asContainer(_findFirst);
      final Set<String> missingFieldNamesInSection = this._deepCloneUtils.findMissingFieldsInContainerElement(fooFoo);
      String _string = expectedMissingFieldNamesForFooFooElement.toString();
      List<String> _sort = IterableExtensions.<String>sort(missingFieldNamesInSection);
      Object[] _array = _sort.toArray();
      String _string_1 = ((List<Object>)Conversions.doWrapArray(_array)).toString();
      Assert.assertEquals(_string, _string_1);
    } catch (Throwable _e) {
      throw Exceptions.sneakyThrow(_e);
    }
  }
}
