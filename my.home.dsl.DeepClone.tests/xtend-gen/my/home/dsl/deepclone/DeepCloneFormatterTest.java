package my.home.dsl.deepclone;

import com.google.inject.Inject;
import my.home.dsl.DeepCloneInjectorProvider;
import my.home.dsl.deepClone.Model;
import org.eclipse.xtend2.lib.StringConcatenation;
import org.eclipse.xtext.junit4.InjectWith;
import org.eclipse.xtext.junit4.XtextRunner;
import org.eclipse.xtext.junit4.util.ParseHelper;
import org.eclipse.xtext.junit4.validation.ValidationTestHelper;
import org.eclipse.xtext.resource.SaveOptions;
import org.eclipse.xtext.serializer.ISerializer;
import org.eclipse.xtext.xbase.lib.Exceptions;
import org.eclipse.xtext.xbase.lib.Extension;
import org.junit.Assert;
import org.junit.Test;
import org.junit.runner.RunWith;

@InjectWith(DeepCloneInjectorProvider.class)
@RunWith(XtextRunner.class)
@SuppressWarnings("all")
public class DeepCloneFormatterTest {
  @Inject
  private ParseHelper<Model> parser;
  
  @Inject
  private ValidationTestHelper validationHelper;
  
  @Inject
  @Extension
  private ISerializer _iSerializer;
  
  @Test
  public void formatTrivialExample() {
    try {
      StringConcatenation _builder = new StringConcatenation();
      _builder.append("deepClone");
      _builder.newLine();
      _builder.append("package a.b.m");
      _builder.newLine();
      _builder.append("a.b.m.Paragraph {");
      _builder.newLine();
      _builder.append("number");
      _builder.newLine();
      _builder.append("text");
      _builder.newLine();
      _builder.append("}");
      _builder.newLine();
      final String sample = _builder.toString();
      StringConcatenation _builder_1 = new StringConcatenation();
      _builder_1.append("deepClone");
      _builder_1.newLine();
      _builder_1.append("package a.b.m");
      _builder_1.newLine();
      _builder_1.append("a.b.m.Paragraph {");
      _builder_1.newLine();
      _builder_1.append("\t");
      _builder_1.append("number");
      _builder_1.newLine();
      _builder_1.append("\t");
      _builder_1.append("text");
      _builder_1.newLine();
      _builder_1.append("}");
      final String expectedFormattedSample = _builder_1.toString();
      final Model model = this.parser.parse(sample);
      this.validationHelper.assertNoErrors(model);
      SaveOptions.Builder _newBuilder = SaveOptions.newBuilder();
      SaveOptions.Builder _format = _newBuilder.format();
      SaveOptions _options = _format.getOptions();
      final String resultFormattedCode = this._iSerializer.serialize(model, _options);
      Assert.assertEquals(expectedFormattedSample, resultFormattedCode);
    } catch (Throwable _e) {
      throw Exceptions.sneakyThrow(_e);
    }
  }
  
  @Test
  public void formatMoreComplexSampleWithNestedStructuresAndComments() {
    try {
      StringConcatenation _builder = new StringConcatenation();
      _builder.append("deepClone");
      _builder.newLine();
      _builder.newLine();
      _builder.append("package my.home.cloners");
      _builder.newLine();
      _builder.newLine();
      _builder.append("/** javadoc style comment */");
      _builder.newLine();
      _builder.append("//some cooment");
      _builder.newLine();
      _builder.append("a.b.m.Magazine {");
      _builder.newLine();
      _builder.append("-isbn");
      _builder.newLine();
      _builder.append("name  // end of   line comment");
      _builder.newLine();
      _builder.append("article {");
      _builder.newLine();
      _builder.append("name");
      _builder.newLine();
      _builder.append("-author");
      _builder.newLine();
      _builder.append("-pageNumber");
      _builder.newLine();
      _builder.append("section {");
      _builder.newLine();
      _builder.append("name ");
      _builder.newLine();
      _builder.append("description ");
      _builder.newLine();
      _builder.append("-paragraph");
      _builder.newLine();
      _builder.append("}");
      _builder.newLine();
      _builder.append("}");
      _builder.newLine();
      _builder.append("}");
      _builder.newLine();
      final String sample = _builder.toString();
      StringConcatenation _builder_1 = new StringConcatenation();
      _builder_1.append("deepClone");
      _builder_1.newLine();
      _builder_1.append("package my.home.cloners");
      _builder_1.newLine();
      _builder_1.newLine();
      _builder_1.append("/** javadoc style comment */");
      _builder_1.newLine();
      _builder_1.append("//some cooment");
      _builder_1.newLine();
      _builder_1.append("a.b.m.Magazine {");
      _builder_1.newLine();
      _builder_1.append("\t");
      _builder_1.append("-isbn");
      _builder_1.newLine();
      _builder_1.append("\t");
      _builder_1.append("name // end of   line comment");
      _builder_1.newLine();
      _builder_1.append("\t");
      _builder_1.append("article {");
      _builder_1.newLine();
      _builder_1.append("\t\t");
      _builder_1.append("name");
      _builder_1.newLine();
      _builder_1.append("\t\t");
      _builder_1.append("-author");
      _builder_1.newLine();
      _builder_1.append("\t\t");
      _builder_1.append("-pageNumber");
      _builder_1.newLine();
      _builder_1.append("\t\t");
      _builder_1.append("section {");
      _builder_1.newLine();
      _builder_1.append("\t\t\t");
      _builder_1.append("name");
      _builder_1.newLine();
      _builder_1.append("\t\t\t");
      _builder_1.append("description");
      _builder_1.newLine();
      _builder_1.append("\t\t\t");
      _builder_1.append("-paragraph");
      _builder_1.newLine();
      _builder_1.append("\t\t");
      _builder_1.append("}");
      _builder_1.newLine();
      _builder_1.append("\t");
      _builder_1.append("}");
      _builder_1.newLine();
      _builder_1.append("}");
      final String expectedFormattedSample = _builder_1.toString();
      final Model model = this.parser.parse(sample);
      this.validationHelper.assertNoErrors(model);
      SaveOptions.Builder _newBuilder = SaveOptions.newBuilder();
      SaveOptions.Builder _format = _newBuilder.format();
      SaveOptions _options = _format.getOptions();
      final String resultFormattedCode = this._iSerializer.serialize(model, _options);
      Assert.assertEquals(expectedFormattedSample, resultFormattedCode);
    } catch (Throwable _e) {
      throw Exceptions.sneakyThrow(_e);
    }
  }
  
  @Test
  public void serializeSampleWithoutFormattingOption_noChangeIsExpected() {
    try {
      StringConcatenation _builder = new StringConcatenation();
      _builder.append("deepClone");
      _builder.newLine();
      _builder.append("package a.b.m    ");
      _builder.newLine();
      _builder.append("a.b.m.Paragraph {");
      _builder.newLine();
      _builder.append("\t  ");
      _builder.append("number");
      _builder.newLine();
      _builder.append("\t");
      _builder.append("text    ");
      _builder.newLine();
      _builder.append("}");
      _builder.newLine();
      final String sample = _builder.toString();
      StringConcatenation _builder_1 = new StringConcatenation();
      _builder_1.append("deepClone");
      _builder_1.newLine();
      _builder_1.append("package a.b.m    ");
      _builder_1.newLine();
      _builder_1.append("a.b.m.Paragraph {");
      _builder_1.newLine();
      _builder_1.append("\t  ");
      _builder_1.append("number");
      _builder_1.newLine();
      _builder_1.append("\t");
      _builder_1.append("text    ");
      _builder_1.newLine();
      _builder_1.append("}");
      final String expectedResut = _builder_1.toString();
      final Model model = this.parser.parse(sample);
      this.validationHelper.assertNoErrors(model);
      final String serializedModel = this._iSerializer.serialize(model);
      Assert.assertEquals(expectedResut, serializedModel);
    } catch (Throwable _e) {
      throw Exceptions.sneakyThrow(_e);
    }
  }
  
  @Test
  public void formatAlreadyFormattedSampleExpectTrailingWhitespacesToBeStripped() {
    try {
      StringConcatenation _builder = new StringConcatenation();
      _builder.append("deepClone");
      _builder.newLine();
      _builder.append("package a.b.m    ");
      _builder.newLine();
      _builder.append("a.b.m.Paragraph {");
      _builder.newLine();
      _builder.append("\t");
      _builder.append("number");
      _builder.newLine();
      _builder.append("\t");
      _builder.append("text    ");
      _builder.newLine();
      _builder.append("}");
      _builder.newLine();
      final String sample = _builder.toString();
      StringConcatenation _builder_1 = new StringConcatenation();
      _builder_1.append("deepClone");
      _builder_1.newLine();
      _builder_1.append("package a.b.m");
      _builder_1.newLine();
      _builder_1.append("a.b.m.Paragraph {");
      _builder_1.newLine();
      _builder_1.append("\t");
      _builder_1.append("number");
      _builder_1.newLine();
      _builder_1.append("\t");
      _builder_1.append("text");
      _builder_1.newLine();
      _builder_1.append("}");
      final String expectedFormattedSample = _builder_1.toString();
      final Model model = this.parser.parse(sample);
      this.validationHelper.assertNoErrors(model);
      SaveOptions.Builder _newBuilder = SaveOptions.newBuilder();
      SaveOptions.Builder _format = _newBuilder.format();
      SaveOptions _options = _format.getOptions();
      final String resultFormattedCode = this._iSerializer.serialize(model, _options);
      Assert.assertEquals(expectedFormattedSample, resultFormattedCode);
    } catch (Throwable _e) {
      throw Exceptions.sneakyThrow(_e);
    }
  }
}
