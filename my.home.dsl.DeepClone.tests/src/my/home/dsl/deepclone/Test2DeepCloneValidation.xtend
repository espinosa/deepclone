package my.home.dsl.deepclone

import com.google.inject.Inject
import my.home.dsl.DeepCloneInjectorProvider
import my.home.dsl.deepClone.Model
import my.home.dsl.deepclone.test.utils.MyValidationHelper
import org.eclipse.xtext.junit4.InjectWith
import org.eclipse.xtext.junit4.XtextRunner
import org.eclipse.xtext.junit4.util.ParseHelper
import org.eclipse.xtext.junit4.validation.ValidationTestHelper
import org.junit.Test
import org.junit.runner.RunWith

import static org.junit.Assert.*

@InjectWith(typeof(DeepCloneInjectorProvider))
@RunWith(typeof(XtextRunner))
class DeepCloneValidationTest {
	
	@Inject ParseHelper<Model> parser
	@Inject ValidationTestHelper validationHelper
	@Inject MyValidationHelper myValidationHelper
 
	@Test
	def void validateCorrectModelWithInclusions() {
		val code = '''
		deepClone
		package a.b.m
		a.b.m.Paragraph {
			number
			text
		}
		'''
		val model = parser.parse(code)
		validationHelper.assertNoErrors(model)
	}
	
	@Test
	def void validateCorrectModelWithExclusions() {
		val code = '''
		deepClone
		package a.b.m
		a.b.m.Paragraph {
			-number
			-text
		}
		'''
		val model = parser.parse(code)
		validationHelper.assertNoErrors(model)
	}
	
	@Test
	def void validateCorrectModelWithInclusionsAndExclusions() {
		val code = '''
		deepClone
		package a.b.m
		a.b.m.Paragraph {
			-number
			text
		}
		'''
		val model = parser.parse(code)
		validationHelper.assertNoErrors(model)
	}
	
	@Test
	def void validateIncorrectModel_MissingAndSurplus() {
		val code = '''
		deepClone
		package a.b.m
		a.b.m.Paragraph {
			text
			fooField
		}
		'''
		val model = parser.parse(code)
		val expectedValidationErrors = '''
			The cloner for type a.b.m.Paragraph must exclude or include field number, line: 3, object: ClassCloner
			The cloner for type a.b.m.Paragraph references unknown field fooField, line: 5, object: SimpleField
			'''.toString
		val actualValidationErrors = myValidationHelper.validate(model).issuesToString 
		assertEquals(
			expectedValidationErrors, actualValidationErrors
		)
	}
	
	@Test
	def void validateIncorrectModel_MissingAndSurplus_namedCloner() {
		val code = '''
		deepClone
		package a.b.m
		a.b.m.Paragraph ShallowCloner {
			text
			fooField
		}
		'''
		val model = parser.parse(code)
		val expectedValidationErrors = '''
			The cloner for type a.b.m.Paragraph must exclude or include field number, line: 3, object: ClassCloner
			The cloner for type a.b.m.Paragraph references unknown field fooField, line: 5, object: SimpleField
			'''.toString
		val actualValidationErrors = myValidationHelper.validate(model).issuesToString 
		assertEquals(
			expectedValidationErrors, actualValidationErrors
		)
	}
	
	@Test
	def void validateIncorrectDeepModel_MissingAndSurplus() {
		val code = '''
		deepClone
		
		package my.home.cloners
		
		a.b.m.Magazine {
			-isbn
			article {
				name
				-author
				-pageNumber
				section {
					-name
					-paragraph
				}
			}
		}
		'''
		val model = parser.parse(code)
		val expectedValidationErrors = 	'''
			The cloner for type a.b.m.Magazine must exclude or include field name, line: 5, object: ClassCloner
			The cloner for type a.b.m.Section must exclude or include field description, line: 11, object: ComplexField
			'''.toString
		val actualValidationErrors = myValidationHelper.validate(model).issuesToString
		assertEquals(
			expectedValidationErrors,
			actualValidationErrors
		)
	}
	
	@Test
	def void validateIncorrectDeepModelWithComments() {
		val code = '''
		deepClone
		
		package my.home.cloners
		
		/** javadoc style comment */
		//some cooment
		a.b.m.Magazine {
			-isbn
			// declaration
			article {
				name
				-author
				-pageNumber
				section {
					-name
					/*
					  description
					 */
					-paragraph
				}
			}
		}
		'''
		val model = parser.parse(code)
		val expectedValidationErrors = '''
			The cloner for type a.b.m.Magazine must exclude or include field name, line: 7, object: ClassCloner
			The cloner for type a.b.m.Section must exclude or include field description, line: 14, object: ComplexField
			'''.toString
		val actualValidationErrors = myValidationHelper.validate(model).issuesToString
		assertEquals(
			expectedValidationErrors,
			actualValidationErrors
		)
	}
	
	@Test
	def void validateCorrectDeepModelWithComments() {
		val code = '''
		deepClone
		
		package my.home.cloners
		
		/** javadoc style comment */
		//some cooment
		a.b.m.Magazine {
			-isbn
			name  // end of line comment
			article {
				name
				-author
				-pageNumber
				section {
					name 
					description 
					-paragraph
				}
			}
		}
		'''
		val model = parser.parse(code)
		val	expectedValidationErrors = ""
		val actualValidationErrors = myValidationHelper.validate(model).issuesToString
		assertEquals(
			expectedValidationErrors,
			actualValidationErrors
		)
	}
}