package my.home.dsl.deepclone

import com.google.inject.Inject
import my.home.dsl.DeepCloneInjectorProvider
import my.home.dsl.deepClone.Body
import my.home.dsl.deepClone.Model
import my.home.dsl.utils.DeepCloneUtils
import org.eclipse.xtext.junit4.InjectWith
import org.eclipse.xtext.junit4.XtextRunner
import org.eclipse.xtext.junit4.util.ParseHelper
import org.eclipse.xtext.resource.SaveOptions
import org.eclipse.xtext.serializer.ISerializer
import org.junit.Test
import org.junit.runner.RunWith

import static org.junit.Assert.*

/**
 * https://bugs.eclipse.org/bugs/show_bug.cgi?id=396426
 * For more similar tests look at Test6DeepCloneModelManipulation 
 */
@InjectWith(typeof(DeepCloneInjectorProvider))
@RunWith(typeof(XtextRunner))
class Bug396426RegressionTest {
	
	@Inject ParseHelper<Model> parser
	@Inject extension ISerializer
	@Inject extension DeepCloneUtils 
	
	@Test
	def void whenAddMissingFieldToBooClonerFormattingOfSectionElementShouldBePreserved_withFormattingOff() {
		val sample = '''
		deepClone
		package my.home.cloners
		a.b.m.Book BookCloner {
			name
			-author
			section {
				name
				description
			}
		}
		'''
		
		val model = parser.parse(sample)
		val cloner = (model as Body).cloners.get(0).asContainer
		
		// do model manipulation - add missing element, missing field, 
		// same operation is done inside relevant QuickFix operation
		cloner.addFields(newArrayList("paragraph"))  
		
		// and serialize back to text with formatting switched off
		val result = model.serialize
		
		val expectedModifiedSample = '''
		deepClone
		package my.home.cloners
		a.b.m.Book BookCloner {
			name
			-author
			section {
				name
				description
			}
			paragraph
		}'''.toString
		
		assertEquals(
			expectedModifiedSample,
			result
		)
	}
	
	@Test
	def void whenAddMissingFieldToBooClonerFormattingOfSectionElementShouldBePreserved_withFormattingOn() {
		val sample = '''
		deepClone
		package my.home.cloners
		a.b.m.Book BookCloner {
			name
			-author
			section {
				name
				description
			}
		}
		'''
		
		val model = parser.parse(sample)
		val cloner = (model as Body).cloners.get(0).asContainer
		
		// do model manipulation - add missing element, missing field, 
		// same operation is done inside relevant QuickFix operation
		cloner.addFields(newArrayList("paragraph"))  
		
		// and serialize back to text with formatting switched ON
		val result = model.serialize(
			SaveOptions::newBuilder.format().getOptions()
		)
		
		val expectedModifiedSample = '''
		deepClone
		package my.home.cloners
		a.b.m.Book BookCloner {
			name
			-author
			section {
				name
				description
			}
			paragraph
		}'''.toString
		
		assertEquals(
			expectedModifiedSample,
			result
		)
	}
}