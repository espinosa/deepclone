package my.home.dsl.deepclone

import com.google.inject.Inject
import my.home.dsl.DeepCloneInjectorProvider
import my.home.dsl.deepClone.Model
import my.home.dsl.deepclone.test.utils.MyValidationHelper
import org.eclipse.xtext.generator.IFileSystemAccess
import org.eclipse.xtext.generator.IGenerator
import org.eclipse.xtext.generator.InMemoryFileSystemAccess
import org.eclipse.xtext.junit4.InjectWith
import org.eclipse.xtext.junit4.XtextRunner
import org.eclipse.xtext.junit4.util.ParseHelper
import org.junit.Test
import org.junit.runner.RunWith

import static org.junit.Assert.*

/**
 * Parse sample DeepClone DSL file and test DeepClone to Java generator.  
 * Files are generated to memory, see {@link InMemoryFileSystemAccess}, perfect for testing.
 * @see http://christiandietrich.wordpress.com/2012/05/08/unittesting-xtend-generators/
 * 
 * @author espinosa
 */
@RunWith(typeof(XtextRunner))
@InjectWith(typeof(DeepCloneInjectorProvider))
class DeepCloneAdvancedGeneratorTest {
	
	@Inject IGenerator generatorDslToJava
	@Inject ParseHelper<Model> parseHelper
	@Inject MyValidationHelper myValidationHelper
	
	@Test
	def modelWithOneCollectionFieldShouldValidateGenerics() {
		val code = '''
		deepClone
		
		package a.b.c.cloners
		
		a.b.c.ListWrapper1 ListWrapper1Cloner {
			personNames  // List<String>
		}
		'''
		val model = parseHelper.parse(code)
		// assert no issues
		assertEquals(
			'''
			'''.toString,
			 myValidationHelper.validate(model).issuesToString.trim
		)
		val fsa = new InMemoryFileSystemAccess()
		generatorDslToJava.doGenerate(model.eResource, fsa)
		assertEquals(1, fsa.textFiles.size)
		val topClonerJavaFileName = IFileSystemAccess::DEFAULT_OUTPUT + "a/b/c/cloners/ListWrapper1Cloner.java"
		assertTrue(fsa.textFiles.containsKey(topClonerJavaFileName))
		assertEquals(
			'''
			package a.b.c.cloners;
			public class ListWrapper1Cloner {
				public a.b.c.ListWrapper1 apply(a.b.c.ListWrapper1 other) {
					if (other == null) return null;
					a.b.c.ListWrapper1 it = new a.b.c.ListWrapper1();
					it.setPersonNames(personNamesCollectionCloner(it.getPersonNames(), other.getPersonNames()));
					return it;
				}
				
				private java.util.List<java.lang.String> personNamesCollectionCloner(java.util.List<java.lang.String> thisCollection, java.util.List<java.lang.String> otherCollection) {
					if (otherCollection == null) return null;
					if (thisCollection == null) thisCollection = new java.util.ArrayList<java.lang.String>();
					for (java.lang.String otherCollectionItem : otherCollection) {
						thisCollection.add(otherCollectionItem);
					}
					return thisCollection;
				}
			}
			'''.toString, 
			fsa.textFiles.get(topClonerJavaFileName).toString
		)
	}
	
	@Test
	def modelWithCollectionTypesWithGenericsShouldValidateAndGenerateLoopsForCollectionTypes1() {
		val code = '''
		deepClone
		
		package a.b.c.cloners
		
		a.b.m2.Book BookCloner789 {
			name
			author
			sections {
				name
				description
				-paragraphs
			}
		}
		'''
		val model = parseHelper.parse(code)
		// assert no issues
		assertEquals(
			'''
			'''.toString,
			 myValidationHelper.validate(model).issuesToString.trim
		)
		val fsa = new InMemoryFileSystemAccess()
		generatorDslToJava.doGenerate(model.eResource, fsa)
		assertEquals(1, fsa.textFiles.size)
		val topClonerJavaFileName = IFileSystemAccess::DEFAULT_OUTPUT + "a/b/c/cloners/BookCloner789.java"
		assertTrue(fsa.textFiles.containsKey(topClonerJavaFileName))
		assertEquals(
			'''
			package a.b.c.cloners;
			public class BookCloner789 {
				public a.b.m2.Book apply(a.b.m2.Book other) {
					if (other == null) return null;
					a.b.m2.Book it = new a.b.m2.Book();
					it.setName(other.getName());
					it.setAuthor(other.getAuthor());
					it.setSections(sectionsCollectionCloner(it.getSections(), other.getSections()));
					return it;
				}
				
				private java.util.List<a.b.m2.Section> sectionsCollectionCloner(java.util.List<a.b.m2.Section> thisCollection, java.util.List<a.b.m2.Section> otherCollection) {
					if (otherCollection == null) return null;
					if (thisCollection == null) thisCollection = new java.util.ArrayList<a.b.m2.Section>();
					for (a.b.m2.Section otherCollectionItem : otherCollection) {
						thisCollection.add(sectionsCloner.apply(otherCollectionItem));
					}
					return thisCollection;
				}
				
				private final SectionsCloner sectionsCloner = new SectionsCloner();
				public static class SectionsCloner {
					public a.b.m2.Section apply(a.b.m2.Section other) {
						if (other == null) return null;
						a.b.m2.Section it = new a.b.m2.Section();
						it.setName(other.getName());
						it.setDescription(other.getDescription());
						return it;
					}
				}
			}
			'''.toString, 
			fsa.textFiles.get(topClonerJavaFileName).toString
		)
	}
	
	// TODO: move this test to validation tests
	@Test
	def validationShouldDetectMultiple() {
		val code = '''
		deepClone
		
		package a.b.c.cloners
		
		a.b.m2.Book BookCloner987 {
			name
			author
			sections {
				name
				description
				paragraphs
				paragraphs {
					number
					text
				}
			}
		}
		'''
		val model = parseHelper.parse(code)
		// assert no issues
		assertEquals(
			'''
			Field paragraphs is declared more then once in container BookCloner987.sections, line: 11, object: SimpleField
			Field paragraphs is declared more then once in container BookCloner987.sections, line: 12, object: ComplexField
			'''.toString.trim,
			 myValidationHelper.validate(model).issuesToString.trim
		)
	}
	
	@Test
	def modelWithCollectionTypesWithGenericsShouldValidateAndGenerateLoopsForCollectionTypes2() {
		val code = '''
		deepClone
		
		package a.b.c.cloners
		
		a.b.m2.Book BookCloner987 {
			name
			author
			sections {
				name
				description
				paragraphs {
					number
					text
				}
			}
		}
		'''
		val model = parseHelper.parse(code)
		// assert no issues
		assertEquals(
			'''
			'''.toString,
			 myValidationHelper.validate(model).issuesToString.trim
		)
		val fsa = new InMemoryFileSystemAccess()
		generatorDslToJava.doGenerate(model.eResource, fsa)
		assertEquals(1, fsa.textFiles.size)
		val topClonerJavaFileName = IFileSystemAccess::DEFAULT_OUTPUT + "a/b/c/cloners/BookCloner987.java"
		assertTrue(fsa.textFiles.containsKey(topClonerJavaFileName))
		assertEquals(
			'''
			package a.b.c.cloners;
			public class BookCloner987 {
				public a.b.m2.Book apply(a.b.m2.Book other) {
					if (other == null) return null;
					a.b.m2.Book it = new a.b.m2.Book();
					it.setName(other.getName());
					it.setAuthor(other.getAuthor());
					it.setSections(sectionsCollectionCloner(it.getSections(), other.getSections()));
					return it;
				}
				
				private java.util.List<a.b.m2.Section> sectionsCollectionCloner(java.util.List<a.b.m2.Section> thisCollection, java.util.List<a.b.m2.Section> otherCollection) {
					if (otherCollection == null) return null;
					if (thisCollection == null) thisCollection = new java.util.ArrayList<a.b.m2.Section>();
					for (a.b.m2.Section otherCollectionItem : otherCollection) {
						thisCollection.add(sectionsCloner.apply(otherCollectionItem));
					}
					return thisCollection;
				}
				
				private final SectionsCloner sectionsCloner = new SectionsCloner();
				public static class SectionsCloner {
					public a.b.m2.Section apply(a.b.m2.Section other) {
						if (other == null) return null;
						a.b.m2.Section it = new a.b.m2.Section();
						it.setName(other.getName());
						it.setDescription(other.getDescription());
						it.setParagraphs(paragraphsCollectionCloner(it.getParagraphs(), other.getParagraphs()));
						return it;
					}
					
					private java.util.Set<a.b.m2.Paragraph> paragraphsCollectionCloner(java.util.Set<a.b.m2.Paragraph> thisCollection, java.util.Set<a.b.m2.Paragraph> otherCollection) {
						if (otherCollection == null) return null;
						if (thisCollection == null) thisCollection = new java.util.HashSet<a.b.m2.Paragraph>();
						for (a.b.m2.Paragraph otherCollectionItem : otherCollection) {
							thisCollection.add(paragraphsCloner.apply(otherCollectionItem));
						}
						return thisCollection;
					}
					
					private final ParagraphsCloner paragraphsCloner = new ParagraphsCloner();
					public static class ParagraphsCloner {
						public a.b.m2.Paragraph apply(a.b.m2.Paragraph other) {
							if (other == null) return null;
							a.b.m2.Paragraph it = new a.b.m2.Paragraph();
							it.setNumber(other.getNumber());
							it.setText(other.getText());
							return it;
						}
					}
				}
			}
			'''.toString, 
			fsa.textFiles.get(topClonerJavaFileName).toString
		)
	}
	
	@Test
	def void modelWithClonerReferences() {
		val code = '''
		deepClone
		
		package a.b.m
		
		a.b.m.Book {
			name
			-author
			&section SectionDeep
		}
		
		a.b.m.Section SectionDeep {
			name
			description
			paragraph {
				number
				text
			}
		}
		
		a.b.m.Section SectionReduced {
			name
			description
			-paragraph
		}
		'''
		val model = parseHelper.parse(code)
		// assert no issues
		assertEquals(
			'''
			'''.toString,
			 myValidationHelper.validate(model).issuesToString.trim
		)
		val fsa = new InMemoryFileSystemAccess()
		generatorDslToJava.doGenerate(model.eResource, fsa)
		assertEquals(3, fsa.textFiles.size)
		val bookCloner = IFileSystemAccess::DEFAULT_OUTPUT + "a/b/m/BookCloner.java"
		assertTrue(fsa.textFiles.containsKey(bookCloner))
		assertEquals(
			'''
			package a.b.m;
			public class BookCloner {
				public a.b.m.Book apply(a.b.m.Book other) {
					if (other == null) return null;
					a.b.m.Book it = new a.b.m.Book();
					it.setName(other.getName());
					it.setSection(sectionCloner.apply(other.getSection()));
					return it;
				}
				
				private final a.b.m.SectionDeep sectionCloner = new a.b.m.SectionDeep();
			}
			'''.toString, 
			fsa.textFiles.get(bookCloner).toString
		)
	}
}
