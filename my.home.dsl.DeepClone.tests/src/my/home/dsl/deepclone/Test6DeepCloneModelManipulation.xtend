package my.home.dsl.deepclone

import com.google.inject.Inject
import my.home.dsl.DeepCloneInjectorProvider
import my.home.dsl.deepClone.Body
import my.home.dsl.deepClone.Model
import my.home.dsl.utils.DeepCloneUtils
import org.eclipse.xtext.junit4.InjectWith
import org.eclipse.xtext.junit4.XtextRunner
import org.eclipse.xtext.junit4.util.ParseHelper
import org.eclipse.xtext.serializer.ISerializer
import org.junit.Ignore
import org.junit.Test
import org.junit.runner.RunWith

import static org.junit.Assert.*

@InjectWith(typeof(DeepCloneInjectorProvider))
@RunWith(typeof(XtextRunner))
class DeepCloneModelManipulationTest {
	
	@Inject ParseHelper<Model> parser
	@Inject extension ISerializer
	@Inject extension DeepCloneUtils 
	
	/** WORKS */
	@Test
	def void removeFieldFromModelAndSerializeBack_removeFieldFromRootClonerElement() {
		val sample = '''
		deepClone
		package my.home.cloners
		a.b.m.Magazine {
			-isbn
			name
			fooFoo
			-article
		}
		'''
		
		val expectedModifiedSample = '''
		deepClone
		package my.home.cloners
		a.b.m.Magazine {
			-isbn
			name
			-article
		}'''.toString
		
		val model = parser.parse(sample)
		val cloner = (model as Body).cloners.get(0).asContainer
		cloner.removeFields(newArrayList("fooFoo"))  // do tested model manipulation 
		val result = model.serialize
		assertEquals(
			expectedModifiedSample,
			result
		)
	}
	
	/** WORKS */
	@Test
	def void removeFieldFromModelAndSerializeBack_removeFieldDeeperInTheStructure() {
		val sample = '''
		deepClone
		package my.home.cloners
		a.b.m.Magazine {
			-isbn
			name
			article {
				name
				-author
				-pageNumber
				fooFoo
				section {
					name
					description
					-paragraph
				}
			}
		}
		'''
		
		val expectedModifiedSample = '''
		deepClone
		package my.home.cloners
		a.b.m.Magazine {
			-isbn
			name
			article {
				name
				-author
				-pageNumber
				section {
					name
					description
					-paragraph
				}
			}
		}'''.toString
		val model = parser.parse(sample)
		val articleContainerField = (model as Body).cloners.get(0).fields.findFirst[it.fieldName=="article"].asContainer
		articleContainerField.removeFields(newArrayList("fooFoo"))  // do tested model manipulation 
		val result = model.serialize
		assertEquals(
			expectedModifiedSample,
			result
		)
	}
	
	/** WORKS */
	@Test
	def void addFieldToModelAndSerializeBack_addFieldToRootClonerElement() {
		val sample = '''
		deepClone
		package my.home.cloners
		a.b.m.Magazine {
			-isbn
			name
			-article
		}
		'''
		
		val expectedModifiedSample = '''
		deepClone
		package my.home.cloners
		a.b.m.Magazine {
			-isbn
			name
			-article
			fooFoo
		}'''.toString
		
		val model = parser.parse(sample)
		val cloner = (model as Body).cloners.get(0).asContainer
		cloner.addFields(newArrayList("fooFoo"))  // do tested model manipulation 
		val result = model.serialize
		assertEquals(
			expectedModifiedSample,
			result
		)
	}
	
	/** WORKS */
	@Test
	def void addFieldToModelAndSerializeBack_addFieldDeeperInTheStructure() {
		val sample = '''
		deepClone
		package my.home.cloners
		a.b.m.Magazine {
			-isbn
			name
			article {
				name
				-author
				-pageNumber
				section {
					name
					description
					-paragraph
				}
			}
		}
		'''
		
		val expectedModifiedSample = '''
		deepClone
		package my.home.cloners
		a.b.m.Magazine {
			-isbn
			name
			article {
				name
				-author
				-pageNumber
				section {
					name
					description
					-paragraph
				}
				fooFoo
			}
		}'''.toString
		val model = parser.parse(sample)
		val articleContainerField = (model as Body).cloners.get(0).fields.findFirst[it.fieldName=="article"].asContainer
		articleContainerField.addFields(newArrayList("fooFoo"))  // do tested model manipulation 
		val result = model.serialize
		assertEquals(
			expectedModifiedSample,
			result
		)
	}
	
	/** FAILS */
	@Test
	@Ignore
	def void addFieldToModelAndSerializeBack_testComments() {
		val sample = '''
		deepClone
		package my.home.cloners
		a.b.m.Magazine {
			-isbn
			name
			article {
				// comment 1
				name
				// comment 2
				-author
				-pageNumber
				// comment 3
				section {
					name
					description
					-paragraph
					// comment 4
				}
				// comment 5
			}
		}
		'''
		
		val expectedModifiedSample = '''
		deepClone
		package my.home.cloners
		a.b.m.Magazine {
			-isbn
			name
			article {
				// comment 1
				name
				// comment 2
				-author
				-pageNumber
				// comment 3
				section {
					name
					description
					-paragraph
					// comment 4
				}
				// comment 5
				fooFoo
			}
		}'''.toString
		
		val model = parser.parse(sample)
		val articleContainerField = (model as Body).cloners.get(0).fields.findFirst[it.fieldName=="article"].asContainer
		articleContainerField.addFields(newArrayList("fooFoo"))  // do tested model manipulation 
		val result = model.serialize
		assertEquals(
			expectedModifiedSample,
			result
		)
	}
	
	/** FAILS */
	@Test
	@Ignore
	def void addFieldToModelAndSerializeBack_testComments_simple() {
		val sample = '''
		deepClone
		package my.home.cloners
		a.b.m.Magazine {
			isbn
			name
			article
			// comment 1
		}
		'''
		
		val expectedModifiedSample = '''
		deepClone
		package my.home.cloners
		a.b.m.Magazine {
			isbn
			name
			article
			// comment 1
			fooFoo
		}'''.toString
		val model = parser.parse(sample)
		val articleContainerField = (model as Body).cloners.get(0).asContainer
		articleContainerField.addFields(newArrayList("fooFoo"))  // do tested model manipulation 
		val result = model.serialize
		assertEquals(
			expectedModifiedSample,
			result
		)
	}
	
	/** FAILS */
	@Test
	@Ignore
	def void addFieldToModelAndSerializeBack_testCommentsHandling_emptyContainerWithJustComment() {
		val sample = '''
		deepClone
		package my.home.cloners // comment 0
		a.b.m.Magazine {
			// comment 1
		} // comment 2
		// comment 3
		'''
		
		val expectedModifiedSample = '''
		deepClone
		package my.home.cloners // comment 0
		a.b.m.Magazine {
			// comment 1
			fooFoo
		} // comment 2
		// comment 3
		'''.toString
		
		val model = parser.parse(sample)
		val articleContainerField = (model as Body).cloners.get(0).asContainer
		articleContainerField.addFields(newArrayList("fooFoo"))  // do tested model manipulation 
		val result = model.serialize
		assertEquals(
			expectedModifiedSample,
			result
		)
	}
	
	/** FAILS */
	@Test
	@Ignore
	def void addFieldToModelAndSerializeBack_testSlashStarCommentsHandling() {
		val sample = '''
		deepClone
		package my.home.cloners /* comment 0 */
		a.b.m.Magazine /* comment 000 */ {
			/* comment 1 */
		} /* comment 2 */
		/* comment 3 */
		'''
		
		val expectedModifiedSample = '''
		deepClone
		package my.home.cloners /* comment 0 */
		a.b.m.Magazine /* comment 000 */ {
			/* comment 1 */
			fooFoo
		} /* comment 2 */
		/* comment */
		'''.toString
		
		val model = parser.parse(sample)
		val articleContainerField = (model as Body).cloners.get(0).asContainer
		articleContainerField.addFields(newArrayList("fooFoo"))  // do tested model manipulation 
		val result = model.serialize
		assertEquals(
			expectedModifiedSample,
			result
		)
	}
	
	/** FAILS */
	@Test
	@Ignore
	def void addFieldToModelAndSerializeBack_testCommentsHandling_simplestCase() {
		val sample = '''
		deepClone
		package my.home.cloners
		a.b.m.Magazine {
			// comment 1
		}
		'''
		
		val expectedModifiedSample = '''
		deepClone
		package my.home.cloners
		a.b.m.Magazine {
			// comment 1
			fooFoo
		}
		'''.toString
		
		val model = parser.parse(sample)
		val articleContainerField = (model as Body).cloners.get(0).asContainer
		articleContainerField.addFields(newArrayList("fooFoo"))  // do tested model manipulation 
		val result = model.serialize
		assertEquals(
			expectedModifiedSample,
			result
		)
	}
	
	/** WORKS */
	@Test
	def void doNotChangeAnythingAndSerializeBack_testCommentsHandling_simplestCase() {
		val sample = '''
		deepClone
		package my.home.cloners
		a.b.m.Magazine {
			// comment 1
		}
		'''
		
		val expectedModifiedSample = '''
		deepClone
		package my.home.cloners
		a.b.m.Magazine {
			// comment 1
		}'''.toString
		
		val model = parser.parse(sample) 
		val result = model.serialize
		assertEquals(
			expectedModifiedSample,
			result
		)
	}
	
	/** FAILS */
	@Test
	@Ignore
	def void removeFieldFromModelAndSerializeBack_testCommentsHandling_simplestCase() {
		val sample = '''
		deepClone
		package my.home.cloners
		a.b.m.Magazine {
			// comment 1
			name
			// comment 2
			fooFoo
			// comment 3
			article
			// comment 4
		}
		'''
		
		val expectedModifiedSample = '''
		deepClone
		package my.home.cloners
		a.b.m.Magazine {
			// comment 1
			name
			// comment 2
			// comment 3
			article
			// comment 4
		}'''.toString
		
		val model = parser.parse(sample)
		val cloner = (model as Body).cloners.get(0).asContainer
		cloner.removeFields(newArrayList("fooFoo"))  // do tested model manipulation 
		val result = model.serialize
		assertEquals(
			expectedModifiedSample,
			result
		)
	}
}