package my.home.dsl.deepclone

import com.google.inject.Inject
import my.home.dsl.DeepCloneInjectorProvider
import my.home.dsl.deepClone.Model
import org.eclipse.xtext.junit4.InjectWith
import org.eclipse.xtext.junit4.XtextRunner
import org.eclipse.xtext.junit4.util.ParseHelper
import org.eclipse.xtext.junit4.validation.ValidationTestHelper
import org.junit.Test
import org.junit.runner.RunWith
import org.eclipse.xtext.serializer.ISerializer
import org.eclipse.xtext.resource.SaveOptions

import static org.junit.Assert.*

@InjectWith(typeof(DeepCloneInjectorProvider))
@RunWith(typeof(XtextRunner))
class DeepCloneFormatterTest {
	
	@Inject ParseHelper<Model> parser
	@Inject ValidationTestHelper validationHelper
	@Inject extension ISerializer
 
	@Test
	def void formatTrivialExample() {
		val sample = '''
		deepClone
		package a.b.m
		a.b.m.Paragraph {
		number
		text
		}
		'''
		val expectedFormattedSample = '''
		deepClone
		package a.b.m
		a.b.m.Paragraph {
			number
			text
		}'''.toString
		val model = parser.parse(sample)
		validationHelper.assertNoErrors(model)
		val resultFormattedCode = model.serialize(
			SaveOptions::newBuilder.format().getOptions() // if this is omitted, then there is no formatting
		)
		assertEquals(
			expectedFormattedSample,
			resultFormattedCode
		)
	}
	
	@Test
	def void formatMoreComplexSampleWithNestedStructuresAndComments() {
		val sample = '''
		deepClone
		
		package my.home.cloners
		
		/** javadoc style comment */
		//some cooment
		a.b.m.Magazine {
		-isbn
		name  // end of   line comment
		article {
		name
		-author
		-pageNumber
		section {
		name 
		description 
		-paragraph
		}
		}
		}
		'''
		
		val expectedFormattedSample = '''
		deepClone
		package my.home.cloners
		
		/** javadoc style comment */
		//some cooment
		a.b.m.Magazine {
			-isbn
			name // end of   line comment
			article {
				name
				-author
				-pageNumber
				section {
					name
					description
					-paragraph
				}
			}
		}'''.toString
		val model = parser.parse(sample)
		validationHelper.assertNoErrors(model)
		val resultFormattedCode = model.serialize(
			SaveOptions::newBuilder.format().getOptions() // if this is omitted, then there is no formatting
		)
		assertEquals(
			expectedFormattedSample,
			resultFormattedCode
		)
	}
	
	@Test
	def void serializeSampleWithoutFormattingOption_noChangeIsExpected() {
		// sample with un-formatted code and extra trailing space characters on some lines - all should be preserved as it is
		// This is a special test - demonstrate that 
		// 1) extracted model still remembers original text layout including all whitespace characters.
		// 2) without providing formatting options to serialize method no formatting happens
		val sample = '''
		deepClone
		package a.b.m    
		a.b.m.Paragraph {
			  number
			text    
		}
		'''
		val expectedResut = '''
		deepClone
		package a.b.m    
		a.b.m.Paragraph {
			  number
			text    
		}'''.toString
		val model = parser.parse(sample)
		validationHelper.assertNoErrors(model)
		val serializedModel = model.serialize
		assertEquals(
			expectedResut,
			serializedModel
		)
	}
	
	@Test
	def void formatAlreadyFormattedSampleExpectTrailingWhitespacesToBeStripped() {
		// use already well formatted sample but with trailing whitespace characters
		// formatting should strip them
		val sample = '''
		deepClone
		package a.b.m    
		a.b.m.Paragraph {
			number
			text    
		}
		'''
		val expectedFormattedSample = '''
		deepClone
		package a.b.m
		a.b.m.Paragraph {
			number
			text
		}'''.toString
		val model = parser.parse(sample)
		validationHelper.assertNoErrors(model)
		val resultFormattedCode = model.serialize(
			SaveOptions::newBuilder.format().getOptions() // if this is omitted, then there is no formatting
		)
		assertEquals(
			expectedFormattedSample,
			resultFormattedCode
		)
	}
}