package my.home.dsl.deepclone

import com.google.inject.Inject
import my.home.dsl.DeepCloneInjectorProvider
import my.home.dsl.deepClone.Body
import my.home.dsl.deepClone.Model
import my.home.dsl.deepclone.test.utils.DeepCloneInspector
import my.home.dsl.utils.DeepCloneUtils
import org.eclipse.xtext.junit4.InjectWith
import org.eclipse.xtext.junit4.XtextRunner
import org.eclipse.xtext.junit4.util.ParseHelper
import org.junit.Test
import org.junit.runner.RunWith

import static org.junit.Assert.*

@InjectWith(typeof(DeepCloneInjectorProvider))
@RunWith(typeof(XtextRunner))
class DeepCloneJvmTypeInferrerTest {

	@Inject ParseHelper<Model> parser
	@Inject extension DeepCloneUtils
	@Inject extension DeepCloneInspector

	@Test
	def void clonerReferringOnlyExistingTypes() {
		val sample = '''
			deepClone
			package my.home.cloners
			a.b.m.Book BookCloner {
				name
				author
				section {
					name
					description
					paragraph
				}
			}
		'''

		val expectedSampleWithInferredTypes = '''
			BookCloner : a.b.m.Book {
				name : java.lang.String
				author : java.lang.String
				section : a.b.m.Section {
					name : java.lang.String
					description : java.lang.String
					paragraph : a.b.m.Paragraph
				}
			}
		'''

		val model = parser.parse(sample)

		//val cloner = (model as Body).cloners.get(0).asContainer
		(model as Body).inferJavaTypes

		// and serialize back to text with "inspector"
		val result = (model as Body).serialize

		assertEquals(
			expectedSampleWithInferredTypes.toString,
			result.toString
		)
	}

	@Test
	def void clonerReferringOnlyExistingTypes_collectionsShouldIndicateParameterType() {
		val sample = '''
			deepClone
			package my.home.cloners
			a.b.m2.Book BookCloner {
				name
				author
				sections {
					name
					description
					paragraphs
				}
			}
		'''

		val expectedSampleWithInferredTypes = '''
			BookCloner : a.b.m2.Book {
				name : java.lang.String
				author : java.lang.String
				sections : a.b.m2.Section {
					name : java.lang.String
					description : java.lang.String
					paragraphs : a.b.m2.Paragraph
				}
			}
		'''

		val model = parser.parse(sample)

		//val cloner = (model as Body).cloners.get(0).asContainer
		(model as Body).inferJavaTypes

		// and serialize back to text with "inspector"
		val result = (model as Body).serialize

		assertEquals(
			expectedSampleWithInferredTypes.toString,
			result.toString
		)
	}

	@Test
	def void clonerReferringOneNonexistentField_suchMustBeInferredAsUnknownTypeAndNotNull() {
		val sample = '''
			deepClone
			package my.home.cloners
			a.b.m.Book BookCloner {
				name
				author
				fooFoo  // a.b.m.Book does not have any fooFoo field
			}
		'''

		val expectedSampleWithInferredTypes = '''
			BookCloner : a.b.m.Book {
				name : java.lang.String
				author : java.lang.String
				fooFoo : [unknown]
			}
		'''

		val model = parser.parse(sample)

		//val cloner = (model as Body).cloners.get(0).asContainer
		(model as Body).inferJavaTypes

		// and serialize back to text with "inspector"
		val result = (model as Body).serialize

		assertEquals(
			expectedSampleWithInferredTypes.toString,
			result.toString
		)
	}

	@Test
	def void unknownTypeShouldBeReferredAsUnknowTypeInludingNestedStructures() {
		val sample = '''
			deepClone
			package my.home.cloners
			a.b.m.Book BookCloner {
				fooFooX1
				name
				author
				fooFooY1 {  // a.b.m.Book does not have any fooFoo field
				   fooFooY11
				   fooFooY12 {
				   	  fooFooY121
				   }
				}
				fooFooZ1
			}
		'''

		val expectedSampleWithInferredTypes = '''
			BookCloner : a.b.m.Book {
				fooFooX1 : [unknown]
				name : java.lang.String
				author : java.lang.String
				fooFooY1 : [unknown] {
					fooFooY11 : [unknown]
					fooFooY12 : [unknown] {
						fooFooY121 : [unknown]
					}
				}
				fooFooZ1 : [unknown]
			}
		'''

		val model = parser.parse(sample)

		//val cloner = (model as Body).cloners.get(0).asContainer
		(model as Body).inferJavaTypes

		// and serialize back to text with "inspector"
		val result = (model as Body).serialize

		assertEquals(
			expectedSampleWithInferredTypes.toString,
			result.toString
		)
	}

	@Test
	def void rootClonerReferringNoneexistingClassShouldBeOfUnknownTypeIncludingAllItsNestedTypes() {

		// deliberate misspelling of a.b.m.Book class in root cloner definition makes it
		// unknown type (referring non-existing class); this should apply to all nested elements
		// even if they would be correct for a.b.m.Book 
		val sample = '''
			deepClone
			package my.home.cloners
			a.b.m.Bookkk BookCloner {  // deliberate mispelling of a.b.m.Book
				name
				author
				section {
				   	name
				description
				paragraph
				}
			}
		'''

		val expectedSampleWithInferredTypes = '''
			BookCloner : [unknown] {
				name : [unknown]
				author : [unknown]
				section : [unknown] {
					name : [unknown]
					description : [unknown]
					paragraph : [unknown]
				}
			}
		'''

		val model = parser.parse(sample)

		//val cloner = (model as Body).cloners.get(0).asContainer
		(model as Body).inferJavaTypes

		// and serialize back to text with "inspector"
		val result = (model as Body).serialize

		assertEquals(
			expectedSampleWithInferredTypes.toString,
			result.toString
		)
	}
}
