package my.home.dsl.deepclone.test.utils;

import java.util.List;

import org.eclipse.emf.ecore.EObject;
import org.eclipse.emf.ecore.resource.ResourceSet;
import org.eclipse.xtend2.lib.StringConcatenation;
import org.eclipse.xtext.junit4.validation.ValidationTestHelper;
import org.eclipse.xtext.validation.Issue;

import com.google.inject.Inject;

public class MyValidationHelper {
	
	@Inject ValidationTestHelper validationHelper;
	
	public ValidationResult validate(EObject model) {
		List<Issue> listOfValidationIssues = validationHelper.validate(model);
		return new ValidationResult(model, listOfValidationIssues);
	}
	
	public class ValidationResult {
		EObject model; 
		List<Issue> listOfValidationIssues;
		
		public ValidationResult(EObject model, List<Issue> listOfValidationIssues) {
			this.listOfValidationIssues = listOfValidationIssues;
			this.model = model;	
		}
		
		public EObject getModel() {
			return model;
		}
		
		public List<Issue> getIssues() {
			return listOfValidationIssues;
		}
		
		/**
		 * Convenient conversion method. Issue does not have reference to a
		 * EObject, but a URI string location relative to resource root. We need to
		 * convert this URI to a EObject to get things like name of offending structure.
		 */
		EObject getIssueEObject(Issue issue) {
			EObject object = null;
			ResourceSet rs = model.eResource().getResourceSet();
			if (rs != null && issue.getUriToProblem() != null) {
				object = rs.getEObject(issue.getUriToProblem(), true);
			}
			return object;
		}
		
		/**
		 * Get string representation of an issue, human friendly and suitable
		 * for simple textual check in a test
		 */
		public String issuesToString() {
			ValidationResult validationResult = this;
			StringConcatenation sc = new StringConcatenation();
			for (Issue issue : validationResult.getIssues()) {
				sc.append(issue.getMessage().toString() + ", line: " + issue.getLineNumber());
				EObject object = validationResult.getIssueEObject(issue);
				if (object!=null) {
					sc.append(", object: " + object.eClass().getName());
				}
				sc.newLine();
			}
			return sc.toString();
		}
	}
}
