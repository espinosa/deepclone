package my.home.dsl.deepclone.test.utils

import com.google.inject.Inject
import my.home.dsl.deepClone.Body
import my.home.dsl.deepClone.ClassCloner
import my.home.dsl.deepClone.ComplexField
import my.home.dsl.deepClone.FieldClonerType
import my.home.dsl.deepClone.SimpleField
import my.home.dsl.utils.ReflectionUtils

/**
 * Renders whole DeepClone semantic model to a string representation revealing inferred Java 
 * types of individual fields.<br>
 * Example:
 * <pre>
 *	BookCloner : a.b.m.Book {
 *		name : java.lang.String
 *		author : java.lang.String
 *		section : a.b.m.Section {
 *			name : java.lang.String
 *			description : java.lang.String
 *			paragraph : a.b.m.Paragraph
 *			fooFoo: [unknown]
 *		}
 *	}
 * </pre>
 */
class DeepCloneInspector {

	@Inject extension ReflectionUtils

	def serialize(Body body) '''
		«FOR cloner : body.cloners»
			«cloner.serializeCC»
		«ENDFOR»
	'''

	def serializeCC(ClassCloner cloner) '''
		«cloner.name» : «cloner.javaType.typeOrCollectionTypeParameter.qualifiedName» {
			«FOR field : cloner.fields»
				«IF field instanceof SimpleField»
					«field.fieldName» : «field.javaType.typeOrCollectionTypeParameter.qualifiedName»
				«ELSEIF field instanceof ComplexField»
					«field.fieldName» : «field.javaType.typeOrCollectionTypeParameter.qualifiedName» {
						«field.asComplex.serializeCF»
					}
				«ENDIF»
			«ENDFOR»
		}
	'''

	def protected CharSequence serializeCF(ComplexField parentField) '''
		«FOR field : parentField.fields»
			«IF field instanceof SimpleField»
				«field.fieldName» : «field.javaType.typeOrCollectionTypeParameter.qualifiedName»
			«ELSEIF field instanceof ComplexField»
				«field.fieldName» : «field.javaType.typeOrCollectionTypeParameter.qualifiedName» {
					«field.asComplex.serializeCF»
				}
			«ENDIF»
		«ENDFOR»
	'''

	def protected asComplex(FieldClonerType field) {
		field as ComplexField
	}
}
