package my.home.dsl.deepclone

import com.google.inject.Inject
import my.home.dsl.DeepCloneInjectorProvider
import my.home.dsl.deepClone.Body
import my.home.dsl.deepClone.Model
import my.home.dsl.utils.DeepCloneUtils
import org.eclipse.xtext.junit4.InjectWith
import org.eclipse.xtext.junit4.XtextRunner
import org.eclipse.xtext.junit4.util.ParseHelper
import org.junit.Test
import org.junit.runner.RunWith

import static org.junit.Assert.*

@InjectWith(typeof(DeepCloneInjectorProvider))
@RunWith(typeof(XtextRunner))
class DeepCloneUtilFindMissingFieldsTest {
	
	@Inject ParseHelper<Model> parser
	@Inject extension DeepCloneUtils 
	
	@Test
	def void detectMissingTwoSimpleFieldsFromRootCloner() {
		val sample = '''
		deepClone
		package my.home.cloners
		a.b.m.Book BookCloner {
			//name
			//author
			section {
				name
				description
			}
		}
		'''
		
		val expectedMissingFieldNamesForBookCloner = '''
			[author, name]'''
		
		val model = parser.parse(sample)
		val cloner = (model as Body).cloners.get(0).asContainer
		val missingFieldNamesSet = cloner.findMissingFieldsInContainerElement
		assertEquals(
			expectedMissingFieldNamesForBookCloner.toString,
			missingFieldNamesSet.sort.toArray.toString
		)
	}
	
	@Test
	def void detectMissingComplexFieldSectionFromRootCloner() {
		val sample = '''
		deepClone
		package my.home.cloners
		a.b.m.Book BookCloner {
			name
			author
			//section
		}
		'''
		
		val expectedMissingFieldNamesForBookCloner = '''
			[section]'''
		
		val model = parser.parse(sample)
		val cloner = (model as Body).cloners.get(0).asContainer
		val missingFieldNamesSet = cloner.findMissingFieldsInContainerElement
		assertEquals(
			expectedMissingFieldNamesForBookCloner.toString,
			missingFieldNamesSet.sort.toArray.toString
		)
	}
	
	@Test
	def void detectMissingTwoSimpleFieldsFromnestedElementSection() {
		val sample = '''
		deepClone
		package my.home.cloners
		a.b.m.Book BookCloner {
			name
			author
			section {
				//name
				//description
				//paragraph
			}
		}
		'''
		
		val expectedMissingFieldNamesForSectionElement = '''
			[description, name, paragraph]'''
		
		val model = parser.parse(sample)
		val cloner = (model as Body).cloners.get(0).asContainer
		val section = cloner.fields.findFirst[it.fieldName=="section"].asContainer
		val missingFieldNamesInSection = section.findMissingFieldsInContainerElement
		assertEquals(
			expectedMissingFieldNamesForSectionElement.toString,
			missingFieldNamesInSection.sort.toArray.toString
		)
	}
	
	@Test
	def void attemptToGetMissingFieldsForNonExistingClassFieldMustBeHandledGratiously() {
		val sample = '''
		deepClone
		package my.home.cloners
		a.b.m.Book BookCloner {
			name
			author
			fooFoo {  // a.b.m.Book does not have any fooFoo field
			}
		}
		'''
		
		val expectedMissingFieldNamesForFooFooElement = '''
			[]'''
		
		val model = parser.parse(sample)
		val cloner = (model as Body).cloners.get(0).asContainer
		val fooFoo = cloner.fields.findFirst[it.fieldName=="fooFoo"].asContainer
		val missingFieldNamesInSection = fooFoo.findMissingFieldsInContainerElement
		assertEquals(
			expectedMissingFieldNamesForFooFooElement.toString,
			missingFieldNamesInSection.sort.toArray.toString
		)
	}
}