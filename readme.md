# DeepClone - micro language for defining deep cloners for Java beans

DeepClone is DSL domain specific language, or micro language if you wish, for defining deep cloners for Java beans.
It is a hosted language, it is translated into Java code, into human readable Java code. So you don't have to tell your boss or colleagues 
that you are using something like DeepClone DSL. Plain core java code is generated, no library required beyond standard JDK classes. 

DeepClone is language with practically zero learning curve, so simple that it does not need any proper documentation, just look at the examples, .

It is recommended to be used with Eclipse, as in this IDE you can see immediately what Java code is generated.
Mavenized version is in development.

If you wonder what benefits provide cloning DSL over annotation based or external configuration based libraries and 
frameworks please read [why DSL for cloning](https://bitbucket.org/espinosa/deepclone/src/master/why_DSL_for_cloning.md) document.

DeepClone is mainly about defining structural hierarchies and field filters. 
Every field has to be either included or explicitly excluded. It makes you not to forget to add a newly added bean field
or remove unused after a refactoring.

## Simple example

It is language so simple that it does not need any proper documentation, just look at the examples provided.

	deepClone
	
	package my.home.cloners.reduced
	
	a.b.m.Magazine {
		name
		-isbn
		article {
			name
			-author
			-pageNumber
			section {
				-name
				description
				-paragraph
			}
		}
	}
	
is translated to this Java code:

		package my.home.cloners.reduced;
			public class MagazineCloner {
				public a.b.m.Magazine apply(a.b.m.Magazine other) {
					a.b.m.Magazine it = new a.b.m.Magazine();
					it.setName(other.getName());
					it.setArticle(articleCloner.apply(other.getArticle()));
					return it;
				}
				
				private final ArticleCloner articleCloner = new ArticleCloner();
				public static class ArticleCloner {
					public a.b.m.Article apply(a.b.m.Article other) {
						a.b.m.Article it = new a.b.m.Article();
						it.setName(other.getName());
						it.setSection(sectionCloner.apply(other.getSection()));
						return it;
					}
					
					private final SectionCloner sectionCloner = new SectionCloner();
					public static class SectionCloner {
						public a.b.m.Section apply(a.b.m.Section other) {
							a.b.m.Section it = new a.b.m.Section();
							it.setDescription(other.getDescription());
							return it;
						}
					}
				}
			}
in file /src-gen/my/home/cloners/reduced/MagazineCloner.java

## Current Features:

   * field inclusions (default operation, no sign) and exclusions (minus sign) see example above 
   
   * specify target package, package for the generated cloners.
   
   * compile time validation checking that every field is explicitly included or excluded
   
   * support for collection fields, 1:N relations, where collection type, including type parameter, is inferred.  
     Example:  
     
		deepClone
		
		package a.b.c.cloners
		
		a.b.m2.Book BookCloner987 {
			name
			author
			sections {  // where 'sections' is of type java.util.List<a.b.m2.Section> so loop has to created to clone all section items
				name
				description
				paragraphs {
					number
					text
				}
			}
		}
   
   * support for references  
     Example:
     
		deepClone
		
		package a.b.m
		
		a.b.m.Book {
			name
			-author
			&section SectionDeep   // reference
		}
		
		a.b.m.Section SectionDeep {
			name
			description
			paragraph {
				number
				text
			}
		}
		
		a.b.m.Section SectionReduced {
			name
			description
			-paragraph
		}
		
   * Eclipse plugin with editor, content assist, formatting aid, as-you-type validation

## How to set output directory in Eclipse

How to set output directory for generated Java classes in Eclipse? Go to project properties -> DeepClone section -> set output directory (./gen-src is default) 

## How to build from sources and install

No binary version of DeepClone, as a tool or Eclipse plugin is currently provided. 

### Quick run the Eclipse way
Recommended way for testing the plugin:

   * Eclipse must have Xtext plugin installed (standard Eclipse plugin)
   * Clone the Git directory. 
   * Import all 3 DSL projects to Eclipse as separate projects (unfortunate limitation of Eclipse, it does not support hierarchical projects)
   * Run 'my.home.dsl.DeepClone/src/my/home/dsl/GenerateDeepClone.mwe2' (Run as workflow in Eclipse, Xtext runner)  
     _technically, this should not be necessary as I include (nearly) all generated files, but for some reason it breaks if ommitted_ 
   * Righ click on a my.home.dsl.DeepClone project `Run as..` then "Eclipse application" and select "DeepClone workbench launcher"
   * New Eclipse application window should appear, this is the workbench. Create new Java project or navigate to an existing project 
     and try to define some cloners by creating a file with suffix `.cloner`.
     For a introductionary start it is recommended to have some existing Java beans in that project so they can be referenced from that cloner.     

### Build the Eclipse way
When you are happy with the plugin as it is:

   * Eclipse must have Xtext plugin installed (standard Eclipse plugin)
   * Clone the Git directory. 
   * Import all 3 DSL projects to Eclipse as separate projects (unfortunate limitation of Eclipse, it does not support hierarchical projects)
   * Run 'my.home.dsl.DeepClone/src/my/home/dsl/GenerateDeepClone.mwe2' (Run as workflow in Eclipse, Xtext runner)  
     _technically, this should not be necessary as I include (nearly) all generated files, but for some reason it breaks if ommitted_ 
   * Export project. Eclipse -> DSL project root (my.home.dsl.DeepClone) -> Export... -> Deployable plug-ins and fragments -> 
     select my.home.dsl.DeepClone and my.home.dsl.DeepClone.ui, set some Directory, like E:\dev\eclipse-plugins, and hit
     Finish. When exporting successfully finish, you can add generated plugin to any to your Eclipse and start defining
     cloners in you Java projects. You may have to add Xtext nature to your project first. 


### Build the Maven way
in development..