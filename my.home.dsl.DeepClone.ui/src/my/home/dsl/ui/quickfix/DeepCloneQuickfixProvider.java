package my.home.dsl.ui.quickfix;

import java.util.Arrays;
import java.util.List;

import my.home.dsl.deepClone.ContainerType;
import my.home.dsl.utils.DeepCloneUtils;
import my.home.dsl.validation.DeepCloneJavaValidator;

import org.eclipse.emf.ecore.EObject;
import org.eclipse.xtext.ui.editor.model.edit.IModificationContext;
import org.eclipse.xtext.ui.editor.model.edit.ISemanticModification;
import org.eclipse.xtext.ui.editor.quickfix.DefaultQuickfixProvider;
import org.eclipse.xtext.ui.editor.quickfix.Fix;
import org.eclipse.xtext.ui.editor.quickfix.IssueResolutionAcceptor;
import org.eclipse.xtext.validation.Issue;

import com.google.inject.Inject;

/**
 * QuickFix provider based on ISemanticModification.
 * <p>
 * There is known issues with broken formatting after the change in xtext 2.3.1
 * 
 * @author espinosa
 */
public class DeepCloneQuickfixProvider extends DefaultQuickfixProvider {
	
	@Inject
	DeepCloneUtils deepCloneUtils;
	
	@Fix(DeepCloneJavaValidator.MISSING_FIELD_ERROR)
	public void addMissingFields(final Issue issue, IssueResolutionAcceptor acceptor) {
		if (issue.getData() != null) {
			acceptor.accept(
				issue, // issue reference, includes issue data
				"Add all missing fields", // label text
				"Add all missing fields", // description text
				null, // quick fix icon
				new ISemanticModification() {
					public void apply(EObject element, IModificationContext context) {
						ContainerType parentContainer = (ContainerType)element;
						List<String> fieldsToBeAdded = Arrays.asList(issue.getData());
						deepCloneUtils.addFields(parentContainer, fieldsToBeAdded);
					}
				}
			);
		}
	}
	
	@Fix(DeepCloneJavaValidator.SURPLUS_FIELD_ERROR)
	public void removeSurplusFields(final Issue issue, IssueResolutionAcceptor acceptor) {
		if (issue.getData() != null) {
			acceptor.accept(
				issue, // issue reference, includes issue data
				"Remove all surplus fields", // label text
				"Remove all surplus fields", // description text
				null, // quick fix icon
				new ISemanticModification() {
					public void apply(EObject element, IModificationContext context) {
						ContainerType parentContainer = (ContainerType)element.eContainer();
						List<String> fieldsToBeRemoved = Arrays.asList(issue.getData());
						deepCloneUtils.removeFields(parentContainer, fieldsToBeRemoved);
					}
				}
			);
		}
	}
}
