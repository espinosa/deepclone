Sample Java Beans for advanced DSL tests. This tests contains collections, that is 1:N type of relations to other beans. 

All classes here are necessary for DeepClone DSL tests to pass, from parsing to validation and generation.
Tested DSL fragments must reference existing Java bean classes.