package a.b.c;

public class WeekdayOpeningHours {
	private DayOfWeek weekDay;
	private FromToItem hours;
	
	public static enum DayOfWeek {
		MONDAY,TUESDAY, WEDNESDAY, THURSDAY, FRIDAY, SATURDAY, SUNDAY
	}
	
	public DayOfWeek getWeekDay() {
		return weekDay;
	}

	public void setWeekDay(DayOfWeek weekDay) {
		this.weekDay = weekDay;
	}

	public FromToItem getHours() {
		return hours;
	}

	public void setHours(FromToItem hours) {
		this.hours = hours;
	}
}
