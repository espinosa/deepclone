package a.b.c;


/**
 * Example of immutable class, all fields has just getter but not setters, values are set
 * via constructor only.
 */
public class ImmutableBook {
	private final String name;
	private final String author;
	
	public ImmutableBook() {
		this.name = null;
		this.author = null;
	}
	
	public ImmutableBook(String name, String author) {
		this.name = name;
		this.author = author;
	}
	
	public String getAuthor() {
		return author;
	}

	public String getName() {
		return name;
	}
}
