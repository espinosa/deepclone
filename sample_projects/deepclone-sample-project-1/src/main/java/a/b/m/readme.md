Sample Java Beans for simple DSL tests. This tests contains only is 1:1 type of relations to other beans. 

All classes here are necessary for DeepClone DSL tests to pass, from parsing to validation and generation.
Tested DSL fragments must reference existing Java bean classes.