package a.b.z.application;

import a.b.cloner.impl.BookCloner988;
import a.b.m2.Book;
import a.b.m2.Paragraph;
import a.b.m2.Section;

import com.google.common.collect.ImmutableList;
import com.google.common.collect.ImmutableSet;

public class SimpleApp {
	public static void main(String[] args) {
		// create some data
		// use a.b.m2.Book which has 1:N relations (collection) to other beans
		Book book = new Book("History of Britain", "John O'Farrel", ImmutableList.copyOf(new Section[] {
			new Section("Section1", "no description", ImmutableSet.copyOf(new Paragraph[] {
					new Paragraph(1, "Bla bla bla"),
					new Paragraph(1, "Bla bla bla"),
			})),
			new Section("Section2", "no description", ImmutableSet.copyOf(new Paragraph[] {
					new Paragraph(1066, "Bla bla bla"),
			})),
			new Section("Section3", "no description", ImmutableSet.copyOf(new Paragraph[] {
					new Paragraph(1, "Bla bla bla"),
					new Paragraph(2, "Bla bla bla"),
					new Paragraph(3, "Bla bla bla"),
			})),
		}));
		
		// instantiate cloner
		BookCloner988 bookCloner = new BookCloner988();
		
		// create clone
		Book clonedBook = bookCloner.apply(book);

		// clone should have same structure and values
		System.out.println(clonedBook.getAuthor()); //  John O'Farrel
		System.out.println(clonedBook.getSections().get(1).getName()); // Section2
		System.out.println(clonedBook.getSections().get(1).getParagraphs().iterator().next().getNumber()); // 1066
				
		// ..but every element of different reference
		System.out.println(clonedBook == book);  // false
		System.out.println(clonedBook.getSections().get(1) == book.getSections().get(1)); // false
		System.out.println(clonedBook.getSections().get(1).getParagraphs().iterator().next() == book.getSections().get(1).getParagraphs().iterator().next()); // false
	}
}
