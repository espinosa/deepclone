package a.b.cloner.impl;
public class BookCloner988 {
	public a.b.m2.Book apply(a.b.m2.Book other) {
		if (other == null) return null;
		a.b.m2.Book it = new a.b.m2.Book();
		it.setName(other.getName());
		it.setSections(sectionsCollectionCloner(it.getSections(), other.getSections()));
		it.setAuthor(other.getAuthor());
		return it;
	}
	
	private java.util.List<a.b.m2.Section> sectionsCollectionCloner(java.util.List<a.b.m2.Section> thisCollection, java.util.List<a.b.m2.Section> otherCollection) {
		if (otherCollection == null) return null;
		if (thisCollection == null) thisCollection = new java.util.ArrayList<a.b.m2.Section>();
		for (a.b.m2.Section otherCollectionItem : otherCollection) {
			thisCollection.add(sectionsCloner.apply(otherCollectionItem));
		}
		return thisCollection;
	}
	
	private final SectionsCloner sectionsCloner = new SectionsCloner();
	public static class SectionsCloner {
		public a.b.m2.Section apply(a.b.m2.Section other) {
			if (other == null) return null;
			a.b.m2.Section it = new a.b.m2.Section();
			it.setName(other.getName());
			it.setDescription(other.getDescription());
			it.setParagraphs(paragraphsCollectionCloner(it.getParagraphs(), other.getParagraphs()));
			return it;
		}
		
		private java.util.Set<a.b.m2.Paragraph> paragraphsCollectionCloner(java.util.Set<a.b.m2.Paragraph> thisCollection, java.util.Set<a.b.m2.Paragraph> otherCollection) {
			if (otherCollection == null) return null;
			if (thisCollection == null) thisCollection = new java.util.HashSet<a.b.m2.Paragraph>();
			for (a.b.m2.Paragraph otherCollectionItem : otherCollection) {
				thisCollection.add(paragraphsCloner.apply(otherCollectionItem));
			}
			return thisCollection;
		}
		
		private final ParagraphsCloner paragraphsCloner = new ParagraphsCloner();
		public static class ParagraphsCloner {
			public a.b.m2.Paragraph apply(a.b.m2.Paragraph other) {
				if (other == null) return null;
				a.b.m2.Paragraph it = new a.b.m2.Paragraph();
				it.setNumber(other.getNumber());
				it.setText(other.getText());
				return it;
			}
		}
	}
}
