package a.b.cloner.impl;
public class BookCloner {
	public a.b.m.Book apply(a.b.m.Book other) {
		if (other == null) return null;
		a.b.m.Book it = new a.b.m.Book();
		it.setName(other.getName());
		it.setSection(sectionCloner.apply(other.getSection()));
		return it;
	}
	
	private final a.b.cloner.impl.SectionDeep sectionCloner = new a.b.cloner.impl.SectionDeep();
}
