package a.b.cloner.impl;
public class MagazineFull {
	public a.b.m.Magazine apply(a.b.m.Magazine other) {
		if (other == null) return null;
		a.b.m.Magazine it = new a.b.m.Magazine();
		it.setName(other.getName());
		it.setIsbn(other.getIsbn());
		it.setArticle(articleCloner.apply(other.getArticle()));
		return it;
	}
	
	private final ArticleCloner articleCloner = new ArticleCloner();
	public static class ArticleCloner {
		public a.b.m.Article apply(a.b.m.Article other) {
			if (other == null) return null;
			a.b.m.Article it = new a.b.m.Article();
			it.setName(other.getName());
			it.setAuthor(other.getAuthor());
			it.setPageNumber(other.getPageNumber());
			it.setSection(sectionCloner.apply(other.getSection()));
			return it;
		}
		
		private final SectionCloner sectionCloner = new SectionCloner();
		public static class SectionCloner {
			public a.b.m.Section apply(a.b.m.Section other) {
				if (other == null) return null;
				a.b.m.Section it = new a.b.m.Section();
				it.setName(other.getName());
				it.setDescription(other.getDescription());
				it.setParagraph(paragraphCloner.apply(other.getParagraph()));
				return it;
			}
			
			private final ParagraphCloner paragraphCloner = new ParagraphCloner();
			public static class ParagraphCloner {
				public a.b.m.Paragraph apply(a.b.m.Paragraph other) {
					if (other == null) return null;
					a.b.m.Paragraph it = new a.b.m.Paragraph();
					it.setNumber(other.getNumber());
					it.setText(other.getText());
					return it;
				}
			}
		}
	}
}
