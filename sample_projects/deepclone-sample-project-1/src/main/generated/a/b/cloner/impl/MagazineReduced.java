package a.b.cloner.impl;
public class MagazineReduced {
	public a.b.m.Magazine apply(a.b.m.Magazine other) {
		if (other == null) return null;
		a.b.m.Magazine it = new a.b.m.Magazine();
		it.setName(other.getName());
		it.setArticle(articleCloner.apply(other.getArticle()));
		return it;
	}
	
	private final ArticleCloner articleCloner = new ArticleCloner();
	public static class ArticleCloner {
		public a.b.m.Article apply(a.b.m.Article other) {
			if (other == null) return null;
			a.b.m.Article it = new a.b.m.Article();
			it.setName(other.getName());
			it.setSection(sectionCloner.apply(other.getSection()));
			return it;
		}
		
		private final SectionCloner sectionCloner = new SectionCloner();
		public static class SectionCloner {
			public a.b.m.Section apply(a.b.m.Section other) {
				if (other == null) return null;
				a.b.m.Section it = new a.b.m.Section();
				it.setDescription(other.getDescription());
				return it;
			}
		}
	}
}
