package a.b.cloner.impl;
public class SectionDeep {
	public a.b.m.Section apply(a.b.m.Section other) {
		if (other == null) return null;
		a.b.m.Section it = new a.b.m.Section();
		it.setName(other.getName());
		it.setDescription(other.getDescription());
		it.setParagraph(paragraphCloner.apply(other.getParagraph()));
		return it;
	}
	
	private final ParagraphCloner paragraphCloner = new ParagraphCloner();
	public static class ParagraphCloner {
		public a.b.m.Paragraph apply(a.b.m.Paragraph other) {
			if (other == null) return null;
			a.b.m.Paragraph it = new a.b.m.Paragraph();
			it.setNumber(other.getNumber());
			it.setText(other.getText());
			return it;
		}
	}
}
