package a.b.cloner.impl;
public class WeekdayOpeningHoursCloner {
	public a.b.c.WeekdayOpeningHours apply(a.b.c.WeekdayOpeningHours other) {
		if (other == null) return null;
		a.b.c.WeekdayOpeningHours it = new a.b.c.WeekdayOpeningHours();
		it.setWeekDay(other.getWeekDay());
		it.setHours(hoursCloner.apply(other.getHours()));
		return it;
	}
	
	private final HoursCloner hoursCloner = new HoursCloner();
	public static class HoursCloner {
		public a.b.c.FromToItem apply(a.b.c.FromToItem other) {
			if (other == null) return null;
			a.b.c.FromToItem it = new a.b.c.FromToItem();
			it.setFrom(other.getFrom());
			it.setTo(other.getTo());
			return it;
		}
	}
}
