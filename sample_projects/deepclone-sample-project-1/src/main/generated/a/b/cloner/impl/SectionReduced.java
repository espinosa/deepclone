package a.b.cloner.impl;
public class SectionReduced {
	public a.b.m.Section apply(a.b.m.Section other) {
		if (other == null) return null;
		a.b.m.Section it = new a.b.m.Section();
		it.setName(other.getName());
		it.setDescription(other.getDescription());
		return it;
	}
}
