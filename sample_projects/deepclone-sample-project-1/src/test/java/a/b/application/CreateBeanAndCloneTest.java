package a.b.application;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertFalse;
import static org.junit.Assert.assertNotNull;

import org.junit.Test;

import a.b.cloner.impl.BookCloner987;
import a.b.m2.Book;
import a.b.m2.Paragraph;
import a.b.m2.Section;

import com.google.common.collect.ImmutableList;
import com.google.common.collect.ImmutableSet;

public class CreateBeanAndCloneTest {
	
	@Test
	public void createBeanCreateCloneThenCloneShouldHaveSameStructureAndValuesButEveryElementOfDifferentReference() {
		
		// create some data
		// use a.b.m2.Book which has 1:N relations (collection) to other beans
		Book book = new Book("History of Britain", "John O'Farrel", ImmutableList.copyOf(new Section[] {
			new Section("Section1", "no description", ImmutableSet.copyOf(new Paragraph[] {
					new Paragraph(1, "Bla bla bla"),
					new Paragraph(1, "Bla bla bla"),
			})),
			new Section("Section2", "no description", ImmutableSet.copyOf(new Paragraph[] {
					new Paragraph(1066, "Bla bla bla"),
			})),
			new Section("Section3", "no description", ImmutableSet.copyOf(new Paragraph[] {
					new Paragraph(1, "Bla bla bla"),
					new Paragraph(2, "Bla bla bla"),
					new Paragraph(3, "Bla bla bla"),
			})),
		}));
		
		// instantiate cloner
		BookCloner987 bookCloner987 = new BookCloner987();
		
		// create clone
		Book clonedBook = bookCloner987.apply(book);
		
		// check assumptions
		assertNotNull(clonedBook);
		
		// clone should have same structure and values
		assertEquals(clonedBook.getAuthor(), "John O'Farrel");
		assertEquals(clonedBook.getSections().get(1).getName(), "Section2");
		assertEquals(clonedBook.getSections().get(1).getParagraphs().iterator().next().getNumber(), new Integer(1066));
				
		// ..but every element of different reference
		assertFalse(clonedBook == book);
		assertFalse(clonedBook.getSections().get(1) == book.getSections().get(1));
		assertFalse(clonedBook.getSections().get(1).getParagraphs().iterator().next() == book.getSections().get(1).getParagraphs().iterator().next());
	}
}
