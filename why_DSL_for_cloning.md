# Why a special language just for cloning

You may wonder why I developed a special language just for cloning 

Benefits of the DSL way

   * Define several different cloners for one class, each going to different depth, possibly with different fields filtered out.
     This is practically impossible with annotation based processors.
   * Convenient Eclipse editor as a plugin with all the usual comfort - context help, quickfix, syntax highlighting, error highlighting, formatting.
     Editor is courtesy of Xtext, mostly generated with just few necessary definitions.
   * All check are compiler time, not like with some libraries and frameworks in run time.
   * For me it was a way to learn Xtext.  
     Provide example of simple yet not trivial project to study for others.  
     All important parts are present - context help, quickfix, formatter, validator and most importantly the Java code generator definitions.

Plus I have bold plans for the future. Cloning is a just first example. Other 
like bean generation with setters and getters and fully customisable equals and hasCode methods, 
toString serializers, any serializers, can build upon infrastructure and now how originally 
developed with DeepClone.